# -*- coding: utf-8 -*-
{
    'name': "PROSAR-ODOO",

    'summary': """
        Modulo para modificar el comportamiento de Odoo.
        """,

    'description': """
        Este modulo contiene las modificaciones que se necesitan para Odoo, satisfaciendo las necesidades de las empresas
        Sarita Hotel,
        Sarita Restaurante,
        PROSERESA. 
        El modulo para facturacion FEL no esta en este modulo.
    """,

    'author': "Erick Hernandez",
    'website': "http://www.saritarestaurante.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Modules Modify',
    'version': '13.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts', 'account', 'l10n_gt', 'maintenance', 'hr', 'stock', 'hotel', 'hotel_reservation', 'bsm_fel_infile_v13'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/base_res_partner.xml',
        'views/maintenance.xml',
        'views/receipt_fact.xml',
        'views/pos_sale_pos_config.xml',
        'views/account_moves.xml',
        'views/account_journal.xml',
        'views/account_payment.xml',
    ],
    'qweb':[
        'views/header_footer_receipt.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'auto_install': False
}
