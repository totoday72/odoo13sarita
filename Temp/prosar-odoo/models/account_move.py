from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
import re


class sarita_account_move(models.Model):
    _inherit = 'account.move'
    # _inherit = 'bsm_fel_infile_v13.FacturaElectronica'
    # inf_fecha_certifica = fields.Char(string='Fecha certific3a')
    codigo_persona = fields.Char(string='Codigo de cliente:', store=True, )
    Referencia_interno = fields.Char(string="Referencia interno:", store=True, )

    def btn_prueba_diarios(self):
        print('Pruebas desde factura para crear diarios')
        pago = self.env['account.payment'].create({
            'payment_type': 'transfer',
            'company_id': 1,
            'journal_id': 7,
            'destination_journal_id': 92,
            'payment_method_id': 1,
            'amount': self.amount_total,
            'currency_id': self.currency_id.id,
            'payment_date': self.invoice_date,
            "communication": "Factura a:" + self.partner_id.name,
            "descripcion": "aca va una descripcion",
            "nombre_impreso": "aca va el nombre impreso",
            "no_negociable": True
        })
        pago.post()
        print('Pruebas desde factura para crear diarios', str(pago.amount))
        return {
                'view_type': 'form',
                'view_mode': 'form',
                # 'view_id': res and res[1] or False,
                'res_model': 'account.payment',
                'src_model': "stock.picking",
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'current',
                'res_id': pago.id,
        }

