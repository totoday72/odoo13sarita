# -*- coding: utf-8 -*-

from . import models
from . import account_move
from . import base_res_partner
from . import maintenance_Sarita
from . import maintenance_equipos
# from . import account_move
from . import pos_sale_pos_config
from . import pos_order
from . import account_journal
from . import account_payment