from odoo import fields, models, api, _
from odoo.exceptions import UserError
import re


class sarita_fel_contact_info(models.Model):
    _inherit = 'res.partner'
    # account_nit = fields.Char(
    #     string = "N.I.T.:",
    # )
    account_dpi = fields.Char(string="CUI:")
    imagen_Factura = fields.Binary(string="Imagen Promo:")
    logo_tiket = fields.Binary(string="Logo Ticket:")
    id_user = fields.Char(string="Perimiso:", readonly=True)
    indexes = [2, 6, 7, 8, 9, 10]  # aca se colocan los id de los que pueden modificar las imagenes que aparecen en los tickets de facturas

    # property_account_receivable_id = fields.Many2one('account.account', string='type', readonly=True)
    # customer_ranks = fields.Selection(
    #     [('0', 'Regular'), ('0', 'Normal'), ('1', 'Bueno'),
    #      ('3', 'Muy Bueno'),
    #      ('4', 'Excelente')], string="Proveedor:")
    # supplier_ranks = fields.Selection(
    #     [('0', 'Regular'), ('0', 'Normal'), ('1', 'Bueno'),
    #      ('3', 'Muy Bueno'),
    #      ('4', 'Excelente')], string="Cliente:")
    # @api.onchange('customer_ranks')
    # def change_customer_ranks(self):
    #     print("se guardo")
    #     for rec in self:
    #         rec.customer_rank = 1
    #         print(rec.customer_rank)
    #     return
    #
    # @api.onchange('supplier_ranks')
    # def change_supplier_ranks(self):
    #     print("se guardo")
    #     for rec in self:
    #         rec.supplier_rank = 1
    #         print(rec.supplier_rank)
    #     return
    @api.onchange('name')
    def verificar_permisos(self):
        print("This is my id user ", self.env.user.id)
        for index in self.indexes:
            if index == self.env.user.id:
                self.id_user = "OK"
                return
        self.id_user = "No tiene permisos"
        # self.id_user['readonly'] = False
        return

    def mark_customer(self):
        for rec in self:
            if rec.customer_rank == 1:
                rec.customer_rank = 0
                return
            rec.customer_rank = 1

    def mark_supplier(self):
        for rec in self:
            if rec.supplier_rank == 1:
                rec.supplier_rank = 0
                return
            rec.supplier_rank = 1

    # para poder hacer un override de un metodo que ya esta implementado en odoo coloco el @api igual que el original y le pongo el mismo nombre
    @api.onchange('country_id')  # esto tiene que estar igual al metodo o funcion que quiero sobre escribir
    def _onchange_country_id(self):  # el nombre del metodo tiene que estar igual como el de la clase.
        print("hola")  # puedo poner el codigo que necesite ejecutar
        res = super(sarita_fel_contact_info, self)._onchange_country_id()  # ejecuta el codigo del original
        return res  # retorno el valor que me produce ejecutar el codigo original

    @api.onchange('name')
    def set_upper(self):
        if self.name:
            self.name = str(self.name).upper()
            companys = self.env['res.partner'].search([])
            variabletexto = ""
            for contacto in companys:
                # print("Empresa>", contacto.name, " ID>", contacto.id)
                variabletexto += "Empresa>," + str(
                    contacto.name) + ", ID>" + str(
                    contacto.id) + "\n"
            # self.description = variabletexto
            if self.env.company.name == "Restaurante Sarita":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Hotel Sarita, S.A.":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Personal y Servicios de Restaurantes S.A.":
                self.parent_id = self.env.company.id
        return
