from odoo import fields, models, api, _
from odoo.exceptions import UserError
import re


class sarita_account_journal(models.Model):
    _inherit = 'account.journal'

    porcentaje_comision = fields.Char(string='Porcentaje de Comision:', store=True)