from odoo import fields, models, api


class mantenimiento_equipments(models.Model):
    _inherit = ['maintenance.equipment']
    location = fields.Selection(
        [('distribution', 'Distribucion'), ('productionB11', 'Produccion B.11'), ('bakery', 'Panaderia'),
         ('offices', 'Oficinas'),
         ('others', 'Otros')],
        string='Used in location', default="offices", store=True)
    id_user = fields.Char(string="Perimiso para crear:", readonly=True, force_save="1")
    indexes = [2, 3, 4, 5, 6, 7]  # aca se colocan los id de los que pueden crear nuevos equipos.

    @api.onchange('name')
    def nombre_equipo_error(self):
        print("This is my id user ", self.env.user.id)
        for index in self.indexes:
            if index == self.env.user.id:
                self.id_user = "OK"
                return
        self.id_user = "No tiene permisos"
        self.id_user['readonly'] = False
        return

    # equipment_assign_to = fields.Selection(
    #     [('department', 'Departamento'), ('employee', 'Empleado'),
    #      ('other', 'Otro')],
    #     string='Used by', default="employee", store=True)
    # employee_id = fields.Many2one('hr.employee', string="Empleado", store=True, )
