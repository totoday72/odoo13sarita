from odoo import fields, models, api, _
from odoo.exceptions import UserError
import re


class sarita_account_payment(models.Model):
    _inherit = 'account.payment'

    porcentaje_comision = fields.Char(string='Porcentaje de Comision:', store=True, related='journal_id.porcentaje_comision')

