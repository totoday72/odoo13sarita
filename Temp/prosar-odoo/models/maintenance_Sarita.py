from odoo import fields, models, api, _
from odoo.exceptions import UserError
import re


class mantenimientoSarita(models.Model):
    _inherit = 'maintenance.request'
    maintenance_type = fields.Selection(
        [('corrective', 'Corrective'), ('preventive', 'Preventive'), ('emergency', 'Emergencia'),
         ('proyect', 'Proyecto'),
         ('infrastructure', 'Infrastructura')],
        string='Maintenance Type', default="corrective")
    account_dpi = fields.Char(string="CUI:")
    company_id = fields.Many2one('res.company', string="Company", store=True,)
    # email_ccc = fields.Char(string="Correo Electronico CC:", invisible=True)
