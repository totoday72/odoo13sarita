# -*- coding: utf-8 -*-
{
    'name': "bolson",

    'summary': """
        Modulo usado para el pago en Lotes..""",

    'description': """
        Este modulo incluye en el área de contabilidad, menú proveedores, sub menú “Liquidaciones”.
Por medio de esta ventana se pueden realizar el pago de facturas por lotes. 
    """,

    'author': "Mynor",
    'website': "https://saritarestaurante.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['account'],

    # always loaded
    'data': [
        'security/bolson_security.xml',
        'security/ir.model.access.csv',
        'views/bolson_view.xml',
        'views/invoice_view.xml',
        'views/payment_view.xml',
        'views/report.xml',
        'views/reporte_bolson.xml',
        'views/templates.xml',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
