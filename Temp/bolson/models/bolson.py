# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

import logging
#importaciones adicionales
import json

from odoo.tools import datetime


class BolsonBolson(models.Model):
    _name = 'bolson.bolson'
    _description = 'Bolson de facturas y cheques'
    _order = 'fecha desc'

    fecha = fields.Date(string="Fecha", required=True)
    #name = fields.Char(string="Descripcion", required=True)  este es el campo orinal
    # Almacena la palabra H1-número correlativo de liquidacion. Ejemplo H1-Liq-1
    name = fields.Char(string='No. Liquidación:', default=lambda self: self.set_nuevo_numero_liquidacion_mostrado(), readonly=True)
    facturas = fields.Many2many('account.move',string="Facturas")
    #facturas = fields.One2many("account.move", "bolson_id", string="Facturas")
    #facturas = fields.One2many("account.move", "bolson_id", string="Facturas", copy=True, readonly=False, states = {'draft': [('readonly', False)]} )
    cheques = fields.One2many("account.payment", "bolson_id", string="Cheques")
#    company_id = fields.Many2one("res.company", string="Company", required=True, default=lambda self: self.env.user.company_id.id)
    # Almacena la empresa en que se esta trabajando
    company_id = fields.Many2one('res.company', string='Empresa', required=True, index=True, store=True,
                                 ondelete="restrict", default=lambda self:self.env['res.company']._company_default_get('sdi.channel'), readonly=True)

    diario = fields.Many2one("account.journal", string="Diario", required=True)
    descripcion = fields.Char(string="Descripcion", required=True)
    asiento = fields.Many2one("account.move", string="Asiento")
    #usuario_id = fields.Many2one("res.users", string="Usuario")
    # Almacena el usuario logueado en el sistema
    usuario_id = fields.Char(string='Usuario', default=lambda self: self._nombre_del_usuario(), readonly=True)
    cuenta_desajuste = fields.Many2one("account.account", string="Cuenta de desajuste")
    estado = fields.Boolean(string='Realizado', default=False, readonly=True)
    #cliente al cual se emitira el pago
    #Proveedor = fields.Many2one("res.partner", string="Proveedor", readonly=False,ondelete='restrict')
    Proveedor = fields.Many2one("res.partner", string="Proveedor", required=True)
    #bolson_id = fields.Many2one("bolson.bolson", string="Liquidacion", readonly=False, ondelete='restrict')
    def conciliar(self):
        #self.obtener_partner_id()
        for rec in self:
            lineas = []

            total = 0
            for f in rec.facturas:
                logging.warn(f.name)
                logging.warn(f.amount_total)
                for l in f.line_ids:
                    if l.account_id.reconcile:
                        if not l.reconciled:
                            total += l.credit - l.debit
                            lineas.append(l)
                            logging.warn(l.credit - l.debit)
                        else:
                            raise UserError('La factura %s ya esta conciliada' % (f.number))

            for c in rec.cheques:
                logging.warn(c.name)
                logging.warn(c.amount)
                for l in c.move_line_ids:
                    if l.account_id.reconcile:
                        if not l.reconciled :
                            total -= l.debit - l.credit
                            lineas.append(l)
                            logging.warn(l.debit - l.credit)
                        else:
                            raise UserError('El cheque %s ya esta conciliado' % (c.name))

            if round(total) != 0 and not rec.cuenta_desajuste:
                raise UserError('El total de las facturas no es igual al total de los cheques y los extractos')

            pares = []
            nuevas_lineas = []
            for linea in lineas:
                nuevas_lineas.append((0, 0, {
                    'name': linea.name,
                    'debit': linea.credit,
                    'credit': linea.debit,
                    'account_id': linea.account_id.id,
                    'partner_id': linea.partner_id.id,
                    'journal_id': rec.diario.id,
                    'date_maturity': rec.fecha,
                }))

            if total != 0:
                nuevas_lineas.append((0, 0, {
                    'name': 'Diferencial en ' + rec.name,
                    'debit': -1 * total if total < 0 else 0,
                    'credit': total if total > 0 else 0,
                    'account_id': rec.cuenta_desajuste.id,
                    'date_maturity': rec.fecha,
                }))

            move = self.env['account.move'].create({
                'line_ids': nuevas_lineas,
                'ref': rec.name,
                'date': rec.fecha,
                'journal_id': rec.diario.id,
            });

            indice = 0
            invertidas = move.line_ids[::+1]
            for linea in lineas:
                par = linea | invertidas[indice]
                par.reconcile()
                indice += 1

            move.post()
            self.write({'asiento': move.id})
        self.estado=True
        return True

    def cancelar(self):
        for rec in self:
            for l in rec.asiento.line_ids:
                if l.reconciled:
                    l.remove_move_reconcile()
            rec.asiento.button_cancel()
            rec.asiento.unlink()

        return True

    # Establece el número de corte interno y lo Muestra en pantalla
    def set_nuevo_numero_liquidacion_mostrado(self):
        empresa=self.get_nom_empresa() + "-Liq-"
        query = "select max(id) from bolson_bolson"
        self.env.cr.execute(str(query))
        json_query = self.env.cr.dictfetchone()
        try:
            valor = json.loads(str(json_query).replace("'", "\""))
            correlativo = int(valor["max"]) + 1
        except:
            correlativo = 1
        return empresa + str(correlativo)

    # Método establece el nombre de usuario logueado.
    @api.model
    def _nombre_del_usuario(self):
        return str(self.env.user.name)

    #para recorrer todas las facturas ver ejemplo en control_cheques.models.py en def btn_emitido(self):

    def get_nom_empresa(self):
        empresa = self.env['res.company'].search([('id', '<=', 100)])
        #empresa = self.env['res.company']
        #print("hola mundo ", len(empresa))
        for p in empresa:
            #print("hola mundo ", p.name)
            if p.id== 1:
                nombreempresa="H1"
        return nombreempresa;

    def action_invoice_register_payment(self):
        id_proveedor = 0
        id_diario = 0
        id_moneda= 0
        nombre_liq=''
        bolson_id = False
        for rec in self:
            id_proveedor= rec.Proveedor.id
            id_diario= rec.diario.id
            #id_moneda= rec.diario.currency_id.res.currency
            id_moneda = rec.diario.currency_id.id
            nombre_liq= rec.name
            bolson_id = rec.id
        print('id_proveedor: ', id_proveedor , '  id_diario: ' , id_diario, '  id_moneda: ' , id_moneda, '  nombre_liq: ' , nombre_liq )
        pagar = self.env['account.payment'].create({
#            'date': '',
#            'ref': payment.communication,
#            'journal_id': payment.journal_id.id,
#            'currency_id': payment.journal_id.currency_id.id or payment.company_id.currency_id.id,
#            'partner_id': payment.partner_id.id,
           # 'date': '2022-02-01',
            #'ref': '123',
            'payment_type': 'outbound',
            'journal_id': 6,#id_diario,#6,
            'amount': 100,
            'partner_type': 'supplier',
            'currency_id': 170,
            'partner_id': id_proveedor,
            'payment_method_id': 2,
            'bolson_id': bolson_id,#'H1-Liq-3',#nombre_liq.name, #'H1-Liq-3' ,
        })
        pagar.post()

        #ids_pago=  self.env['account.payment'].search()
        ides = self.env['account.payment'].search([('bolson_id','=',str(nombre_liq))])
        for idet in ides:
            print('el id es: ' , idet.id)
            #self.cheques=idet.name
            self.cheques = self.get_taxes(idet.id)
        # return self.env['account.payment'] \
        #     .with_context(active_ids=[8739], active_model='account.move', active_id=False) \
        #     .action_register_payment()

#oara realizar un pago automatico lo veo en addons/account-models-account_payment-(def post(self))
#oara realizar un pago automatico lo veo en addons/account-models-action_register_payment

    def obtener_partner_id(self):
        id_proveedor = 0
        for rec in self:
            id_proveedor= rec.Proveedor.id
        #print("Este es el id del cliente : " , id_proveedor)
        return id_proveedor

    @api.model
    def get_taxes(self,id):
        return  self.env['account.payment'].search([('id','=',id)])

