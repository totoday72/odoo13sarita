# -*- coding: utf-8 -*-
{
    'name': "Corte Z",
    'version': '13.0.0.0.1',
    'summary': 'Corte Z',
    'author': 'Aníbal Gómez Morales',
    'maintainer': 'Aníbal Gómez Morales',
    'Company': 'RESASA',
    'website': 'https://saritarestaurante.com/',

    # any module necessary for this one to work correctly
    'depends': ['base','sale_management'],
    'license': 'LGPL-3',
    'category': 'Sales',


    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/facturas.xml',
        'views/facturas_vencidas.xml',
        'views/pagos.xml',
        'views/cxc.xml',
        "views/menus.xml",
        "views/anticipos.xml",
        'reports/corte_z.xml',


    ],


    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

    "images": ["static/description/icon.png",
                "static/description/src/img/icon.png",
               ],
    "application": True,
}
