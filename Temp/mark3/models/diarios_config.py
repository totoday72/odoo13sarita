from odoo import api, fields, models, _
from odoo import models, fields, api


class ConfigDiario(models.Model):
    _inherit = 'res.company'

    id_diario_efectivo = fields.Char(string="ID Efectivo")
    id_diario_banco = fields.Char(string="Id Bancos")
    id_diario_tarjeta_visa = fields.Char(string="Id Tarjeta Visa")
    id_diario_tarjeta_credo = fields.Char(string="Id Tarjeta Credomatic")


