from odoo import api, fields, models, _
import datetime
import base64
import json
import uuid
import qrcode
from odoo.exceptions import RedirectWarning, MissingError, UserError, ValidationError
from io import BytesIO
import requests
import pytz
from array import array
import locale
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib


class corte_hotel(models.Model):
    _name = 'corte_z.hotel'
    _description = 'Hotel'
    _inherit = ["mail.thread", "mail.activity.mixin"]

    # Variables que almacenan información general del corte Z.
    # Almacena solo el número de corte
    num_corte = fields.Integer(string='No.', default=lambda self: self.set_nuevo_numero_corte(), required=True)
    # Almacena la palabra H1-número correlativo. Ejemplo H1-42
    num_corte_mostrado = fields.Char(string='No. Corte:', default=lambda self: self.set_nuevo_numero_corte_mostrado(), readonly=True)
    # Almacena el usuario logueado en el sistema
    usuario = fields.Char(string='Usuario', default=lambda self: self._nombre_del_usuario(), readonly=True)
    usuario_conta = fields.Char(string='Usuario', readonly=True)
    # Almacena fecha en que se realiza el corte
    fecha = fields.Date(string='Fecha', default=lambda self: self.set_fecha_actual(), readonly=True)
    # Almacena informaci{on el periodo en curso
    num_periodo = fields.Integer(string='No. Periodo', default=lambda self: self.set_periodo(), readonly=True)
    # Almacena el turno en que se realiza el corte
    turno = fields.Char(string='Turno:', default="Unico", readonly=True)
    # Almacena la empresa en que se esta trabajando
    company_gt = fields.Many2one('res.company', string='Empresa', required=True, index=True, store=True,
                                 ondelete="restrict", default=lambda self:self.env['res.company']._company_default_get('sdi.channel'), readonly=True)
    # Almacena el estado del corte Z, si este ya se realizo o aun no.
    realizado = fields.Boolean(string='Realizado', default=False, readonly=True)

    # Validez de corte z.
    vali_trans_venta = fields.Boolean(string='Validado', default=False, readonly=True)
    cant_facturas = fields.Integer(string='Facturas Emitidas:', readonly=True)
    description = fields.Char(string='Nota', default="", store=True)

    # Campos auxiliares para mostrar las conversiones respectivas entre monedas
    quetzales = fields.Float(string='Monto en Dolares', default="", readonly=True)
    dolares = fields.Float(string='Cantidad En Sistema $:', readonly=True)

    # Varibales que almacenan las cantidades totales según método de pago.
    cant_efectivo = fields.Float(string='Efectivo:', readonly=True)
    cant_dolares = fields.Float(string='Dolares:', readonly=True)
    cant_visa = fields.Float(string='Tarjeta Visa:', readonly=True)
    cant_credo = fields.Float(string='Tarjeta Credo:', readonly=True)
    cant_deposito_transferencia = fields.Float(string='Depositos/Transferencias:', readonly=True)
    cant_credito = fields.Float(string='Crédito:', readonly=True)
    cant_anticipo = fields.Float(string='Anticipos', readonly=True)
    cant_exencion = fields.Float(string='Exención', readonly=True)
    cant_retencion = fields.Float(string='Retención', readonly=True)
    monto_total = fields.Float(string='Monto Total', readonly=True)
    # Varibales que almacenan las cantidades totales en visa y credo (Ej: visa en corte_z+visa en anticipo+visa en cxc).
    cant_total_visa = fields.Float(string='Total Tarjeta Visa:', required=True, default=-1,
                                   help="Ingrese el Total neto según el POS VisaNet")
    cant_total_credo = fields.Float(string='Total Tarjeta Credo:', required=True, default=-1,
                                    help="Ingrese el total Neto según el POS Credomatic")

    # Varibales que almacenan información sobre los Depositos.
    no_boleta_ventas = fields.Char(string='No. Boleta', default="",
                                   help="Ingrese el número de boleta que se depositara en el banco.", required=True)
    no_boleta_propina = fields.Char(string='No. Boleta propina', default="",
                                    help="Ingrese el número de boleta que se depositara en el banco.")
    no_boleta_dolares = fields.Char(string='No. Boleta dolares', default="",
                                    help="Ingrese el número de boleta que se depositara en el banco.")
    # Variables que muestran la cantidad segun los calculos el monto que debiera ir en cada deposito (Quetzales y Dolares)
    monto_boleta_ventas = fields.Float(string='Cantidad en sistema', default="", readonly=True)
    monto_boleta_dolares = fields.Float(string='Cantidad en sistema $', default="", readonly=True)
    # Variables que almacenan información de los montos a depositar en los depositos.
    monto_quetzales = fields.Float(string='Monto en Q', required=True, default=-1, help="Ingrese la cantidad en quetzales.")
    monto_propina = fields.Float(string='Monto en Q', default="", help="Ingrese la cantidad en quetzales.")
    monto_dolares = fields.Float(string='Monto en $', default="", help="Ingrese la cantidad e dolares.")

    imagen_boleta_ventas = fields.Binary(string='No. Boleta Ventas')
    imagen_boleta_propina = fields.Binary(string='No. Boleta Propina')
    imagen_boleta_dolares = fields.Binary(string='No. Boleta Dolares')
    imagen_cortez_pos = fields.Binary(string='Cargar Corte Z POS')

    # Variables que almacenan las cantidades de Voucher emitidas segun VisaNet y CredoMatic.
    cant_voucher_visa = fields.Integer(string='Cantidad Voucher Visa:', default="", readonly=True)
    cant_voucher_credo = fields.Integer(string='Cantidad Voucher Credo:', default="", readonly=True)
    cant_total_voucher_visa = fields.Integer(string='Cantidad Total Voucher Visa:', default="", required=True,
                                             help="Ingrese la cantidad de voucher según el POS VisaNet")
    cant_total_voucher_credo = fields.Integer(string='Cantidad Total Voucher Credo:', default="", required=True,
                                              help="Ingrese la cantidad de voucher según el POS CredoMatic")
    # Varibales que almacenan la cantidad de Movimientos por deposito o transferencia.
    cant_mov = fields.Integer(string='Movimientos Depositos/Transferencia', default="", readonly=True)

    # Varibales que almacenan las cantidades totales según los cortes Z que imprimen los POS de VisaNet y CredoMatic.
    pos_visa = fields.Float(string='Total POS Visa', default="", store=True, required=True)
    pos_credo = fields.Float(string='Total POS Credo', default="", store=True, required=True)
    saldo_pendiente_de_cobro = fields.Float(string='Faltante', default="", readonly=True)

    # Varibales que almacenan información sobre los anticipos.
    anticipo_efectivo = fields.Float(string='Efectivo', default="", readonly=True)
    anticipo_dolares = fields.Float(string='Dolares', default="", readonly=True)
    anticipo_en_dolares = fields.Float(string='Dolares', default="", readonly=True)
    anticipo_visa = fields.Float(string='Tarjeta Visa', default="", readonly=True)
    anticipo_credo = fields.Float(string='Tarjeta Credo', default="", readonly=True)
    anticipo_deposito_transferencia = fields.Float(string='Depositos/Transferencia', default="", readonly=True)
    monto_anticipo = fields.Float(string='Total', default="", readonly=True)

    imagen_anticipo = fields.Binary(string='Cargar Documento')
    nota_anticipo = fields.Char(string='Nota')

    # Varibales que almacenan las cantidades totales según método de pago al Credito en la pestaña Corte al Credito.
    cxc_efectivo = fields.Float(string='Efectivo', default="", readonly=True)
    cxc_dolares = fields.Float(string='Dolares', default="", readonly=True)
    cxc_visa = fields.Float(string='Tarjeta Visa', default="", readonly=True)
    cxc_credo = fields.Float(string='Tarjeta Credo', default="", readonly=True)
    cxc_cheque = fields.Float(string='Cheque', default="", readonly=True)
    cxc_deposito = fields.Float(string='Depositos', default="", readonly=True)
    cxc_transferencia = fields.Float(string='Transferencia', default="", readonly=True)
    cxc_exencion = fields.Float(string='Exención', readonly=True)
    cxc_retencion = fields.Float(string='Retención', readonly=True)

    cxc_total = fields.Float(string='Total', default="", readonly=True)
    nota_cxc = fields.Char(string='Nota')
    saldos_vencidos = fields.Float(string='Saldos Vencidos:', default="", readonly=True)

    # Calcula el corte x.
    corte_x = fields.Boolean(string='Corte X')

    # Reportes
    facturas_corte = fields.Html(string='HTML')
    facturas_pagadas = fields.Html(string='HTML')
    tabla_pagos_efectivo = fields.Html(string='HTML')
    tabla_pagos_dolares = fields.Html(string='HTML')
    tabla_pagos_tarjeta_visa = fields.Html(string='HTML')
    tabla_pagos_tarjeta_credo = fields.Html(string='HTML')
    tabla_pagos_deposito_trans = fields.Html(string='HTML')
    tabla_pagos_anticipo_deposito_trans = fields.Html(string='HTML')
    tabla_pagos_anticipo_dolares = fields.Html(string='HTML')
    tabla_pagos_anticipo_visanet = fields.Html(string='HTML')
    tabla_pagos_anticipo_credomatic = fields.Html(string='HTML')
    cuentas_x_cobrar = fields.Html(string='HTML')
    tabla_anticipos = fields.Html(string='Tabla Anticipos')
    tabla_facturas_pendientes = fields.Html(string='Facturas pendientes')
    tabla_facturas_vencidas = fields.Html(string='Tabla Facturas Vencidas')
    tabla_facturas_pagadas_credito = fields.Html(string='Tabla Facturas credito')

    # Id Usuarios que pueden realizar un Corte
    empleados = [1, 13, 14, 15, 11, 12]

    # Columnas DEBE y HABER de Partida Ventas por Clase.
    p_ventas_debe = fields.Html(string='Debe')
    p_ventas_haber = fields.Html(string='Haber')

    # Cuentas de la columna haber de la partida de Corte z Detalle de Ventas
    # pd_ = partida debe
    pd_efectivo = fields.Float(string='Efectivo', readonly=True)
    pd_dolares = fields.Float(string='Dolares', readonly=True)
    pd_visanet = fields.Float(string='Visanet', readonly=True)
    pd_credomatic = fields.Float(string='Credomatic', readonly=True)
    pd_dep_transf = fields.Float(string='Dep/Transf', readonly=True)
    pd_credito = fields.Float(string='Credito', readonly=True)
    pd_retencion = fields.Float(string='Retención', readonly=True)
    pd_extencion = fields.Float(string='Exención', readonly=True)
    pd_total = fields.Float(string='Total', readonly=True)

    # Cuentas de la columna debe de la partida de Corte z
    pd_iva_pagar = fields.Float(string='Iva por Pagar', readonly=True)
    pd_inguat_pagar = fields.Float(string='Inguat por Pagar', readonly=True, )
    pd_ventas = fields.Float(string='Ventas', readonly=True)
    pd_hospedaje = fields.Float(string='Servicio-Hospedaje', readonly=True)
    pd_lavanderia = fields.Float(string='Lavanderia', readonly=True)
    pd_alimentacion = fields.Float(string='Alimentacion', readonly=True)
    pd_piscinas = fields.Float(string='Piscinas', readonly=True)
    pd_otros_servicios = fields.Float(string='Otros Servicios', readonly=True)
    ph_total = fields.Float(string='Total', readonly=True)

    ajuste_efectivo = fields.Float(string='Efectivo')
    ajuste_dolares = fields.Float(string='dolares')
    ajuste_visanet = fields.Float(string='visanet')
    ajuste_credomatic = fields.Float(string='credomatic')
    ajuste_dep_transf = fields.Float(string='dep_transf')
    ajuste_credito = fields.Float(string='credito')
    ajuste_retencion = fields.Float(string='retencion')
    ajuste_extencion = fields.Float(string='extencion')
    ajuste_total = fields.Float(string='Total')

    # Datos para partida para Abono Creditos
    # Variables para el Debe
    # ac=abono credito, pd=partida en el debe, Eje ac_pd_NombreDeVariable
    ac_pd_efectivo_trans = fields.Float(string='Efectivo Transitorio', default='1', readonly=True)
    ac_pd_dolares_trans = fields.Float(string='Dolares Transitorio', readonly=True)
    ac_pd_visanet_trans = fields.Float(string='Visanet Transitorio', readonly=True)
    ac_pd_credomatic_trans = fields.Float(string='Credomatic Transitorio', readonly=True)
    ac_pd_dep_transf_trans = fields.Float(string='Dep/Transf Transitorio', readonly=True)
    ac_pd_credito_trans = fields.Float(string='Credito Transitorio', readonly=True)
    ac_pd_retencion_trans = fields.Float(string='Retencion Transitorio', readonly=True)
    ac_pd_extencion_trans = fields.Float(string='Extencion Transitorio', readonly=True)
    ac_pd_total_trans = fields.Float(string='Total Transitorio', )
    # Variables para el Haber
    ac_ph_efectivo = fields.Float(string='Efectivo', default='1', readonly=True)
    ac_ph_dolares = fields.Float(string='Dolares', readonly=True)
    ac_ph_visanet = fields.Float(string='Visanet', readonly=True)
    ac_ph_credomatic = fields.Float(string='Credomatic', readonly=True)
    ac_ph_dep_transf = fields.Float(string='Dep/Transf', readonly=True)
    ac_ph_credito = fields.Float(string='Credito', readonly=True)
    ac_ph_retencion = fields.Float(string='Retencion', readonly=True)
    ac_ph_extencion = fields.Float(string='Extencion', readonly=True)
    ac_ph_total = fields.Float(string='Total', )

    # Conciliacion de tarjetas Visa
    cs_consumo_v = fields.Float(string='Consumo', readonly=True)
    cs_comision_v = fields.Float(string='Comisión Visa', readonly=True)
    cs_iva = fields.Float(string='IVA', readonly=True)
    cs_neto = fields.Float(string='Neto', readonly=True)
    cs_propina = fields.Float(string='Propina', readonly=True)
    cs_retencion = fields.Float(string='Retencion', readonly=True)
    cs_liquido = fields.Float(string='Liquido a Recibir', readonly=True)
    cs_recibido = fields.Float(string='Recibido')
    cs_diferencia = fields.Float(string='Diferencia')
    cs_inguat = fields.Float(string='INGUAT', required=True, default=-1, help="Ingrese el Impueto INGUAT según el POS VisaNet")
    cs_comision_sin_iva = fields.Float(string='Comisión sin IVA', readonly=True)

    # Conciliacion de tarjetas Credo
    cs_inguat_c = fields.Float(string='INGUAT', required=True, default=-1, help="Ingrese el Impueto INGUAT según el POS Credomatic")
    cs_consumo_c = fields.Float(string='Consumo', readonly=True)
    cs_comision_c = fields.Float(string='Comisión Credo', readonly=True)
    cs_iva_c = fields.Float(string='IVA', readonly=True)
    cs_neto_c = fields.Float(string='Neto', readonly=True)
    cs_propina_c = fields.Float(string='Propina', readonly=True)
    cs_retencion_c = fields.Float(string='Retencion', readonly=True)
    cs_liquido_c = fields.Float(string='Liquido a Recibir', readonly=True)
    cs_recibido_c = fields.Float(string='Recibido')
    cs_diferencia_c = fields.Float(string='Diferencia')
    val_visa = fields.Boolean(string='Validado', default=False, readonly=True)
    val_credo = fields.Boolean(string='Validado', default=False, readonly=True)
    cs_comision_sin_iva_c = fields.Float(string='Comisión sin IVA', readonly=True)

    nota_consiliacion = fields.Char(string='Nota')
    nota_detalle_ventas = fields.Char(string='Nota')

    # Arreglo
    cant_debe = []

    # Transforma el numero con formato de comas y punto
    def format_num(self, num):
        numero = '{:<}{:=12,.2f}'.format('Q', num)
        return numero

    # Retorna la fecha actual del sistema
    def set_fecha_actual(self):
        return datetime.datetime.now(pytz.timezone("America/Guatemala"))

    # Establece el número de corte interno
    def set_nuevo_numero_corte(self):
        query = "select max(id) from corte_z_hotel"
        self.env.cr.execute(str(query))
        json_query = self.env.cr.dictfetchone()
        try:
            valor = json.loads(str(json_query).replace("'", "\""))
            correlativo = int(valor["max"]) + 1
        except:
            correlativo = 1
        return correlativo

    # Establece el número de corte interno y lo Muestra en pantalla
    def set_nuevo_numero_corte_mostrado(self):
        query = "select max(id) from corte_z_hotel"
        self.env.cr.execute(str(query))
        json_query = self.env.cr.dictfetchone()
        try:
            valor = json.loads(str(json_query).replace("'", "\""))
            correlativo = int(valor["max"]) + 1
        except:
            correlativo = 1
        return 'H1-' + str(correlativo)

    # Obtiene el numero de creacion del registro
    def get_creacion_corte(self, corte):
        query = "select fecha from corte_z_hotel where num_corte ="+ str(corte)
        self.env.cr.execute(str(query))
        query_result = self.env.cr.dictfetchone()
        data = query_result.get("fecha")
        return data

    def insertar_numero_corte(self, correlativo):
        empresa = self.env['res.company']._company_default_get('sdi.channel').name
        emp = ",'" + empresa + "'"
        try:
            query = """
                  INSERT INTO correlativo_cortes_z(id,empresa)
                  VALUES (""" + str(correlativo) + emp + """)
              """
            self.env.cr.execute(query)
        except NameError:
            print("Error")
        return "ok"

    # Establece el número de periodo.
    def set_periodo(self):
        query = "select codigo from periodo where fechainicial <= '" + str(
            fields.Datetime.now()) + "'" + " and fechafinal >= '" + str(fields.Datetime.now()) + "'"
        self.env.cr.execute(query)
        json_query = self.env.cr.dictfetchone()
        valor = json.loads(str(json_query).replace("'", "\""))
        periodo_actual = int(valor["codigo"])
        return periodo_actual

    # Obtiene la cantiad de facturas emitidas en el dia actual
    def get_total_facturas(self):
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")
        query = "SELECT count(*) FROM account_move WHERE TYPE = 'out_invoice' AND numero_corte is null " \
                "AND state = 'posted' AND company_id= " + str(self.company_gt.id) + " AND date = '" + varDate + "'"
        self.env.cr.execute(query)
        json_query = self.env.cr.dictfetchone()
        salida = str(json_query).replace("'", "\"")
        try:
            y = json.loads(salida)
            total_facturas = int(y["count"])
        except:
            total_facturas = 0
        return total_facturas

    # Calcula y establece el total según los métodos de pago visa o credo.
    def total_por_metodo_pago_visa_o_credo(self, id_diario):
        query = "select sum(AC2.amount_total) from account_move AC1 INNER JOIN account_move AC2 ON (AC1.NAME=AC2.REF) WHERE AC2.journal_id= " + str(
            id_diario) + " AND AC2.NUMERO_CORTE is null and AC2.type='entry' and AC2.state='posted' and AC1.STATE='posted' and AC1.type='out_invoice' AND AC2.company_id=AC1.company_id  AND AC2.company_id=" + str(
            self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según los métodos de pago deposito o Transferencia.
    def total_por_metodo_pago_deposito_o_transferencia(self, id_diario):
        query = " select sum(AC2.amount_total) from account_move AC1 INNER JOIN account_move " \
                " AC2 ON (AC1.NAME=AC2.REF) " \
                " WHERE AC2.journal_id=" + str(id_diario) + \
                " AND AC2.NUMERO_CORTE is null and AC2.type='entry' " \
                " AND AC2.state='posted' " \
                " AND AC1.state='posted' " \
                " AND AC1.type='out_invoice' " \
                " AND AC2.company_id=AC1.company_id " \
                " AND AC2.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según los métodos de pago quetzales.
    def total_por_metodo_pago_quetzales(self, id_diario_quetzales, id_diario_dolares):
        query1 = "select (( select sum(Pago.amount_total_signed- (SELECT COALESCE(Pago2.cambio,0)))  from account_move Factura" \
                 " INNER JOIN account_move Pago ON (Factura.NAME=Pago.REF) " \
                 " INNER JOIN account_payment Pago2 on (Pago.name=Pago2.move_name " \
                 " and Pago.ref=Pago2.communication and Pago.amount_total=Pago2.amount) " \
                 " WHERE Pago.journal_id=" + str(id_diario_quetzales) + " AND Pago.NUMERO_CORTE is null " \
                                                                        " and Pago.type='entry' and Pago.state='posted' " \
                                                                        " and Factura.STATE='posted' and Factura.type='out_invoice' " \
                                                                        " and Pago.company_id=Factura.company_id and Pago2.state='posted' " \
                                                                        " and Factura.company_id=Pago.company_id AND Pago.company_id=" + str(
            self.company_gt.id) + ")-"
        query2 = "( select (SELECT COALESCE(sum(Pago2.cambio),0))  from account_move Factura " \
                 " INNER JOIN account_move Pago ON (Factura.NAME=Pago.REF) " \
                 " INNER JOIN account_payment Pago2 on (Pago.name=Pago2.move_name " \
                 " and Pago.ref=Pago2.communication and Pago.amount_total=Pago2.amount) " \
                 " WHERE Pago.journal_id= " + str(id_diario_dolares) + \
                 " AND Pago.NUMERO_CORTE is null and Pago.type='entry'and Pago.state='posted' " \
                 " and Factura.STATE='posted' and Factura.type='out_invoice' " \
                 " and Pago.company_id=Factura.company_id and Pago2.state='posted' " \
                 " and Factura.company_id=Pago.company_id AND Pago.company_id= " + str(self.company_gt.id) + "))"
        query = query1 + query2
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["?column?"])
        except:
            total = 0
        return total

    # Calcula y establece el total según los métodos de pago dolares.
    def total_por_metodo_pago_dolares(self, id_diario):
        query = "select sum(AC2.amount_total_signed) from account_move " \
                " AC1 INNER JOIN account_move AC2 ON (AC1.NAME=AC2.REF) WHERE AC2.journal_id=" + str(id_diario) + " " \
                                                                                                                  " AND AC2.NUMERO_CORTE is null and AC2.type='entry' and AC2.state='posted' and AC1.STATE='posted' " \
                                                                                                                  " and AC1.type='out_invoice' AND AC2.company_id=AC1.company_id AND AC2.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según los métodos de pago exencion.
    def total_por_metodo_pago_exencion(self, id_diario):
        query = "select sum(AC2.amount_total) from account_move AC1 INNER JOIN account_move AC2  ON (AC1.NAME=AC2.REF) " \
                "WHERE AC2.journal_id=" + str(id_diario) + " AND AC2.NUMERO_CORTE is null " \
                                                           "AND AC2.type='entry' and AC2.state='posted' " \
                                                           "AND AC1.state='posted' and AC1.type='out_invoice'" \
                                                           "AND AC2.company_id=AC1.company_id AND AC2.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según los métodos de pago Retención.
    def total_por_metodo_pago_retencion(self, id_diario):
        query = "select sum(AC2.amount_total) from account_move AC1 INNER JOIN account_move AC2 ON (AC1.NAME=AC2.REF)" \
                " WHERE AC2.journal_id=" + str(id_diario) + " AND AC2.NUMERO_CORTE is null and AC2.type='entry' " \
                                                            "AND AC2.state='posted' and AC1.state='posted' and AC1.type='out_invoice'" \
                                                            "AND AC2.company_id=AC1.company_id AND AC2.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total



    # Calcula y establece el total según el método de pago "Al Credito".
    def total_facturas_credito(self):
        query = "select sum(Factura.amount_residual) from account_move Factura where Factura.numero_corte is null " \
                "and Factura.type = 'out_invoice' AND Factura.state='posted' AND Factura.invoice_payment_state = 'not_paid' " \
                "and Factura.invoice_payment_term_id > 1 AND Factura.company_id = " + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según el método de pago "Anticipo".
    def pagos_anticipo(self, diario):
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")
        query = " SELECT sum(B.credit) FROM account_payment A inner join account_move_line B ON A.id=B.payment_id " \
                "where A.state='posted' and B.parent_state='posted' and A.journal_id=B.journal_id and A.payment_type='inbound' " \
                "and A.partner_id=B.partner_id AND A.amount=B.credit and B.ref is null and B.account_internal_type='receivable' " \
                "and B.reconciled='false' and B.amount_residual<0 and A.payment_date='"+varDate+"' and B.company_id= " + str(
        self.company_gt.id) + "and A.journal_id = " + str(diario)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Calcula y establece el total según el método de pago "Anticipo Dolares".45231763
    def pagos_anticipo_dolares(self,):
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")
        query = " SELECT sum(B.credit) FROM account_payment A inner join account_move_line B ON A.id=B.payment_id " \
                "left join account_move C ON A.move_name=C.name where A.state='posted' " \
                "and C.numero_corte is null and B.parent_state='posted' " \
                "and A.journal_id=B.journal_id and A.payment_type='inbound' " \
                "and A.partner_id=B.partner_id AND (A.amount * (select rate from res_currency_rate where id=1) ) = B.credit " \
                "and B.ref is null and B.account_internal_type='receivable' " \
                "and B.reconciled='false' and B.amount_residual<0 and A.payment_date= '"+ str(varDate)+"' and B.company_id= 1 and A.journal_id = 106 "
        # print(query)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total


    # Obtiene y establece el total de voucher según el método de pago ya sea VisaNeto o CredoMatic.
    def get_total_voucher(self, id_diario):
        query = "select count(AC2.id) from account_move AC1 INNER JOIN account_move AC2 ON (AC1.NAME=AC2.REF) WHERE AC2.journal_id= " + str(
            id_diario) + " AND AC2.NUMERO_CORTE is null and AC2.type='entry' and AC2.state='posted' and AC1.STATE='posted' and AC1.type='out_invoice' AND AC2.company_id=AC1.company_id AND AC2.company_id=" + str(
            self.company_gt.id)
        self.env.cr.execute(query)
        json_query = self.env.cr.dictfetchone()
        salida = str(json_query).replace("'", "\"")
        try:
            y = json.loads(salida)
            total_voucher = int(y["count"])
        except:
            total_voucher = 0
        return total_voucher

    # Obtiene y establece el total de movimientos segun sea Deposito o Transferencia.
    def get_total_movimientos(self, id_diario):
        query = "select count(AC2.id) from account_move AC1 INNER JOIN account_move AC2 ON (AC1.NAME=AC2.REF)" \
                " WHERE AC2.journal_id=" + str(id_diario) + \
                " AND AC2.NUMERO_CORTE is null " \
                " AND AC2.type='entry'" \
                " AND AC2.state='posted'" \
                " AND AC1.STATE='posted' " \
                " AND AC1.type='out_invoice'" \
                " AND AC2.company_id=AC1.company_id " \
                " AND AC2.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_query = self.env.cr.dictfetchone()
        salida = str(json_query).replace("'", "\"")
        try:
            y = json.loads(salida)
            total_voucher = int(y["count"])
        except:
            total_voucher = 0
        return total_voucher






    # Metodo que se activa mediante el botón realizar corte z, donde colocar los valores calculados en las variables respectivas.
    def realizar_corte(self):

        if self.cant_total_visa == -1 or self.cs_inguat == -1 or self.cant_total_credo == -1 or self.cs_inguat_c == -1:
            raise ValidationError(
                _("Debe ingresar valores en los campos INGUAT y Total ")
            )
        else:
            self.cant_facturas = self.get_total_facturas()
            tasa = self.env['res.currency'].search([('name', '=', 'GTQ')])
            self.anticipo_en_dolares = self.pagos_anticipo_dolares()/ tasa.rate
            self.anticipo_dolares = self.pagos_anticipo_dolares()
            self.anticipo_visa = self.pagos_anticipo(107)
            self.anticipo_credo = self.pagos_anticipo(108)
            self.anticipo_deposito_transferencia = self.pagos_anticipo(104)
            self.monto_anticipo = self.anticipo_dolares + self.anticipo_visa + self.anticipo_credo + self.anticipo_deposito_transferencia
            self.cant_voucher_visa = self.get_total_voucher(10)
            self.cant_voucher_credo = self.get_total_voucher(11)
            self.cant_deposito_transferencia = self.get_total_movimientos(94)
            self.cant_efectivo = self.total_por_metodo_pago_quetzales(6, 12)
            self.cant_dolares = self.total_por_metodo_pago_dolares(12)  # Muestra el valor en quetzales
            self.dolares = round((self.cant_dolares / tasa.rate), 2)  # La cantidad en quetales la convierte a dolares
            self.cant_visa = self.total_por_metodo_pago_visa_o_credo(10)
            self.cant_credo = self.total_por_metodo_pago_visa_o_credo(11)
            self.cant_deposito_transferencia = self.total_por_metodo_pago_deposito_o_transferencia(94)
            self.cant_exencion = self.total_por_metodo_pago_exencion(112)
            self.cant_retencion = self.total_por_metodo_pago_retencion(113)
            self.cant_credito = self.total_facturas_credito()
            self.cant_anticipo = self.cant_anticipo_query()  # self.anticipo_efectivo+self.anticipo_dolares + self.anticipo_visa + self.anticipo_credo + self.anticipo_deposito_transferencia
            self.monto_total = self.cant_anticipo+self.cant_efectivo + self.cant_visa + self.cant_credo + self.cant_dolares + self.cant_credito + self.cant_deposito_transferencia + self.cant_exencion + self.cant_retencion
            self.saldo_pendiente_de_cobro = self.saldos_pendientes_x_cobrar()
            self.saldos_vencidos = self.saldos_vencidos_de_facturas()
            self.monto_boleta_ventas = self.total_por_metodo_pago_quetzales(6, 12)
            self.monto_boleta_dolares = float(self.total_por_metodo_pago_dolares(12) / tasa.rate)

            # self.cant_total_visa = self.anticipo_visa + self.cant_visa + self.cxc_visa
            # self.cant_total_credo = self.anticipo_credo + self.cant_credo + self.cxc_credo
            # Cantidad total de voucher de visa y de credo
            # self.cant_total_voucher_visa = 0#self.cant_voucher_visa + self.get_total_voucher(96) + self.vouchers_anticipos('visa')
            # self.cant_total_voucher_credo = 0#self.cant_voucher_credo + self.get_total_voucher(97) +self.vouchers_anticipos('credo')

            datos = self.env['account.move'].search(
                ['&', '&', '|', ('numero_corte', '=', False), ('numero_corte', '=', 0), ('state', '=', 'posted'), '|',
                 ('type', '!=', 'in_invoice'), ('type', '!=', 'in_refund')])
            for dato in datos:
                dato.numero_corte = self.num_corte
                pagos_anticipo = self.env['account_payment_widget_amount.payment_move_rel'].search([('factura_id', '=', dato.id)])
                for pago in pagos_anticipo:
                    pago.no_corte_pago_abono = self.num_corte

            self.ver_facturas_z(self.num_corte)
            self.repote_anticipos(self.num_corte)
            self.realizado = True
            self.conciliacion_targetas()
            self.reporte_cuentas_pendientes_pago()
            self.reporte_facturas_por_cobrar()
            self.corte_cxc()
            self.facturas_vencidas()
            self.reporte_facturas_credito_pagadas_hoy(self.num_corte)
            self.pd_efectivo = self.monto_quetzales
            self.pd_dolares = self.set_total_x_cuentas_detalle_ventas(12, self.num_corte)
            self.pd_visanet = self.set_total_x_cuentas_detalle_ventas(10, self.num_corte)
            self.pd_credomatic = self.set_total_x_cuentas_detalle_ventas(11, self.num_corte)
            self.pd_dep_transf = self.set_total_x_cuentas_detalle_ventas(94, self.num_corte)
            self.pd_credito = self.set_total_credito_detalle_ventas(self.num_corte)
            self.pd_retencion = self.set_total_x_cuentas_detalle_ventas(101, self.num_corte)
            self.pd_extencion = self.set_total_x_cuentas_detalle_ventas(100, self.num_corte)
            self.pd_total = float(self.pd_efectivo) + float(self.pd_dolares) + float(self.pd_visanet) + float(
                self.pd_credomatic) + float(self.pd_dep_transf) + float(self.pd_credito) + float(
                self.pd_extencion) + float(
                self.pd_retencion) + self.cant_anticipo
            self.pd_iva_pagar = self.set_total_x_cuentas_detalle_ventas_col_haber('2.01.02.01', self.num_corte)
            self.pd_inguat_pagar = self.set_total_x_cuentas_detalle_ventas_col_haber('2.01.02.09', self.num_corte)
            self.pd_hospedaje = self.set_total_x_cuentas_detalle_ventas_col_haber('4.01.06.01', self.num_corte)
            self.pd_piscinas = self.set_total_x_cuentas_detalle_ventas_col_haber('4.01.06.02', self.num_corte)
            self.pd_alimentacion = self.set_total_x_cuentas_detalle_ventas_col_haber('4.01.06.05', self.num_corte)
            self.pd_lavanderia = self.set_total_x_cuentas_detalle_ventas_col_haber('4.01.06.06', self.num_corte)
            self.pd_otros_servicios = self.set_total_x_cuentas_detalle_ventas_col_haber_otros(self.num_corte)
            self.ph_total = float(self.pd_hospedaje) + float(self.pd_lavanderia) + float(self.pd_piscinas) + float(
                self.pd_alimentacion) + float(self.pd_otros_servicios) + float(self.pd_iva_pagar) + float(
                self.pd_inguat_pagar)

            return "ok"

    # Método para retornar el valor de los anticipos.
    def cant_anticipo_query(self):
        # print("entro a calcular el anticipo")
        query = ("""
                   select sum(REL.MONTO_PAGO_ABONO) from account_payment_widget_amount_payment_move_rel REL
                        INNER JOIN account_move FACTURA 
                        ON (FACTURA.ID=REL.FACTURA_ID)
                        INNER JOIN account_payment ANTICIPO
                        ON (ANTICIPO.ID=REL.PAGO_ABONO_ID)
                        INNER JOIN account_move_line PAGO
                        ON (PAGO.PAYMENT_ID=REL.PAGO_ABONO_ID)
                        WHERE FACTURA.partner_id=PAGO.partner_id
                        AND ANTICIPO.ID=PAGO.PAYMENT_ID
                        AND ANTICIPO.MOVE_NAME=PAGO.MOVE_NAME
                        and FACTURA.company_id=PAGO.company_id
                        and PAGO.parent_state='posted'
                        and PAGO.account_internal_type='receivable' 
                        and PAGO.CREDIT>0 
                        and FACTURA.partner_id=ANTICIPO.partner_id
                        AND FACTURA.NUMERO_CORTE is null
                        and FACTURA.type='out_invoice'
                        and FACTURA.STATE='posted'
                        and ANTICIPO.state='posted'
                        and ANTICIPO.communication IS NULL 
                        and ANTICIPO.payment_type='inbound'
                        and ANTICIPO.state='posted'
                        AND ANTICIPO.journal_id in (104,106,107,108)
                        and FACTURA.company_id=1
                   """)
        self.env.cr.execute(query)
        query_result = self.env.cr.dictfetchall()
        # print(query_result)
        for x in query_result:
            suma = x["sum"]
            if suma is not None:
                return float(suma)
            # print(suma)
            else:
                return 0

    # Método que forma el link para redireccionar a una factura por medio del id.
    def link_factura(self, id):
         # Server
        url_factura = '/web#id=' + id + '&action=197&model=account.move&view_type=form&cids=1&menu_id=164'
        return url_factura

    # Método que forma el link para redireccionar a un pago por medio del id.
    def link_pago(self, id):
         # Server
        url_pago = '/web#id=' + id + '&action=171&model=account.payment&view_type=form&cids=1&menu_id=164'
        return url_pago

    # Método que muestra las facturas asociadas a un corte Z.
    def ver_facturas_z(self, num):
        facturas = self.env['account.move'].search([('numero_corte', '=', num),
                                                    ('company_id', '=', self.env.user.company_id.id),
                                                    ('type', '=', 'out_invoice')], order="id desc")
        variabletexto = ""
        lista_pagos_efectivo = ""
        lista_pagos_dolares = ""
        lista_pagos_tarjeta_visa = ""
        lista_pagos_tarjeta_credo = ""
        lista_pagos_deposito_trans = ""
        lista_pagos_anticipo_deposito_trans = ""
        lista_pagos_anticipo_dolares = ""
        lista_pagos_anticipo_visanet = ""
        lista_pagos_anticipo_credomatic = ""

        suma_pagos_efectivo = 0
        suma_pagos_dolares = 0
        suma_pago_tarjeta_visa = 0
        suma_pago_tarjeta_credo = 0
        suma_pago_deposito_trans = 0
        suma_pagos_anticipo_deposito_trans = 0
        suma_pagos_anticipo_dolares = 0
        suma_pagos_anticipo_visanet = 0
        suma_pagos_anticipo_credomatic = 0

        total_corte_factura = 0
        for factura in facturas:
            pagos_anticipo = self.env['account_payment_widget_amount.payment_move_rel'].search([('factura_id', '=', factura.id)])
            for rel_pago in pagos_anticipo:
                pago = self.env['account.payment'].search([('id', '=', rel_pago.pago_abono_id.id)])
                fact = self.env['account.move'].search([('id', '=', int(rel_pago.factura_id))])

                if pago.journal_id.id == 107:
                    lista_pagos_anticipo_visanet += '<tr><td><a href=' + self.link_pago(str(pago.name)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(fact.id)) + '">' + str(
                        fact.name) + '</a></td><td>' + str(
                        pago.payment_date.strftime("%d/%m/%Y")) + '</td><td>' + str(
                        fact.invoice_partner_display_name) + '</td><td>' +str(pago.num_autorizacion) + '</td><td>Q ' + str(
                        rel_pago.monto_pago_abono) + '</td><tr>'
                    suma_pagos_anticipo_visanet+=rel_pago.monto_pago_abono
                elif pago.journal_id.id == 108:
                    lista_pagos_anticipo_credomatic += '<tr><td><a href=' + self.link_pago(str(pago.name)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(fact.id)) + '">' + str(
                        fact.name) + '</a></td><td>' + str(
                        pago.payment_date.strftime("%d/%m/%Y")) + '</td><td>' + str(
                        fact.invoice_partner_display_name) + '</td><td>' +  str(pago.num_autorizacion) + '</td><td>Q ' + str(
                        rel_pago.monto_pago_abono) + '</td><tr>'
                    suma_pagos_anticipo_credomatic += rel_pago.monto_pago_abono
                elif pago.journal_id.id == 104:
                    lista_pagos_anticipo_deposito_trans += '<tr><td><a href=' + self.link_pago(str(pago.name)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(fact.id)) + '">' + str(
                        fact.name) + '</a></td><td>' + str(
                        pago.payment_date.strftime("%d/%m/%Y")) + '</td><td>' + str(
                        fact.invoice_partner_display_name) + '</td><td>' +'Boleta' + '</td><td>Q ' + str(
                        rel_pago.monto_pago_abono) + '</td><tr>'
                    suma_pagos_anticipo_deposito_trans += rel_pago.monto_pago_abono
                    self.procesar_cambio(pago.cambio, pago.name)
                elif pago.journal_id.id == 106:
                    lista_pagos_anticipo_dolares += '<tr><td><a href=' + self.link_pago(str(pago.name)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(fact.id)) + '">' + str(
                        fact.name) + '</a></td><td>' + str(
                        pago.payment_date.strftime("%d/%m/%Y")) + '</td><td>' + str(
                        fact.invoice_partner_display_name) + '</td><td>' +'Boleta' + '</td><td>Q ' + str(
                        rel_pago.monto_pago_abono) + '</td><tr>'
                    suma_pagos_anticipo_dolares += rel_pago.monto_pago_abono
                    self.procesar_cambio(pago.cambio, pago.name)


            pagos = self.env['account.payment'].search([('communication', '=', factura.name)])
            texto = ""
            boleta = ""

            for pago in pagos:
                fecha_pago = '{}/{}/{}'.format(pago.payment_date.day, pago.payment_date.month,
                                               pago.payment_date.year)
                if pago.boleta_deposito:
                    boleta = pago.boleta_deposito
                else:
                    boleta = 'PAGO EN EFECTIVO'

                if pago.journal_id.id == 6 and pago.state == 'posted':
                    texto += "EFECTIVO, "
                    lista_pagos_efectivo += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td>' \
                                              '<td>' + str(fecha_pago) + '</td><td>' + str(
                        factura.invoice_partner_display_name) + '</td><td>' + boleta + '</td><td>' + str(
                        self.format_num(pago.amount)) + '</td><tr>'

                    suma_pagos_efectivo += pago.amount
                elif pago.journal_id.id == 94 and pago.state == 'posted':
                    texto += "Deposito/Transferencia, "
                    lista_pagos_deposito_trans += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td><td>' + str(
                        fecha_pago) + '</td><td>' + str(
                        factura.invoice_partner_display_name) + '</td><td>' + boleta + '</td><td>Q ' + str(
                        self.format_num(pago.amount)) + '</td><tr>'
                    suma_pago_deposito_trans += pago.amount
                    self.procesar_cambio(pago.cambio, pago.name)

                elif pago.journal_id.id == 10 and pago.state == 'posted':
                    texto += "VISA, "
                    lista_pagos_tarjeta_visa += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td><td>' + str(
                        fecha_pago) + '</td><td>' + str(factura.invoice_partner_display_name) + '</td><td>' + str(
                        pago.num_tarjeta) + '</td><td>' + str(pago.num_autorizacion) + ' </td><td>' + str(
                        self.format_num(pago.amount)) + '</td><tr>'
                    suma_pago_tarjeta_visa += pago.amount
                elif pago.journal_id.id == 11 and pago.state == 'posted':
                    texto += "CREDO, "
                    lista_pagos_tarjeta_credo += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td><td>' + str(
                        fecha_pago) + '</td><td>' + str(factura.invoice_partner_display_name) + '</td><td>' + str(
                        pago.num_tarjeta) + '</td><td>' + str(pago.num_autorizacion) + ' </td><td>Q ' + str(
                        pago.amount) + '</td><tr>'
                    suma_pago_tarjeta_credo += pago.amount
                elif pago.journal_id.id == 12 and pago.state == 'posted':
                    texto += "DOLARES, "
                    lista_pagos_dolares += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td><td>' + str(
                        fecha_pago) + '</td><td>' + str(
                        factura.invoice_partner_display_name) + '</td><td>' + boleta + '</td><td>$ ' + str(
                        pago.amount) + '</td><tr>'
                    suma_pagos_dolares += pago.amount
                    self.procesar_cambio(pago.cambio,pago.name)

                elif pago.journal_id.id == 107:
                    texto += "Pago Anticipado Visanet, "
                    lista_pagos_anticipo_visanet += '<tr><td><a href=' + self.link_pago(str(pago.id)) + '>' + str(
                        pago.name) + '</td>' + '<td><a href="' + self.link_factura(str(factura.id)) + '">' + str(
                        pago.communication) + '</a></td><td>' + str(
                        fecha_pago) + '</td><td>' + str(
                        factura.invoice_partner_display_name) + '</td><td>' + boleta + '</td><td>$ ' + str(
                        pago.amount) + '</td><tr>'
                    suma_pagos_dolares += pago.amount
                else:
                    texto += "PENDIENTE DE PAGO"
            total_corte_factura += factura.amount_total_signed
            if str(factura.state) == 'posted':
                estado_factura = 'Publicada'
            else:
                estado_factura = 'No Publicada'

            if str(factura.invoice_payment_state) == 'paid':
                estado_pago = 'Cancelada'
            else:
                estado_pago = 'Credito'

            fecha = '{}/{}/{}'.format(factura.invoice_date.day, factura.invoice_date.month, factura.invoice_date.year)

            variabletexto += \
                "<tr>" \
                "<td><a href=" + self.link_factura(str(factura.id)) + ">" + str(factura.name) + "</a></td>" + \
                "<td>" + str(factura.inf_numero_factura) + "</td>" + \
                "<td>" + str(factura.invoice_partner_display_name) + "</td>" + \
                "<td>" + str(factura.partner_id.vat) + "</td>" + \
                "<td>" + fecha + "</td>" + \
                "<td align=\"right\">" + str(self.format_num(factura.amount_total_signed)) + "</td><td align=\"center\">" + str(estado_pago) + "</td></tr>"

        encabezado = '<table class="table table-condensed table-sm">' \
                     '<thead>' \
                     '<tr>' \
                     '<td>Numero</td>' \
                     '<td>No. Fel</td>' \
                     '<td>Cliente</td>' \
                     '<td>Nit</td>' \
                     '<td>Fecha</td>' \
                     '<td>Total</td>' \
                     '<td>Estado</td>' \
                     '</tr>' \
                     '</thead>'
        total = '<td></td><td></td><td></td><td></td><td>Total</td><td align="right"><strong>'+str(self.format_num(total_corte_factura)) + '</strong></td><td></td>'
        self.facturas_corte = encabezado + variabletexto + total + '</table>'
        encabezado = '<tr><td>Ref. Pago</td> <td>Factura</td> <td>Fecha</td> <td>Cliente</td> <td>Referencia</td> <td>Monto</td></tr>'
        encabezado_tarjeta = '<tr><td>Ref. Pago</td> <td>Factura</td> <td>Fecha</td> <td>Cliente</td> <td>No. Tarjeta</td> <td>No. Autorización</td> <td>Monto</td></tr>'
        total_pago_efectivo = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pagos_efectivo)) + '</td></tr>'
        total_pago_dolares = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>$ ' + str(
            self.format_num(suma_pagos_dolares)) + '</td></tr>'
        total_pago_visa = '<tr><td></td><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pago_tarjeta_visa)) + '</td></tr>'
        total_pago_credo = '<tr><td></td><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pago_tarjeta_credo)) + '</td></tr>'
        total_pago_deposito_trans = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pago_deposito_trans)) + '</td></tr>'

        total_pago_anticipo_deposito_trans = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pagos_anticipo_deposito_trans)) + '</td></tr>'
        total_pago_anticipo_dolares = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pagos_anticipo_dolares)) + '</td></tr>'
        total_pago_anticipo_visanet = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pagos_anticipo_visanet)) + '</td></tr>'
        total_pago_anticipo_credomatic = '<tr><td></td><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            self.format_num(suma_pagos_anticipo_credomatic)) + '</td></tr>'

        self.tabla_pagos_efectivo = '<h2>EFECTIVO</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_efectivo + total_pago_efectivo + '</table>'
        self.tabla_pagos_dolares = '<h2>DOLARES</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_dolares + total_pago_dolares + '</table>'
        self.tabla_pagos_tarjeta_visa = '<h2>VISANET</h2><table class="table table-condensed table-sm">' + encabezado_tarjeta + lista_pagos_tarjeta_visa + total_pago_visa + '</table>'
        self.tabla_pagos_tarjeta_credo = '<h2>CREDOMATIC</h2><table class="table table-condensed table-sm">' + encabezado_tarjeta + lista_pagos_tarjeta_credo + total_pago_credo + '</table>'
        self.tabla_pagos_deposito_trans = '<h2>DEPOSITOS/TRANSFERENCIA</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_deposito_trans + total_pago_deposito_trans + '</table>'
        self.tabla_pagos_anticipo_deposito_trans = '<h2>ANTICIPO DEPOSITOS/TRANSFERENCIA</h2><table class="table table-condensed table-sm">' + encabezado +lista_pagos_anticipo_deposito_trans+ total_pago_anticipo_deposito_trans + '</table>'
        self.tabla_pagos_anticipo_dolares = '<h2>ANTICIPOS DOLARES</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_anticipo_dolares + total_pago_anticipo_dolares + '</table>'
        self.tabla_pagos_anticipo_visanet = '<h2>ANTICIPOS VISANET</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_anticipo_visanet + total_pago_anticipo_visanet +'</table>'
        self.tabla_pagos_anticipo_credomatic = '<h2>ANTICIPOS CREDOMATIC</h2><table class="table table-condensed table-sm">' + encabezado + lista_pagos_anticipo_credomatic + total_pago_anticipo_credomatic + '</table>'
        return 'ok'

    # Método que muestra las facturas pagadas "Cuentas al Credito".
    def ver_facturas_pagadas(self):
        facturas = self.env['account.move'].search(
            [('company_id', '=', self.env.user.company_id.id), ('type', '=', 'out_invoice')], order="id desc")
        variabletexto = ""
        for factura in facturas:
            if factura.journal_id == 6:
                pago = "Efectivo"
            else:
                pago = "NA"

            variabletexto += \
                "<tr>" \
                "<td><a href=" + self.link_factura(str(factura.id)) + ">" + str(factura.name) + "</a></td>" + \
                "<td>" + str(factura.invoice_partner_display_name) + "</td>" + \
                "<td>" + str(factura.invoice_date) + "</td>" + \
                "<td>" + pago + "</td>" + \
                "<td>" + str(self.format_num(factura.amount_total_signed)) + \
                "</td></tr>"

        encabezado = '<table class="table table-condensed">' \
                     '<thead>' \
                     '<tr>' \
                     '<td>Numero</td>' \
                     '<td>Cliente</td>' \
                     '<td>Fecha</td>' \
                     '<td>Tipo Pago</td>' \
                     '<td>Total</td>' \
                     '</thead></tr>'
        self.facturas_corte = encabezado + variabletexto + ' </table>'
        return 'ok'




    def reporte_cuentas_pendientes_pago(self):
        contenido_tabla = ''
        total = 0
        registro = self.env['account.move'].search(
            ['&', '&', '&', '&', ('type', '=', 'out_invoice'), ('state', '=', 'posted'),
             ('invoice_payment_state', '=', 'not_paid'), ('invoice_payment_term_id', '>', 1), ('company_id', '=', 1)])
        for dato in registro:
            contenido_tabla += '<tr>' + '<td>' + str(dato.name) + '</td>' + '<td>' + str(
                dato.date.strftime("%d/%B/%Y")) + '</td>' + '<td>' + str(dato.invoice_partner_display_name) + '</td>' \
                '<td>' + str(dato.inf_serie) + '<td>' + str(dato.inf_numero_factura) + '</td><td>' + str(
                '{:<}{:=12,.2f}'.format('Q',dato.amount_residual)) + '</td><td>' + str(dato.invoice_date_due.strftime("%d/%B/%Y")) + '</td></tr>'
            total += dato.amount_residual
        encabezado_tabla = '<tr><th>Documento</th><th>Emision</th><th>Cliente</th><th>Serie</th><th>No. FEL</th><th>Total</th><th>Vence</th></tr>'

        total_a_pagar = '<tr><th></th><th></th><th></th><th></th><th>Total</th><th>'+str('{:<}{:=12,.2f}'.format('Q',total))+'</th></tr>'
        self.cuentas_x_cobrar = '<table class="table table-condensed"><tbody>' + encabezado_tabla + contenido_tabla + total_a_pagar+'</tbody></table>'

    def repote_anticipos(self, no_corte):
        contenido_tabla = ''
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")
        anticipos = self.env['account.payment'].search([ '&','&','&','&',('partner_type', '=', 'customer'),('communication', '=', False),
                                                         ('payment_type', '=', 'inbound'),('state', '=', 'posted'),
                                                         ('payment_method_id', '=', 1),('payment_date', '=', varDate)])
        for anticipo in anticipos:
            contenido_tabla += '<tr><td>' + anticipo.name + '</td><td>' + str(anticipo.payment_date.strftime(
                "%d/%B/%Y")) + '</td><td>' + anticipo.partner_id.name + '</td><td>' + anticipo.journal_id.name + '</td><td>' + str(
                anticipo.amount) + '</td> <td>'+anticipo.informacion+'</td></tr>'

        encabezado_tabla = '<tr><th>Documento</th><th>Fecha</th><th>Cliente</th><th>Tipo de pago</th><th>Monto (Quetzales)</th><th>Boleta</th></tr>'
        total_tabla = '<tr><td></td><td></td><td></td><td><strong>Total</strong></td><td>' + str(
            '{:<}{:=12,.2f}'.format('Q',self.monto_anticipo))+ '</td><td></td></tr>'
        self.tabla_anticipos = '<table class="table table-condensed"><tbody>' + encabezado_tabla + contenido_tabla + total_tabla + '</tbody></table>'

    def reporte_facturas_por_cobrar(self):
        contenido_tabla = ''
        monto_final = 0
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")
        facturas_vencidas = self.env['account.move'].search(['&',
                                                             ('amount_residual', '>', 0),
                                                             '&', '&',
                                                             ('invoice_payment_state', '=', 'not_paid'),
                                                             ('state', '=', 'posted'),
                                                             '&', '&',
                                                             ('type', '=', 'out_invoice'),
                                                             ('invoice_payment_term_id', '>', 1),
                                                             '&',
                                                             ('invoice_date_due', '<', varDate),
                                                             ('company_id', '=', 1)])

        ('type', '=', 'out_refund')

        for factura in facturas_vencidas:
            contenido_tabla += '<tr>' + '<td>' + factura.name + '</td>' + '<td>' + factura.partner_id.name + '</td>' + '<td>' + str(
                factura.invoice_date_due.strftime("%d/%B/%Y")) + '</td>' + '<td>' + str(
                factura.inf_serie) + '<td>' + str(factura.inf_numero_factura) + '</td>' + '<td>Q' + str(
                factura.amount_total) + '</td>' + '</td>' + '</tr>'
            monto_final += factura.amount_total
        encabezado = '<tr><th>Factura</th><th>Cliente</th><th>Fecha de Vencimiento</th><th>Serie</th><th>No. Factura</th><th>Monto</th></tr>'
        total = '<tr><th></th><th></th><th></th><th></th><th>Total: </th><th>Q' + str(monto_final) + '</th></tr>'
        self.tabla_facturas_pendientes = '<table class="table table-condensed"><tbody>' + encabezado + contenido_tabla + str(total)+'</tbody></table>'

    def reporte_facturas_credito_pagadas_hoy(self,corte):
        monto_final = 0
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")

        sql = 'SELECT account_move.name, account_move.invoice_partner_display_name,account_move.inf_serie,' \
              'account_move.inf_numero_factura, account_payment.amount,account_payment.payment_date FROM account_move' \
              ' INNER JOIN account_payment ON account_move.name = account_payment.communication ' \
              'and account_move.company_id = 1 ' \
              'and account_move.state=\'posted\' ' \
              'and account_move.type=\'out_invoice\' ' \
              'and account_move.numero_corte < '+str(corte)+ ' and account_payment.payment_date = \''+str(varDate)+'\''
        self.env.cr.execute(sql)
        json_data = self.env.cr.dictfetchall()
        tabla = ""
        for dato in json_data:
            tabla += '<tr><td>'+str(dato['name'])+'</td><td>'+str(dato['invoice_partner_display_name'])+'</td><td>'+str(dato['inf_serie'])+'</td><td>'+str(dato['inf_numero_factura'])+'</td><td>'+str(self.format_num(dato['amount']))+'</tr>'
            monto_final += dato['amount']
        total = '<tr><th></th><th></th><th></th><th>Total: </th><th>' + str(self.format_num(monto_final)) + '</th></tr>'
        self.tabla_facturas_pagadas_credito = '<table class="table table-condensed"><tbody>'+tabla+total+'</tbody></table>'


    def facturas_vencidas(self,):
        contenido_tabla = ''
        monto_final = 0
        varDate = datetime.datetime.now(pytz.timezone("America/Guatemala")).strftime("%Y-%m-%d")


        facturas_vencidas = self.env['account.move'].search(['&',
                                                             ('invoice_payment_state', '=', 'not_paid'),
                                                             '&', '&',
                                                             ('state', '=', 'posted'),
                                                             ('type', '=', 'out_invoice'),
                                                             '&','&',
                                                             ('invoice_date_due', '=', varDate),
                                                             ('company_id', '=', 1),
                                                             '&',
                                                             ('numero_corte', '!=', 0),
                                                             ('name', '!=', False)])

        for factura in facturas_vencidas:
            contenido_tabla += '<tr>' + '<td>' + factura.name + '</td>' + '<td>' + factura.partner_id.name + '</td>' + '<td>' + str(
                factura.invoice_date_due.strftime("%d/%B/%Y")) + '</td>' + '<td>' + str(
                factura.inf_serie) + '<td>' + str(factura.inf_numero_factura) + '</td>' + '<td>Q' + str(
                factura.amount_total) + '</td>' + '</td>' + '</tr>'
            monto_final += factura.amount_total

        encabezado = '<tr><th>Factura</th><th>Cliente</th><th>Fecha de Vencimiento</th><th>Serie</th><th>No. Factura</th><th>Monto</th></tr>'
        total = '<tr><th></th><th></th><th></th><th></th><th>Total: </th><th>Q '+str(monto_final)+'</th></tr>'
        self.tabla_facturas_vencidas = '<table class="table table-condensed"><tbody>' + encabezado + contenido_tabla + total + '</tbody></table>'


    def test(self):
        # create message object instance
        msg = MIMEMultipart()
        message = "Thank you"
        # setup the parameters of the message
        password = "gomezmorales11"
        msg['From'] = "gonibal15@gmail.com"
        msg['To'] = "informatica2sarita@gmail.com"
        msg['Subject'] = "Subscription"
        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        # create server
        server = smtplib.SMTP('smtp.gmail.com: 587')
        server.starttls()
        # Login Credentials for sending the mail
        server.login(msg['From'], password)
        # send the message via the server.
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()
        print("successfully ")

    # Método establece el nombre de usuario logueado.
    @api.model
    def _nombre_del_usuario(self):
        return str(self.env.user.name)

    # Método que calcula el corte z temporalmente.
    @api.onchange('corte_x')
    def realizar_corte_x(self):
        if self.corte_x:
            self.cant_facturas = self.get_total_facturas()
            tasa = self.env['res.currency'].search([('name', '=', 'GTQ')])
            self.monto_total = 0
            self.cant_voucher_visa = self.get_total_voucher(10)
            self.cant_voucher_credo = self.get_total_voucher(11)
            self.cant_deposito_transferencia = self.get_total_movimientos(94)
            self.cant_efectivo = self.total_por_metodo_pago_quetzales(6, 12)
            self.cant_dolares = self.total_por_metodo_pago_dolares(12)
            self.quetzales = round((self.cant_dolares / tasa.rate), 2)
            self.cant_visa = self.total_por_metodo_pago_visa_o_credo(10)
            self.cant_credo = self.total_por_metodo_pago_visa_o_credo(11)
            self.cant_deposito_transferencia = self.total_por_metodo_pago_deposito_o_transferencia(94)
            self.cant_credito = self.total_facturas_credito()
            self.monto_total = self.cant_efectivo + self.cant_visa + self.cant_credo + self.cant_dolares + self.cant_credito + self.cant_deposito_transferencia + self.cant_exencion + self.cant_retencion + self.cant_anticipo
            self.monto_boleta_ventas = self.total_por_metodo_pago_quetzales(6, 12)
            self.monto_boleta_dolares = float(self.total_por_metodo_pago_dolares(12) / tasa.rate)
            self.cant_exencion = self.total_por_metodo_pago_exencion(112)
            self.cant_retencion = self.total_por_metodo_pago_retencion(113)

            tasa = self.env['res.currency'].search([('name', '=', 'GTQ')])
            self.anticipo_en_dolares = self.pagos_anticipo_dolares() / tasa.rate
            self.anticipo_dolares = self.pagos_anticipo_dolares()
            self.anticipo_visa = self.pagos_anticipo(107)
            self.anticipo_credo = self.pagos_anticipo(108)
            self.anticipo_deposito_transferencia = self.pagos_anticipo(104)
            self.monto_anticipo = self.anticipo_dolares + self.anticipo_visa + self.anticipo_credo + self.anticipo_deposito_transferencia
            self.cant_anticipo = 0

        else:
            self.cant_efectivo = 0
            self.cant_dolares = 0
            self.cant_visa = 0
            self.cant_credo = 0
            self.cant_deposito_transferencia = 0
            self.cant_credito = 0
            self.monto_total = 0
            self.cant_facturas = 0
            self.cant_voucher_visa = 0
            self.cant_voucher_credo = 0
            self.monto_boleta_ventas = 0
            self.monto_boleta_dolares = 0
            self.cant_exencion = 0
            self.cant_retencion = 0
            self.cant_anticipo = 0
            self.anticipo_efectivo = 0
            self.anticipo_dolares = 0
            self.anticipo_visa = 0
            self.anticipo_credo = 0
            self.anticipo_deposito_transferencia = 0
            self.monto_anticipo = 0

    # Método que calcula el total en quetzales corte z Menu Abonos Credito.
    def corte_z_credito_quetzales(self, id_diario_quetzales, id_diario_dolares, num_corte):
        query = "select (( select sum(Pago.amount_total_signed-(SELECT COALESCE(Pago2.cambio,0))) " \
                " from account_move Factura INNER JOIN account_move Pago ON (Factura.NAME=Pago.REF)" \
                " INNER JOIN account_payment Pago2 on (Pago.name=Pago2.move_name and Pago.ref=Pago2.communication " \
                " and Pago.amount_total=Pago2.amount) WHERE Pago.journal_id=" + str(
            id_diario_quetzales) + " and Pago.type='entry' " \
                                   " and Pago.state='posted' " \
                                   " and Factura.STATE='posted' " \
                                   " and Factura.type='out_invoice'" \
                                   " and Pago.company_id=Factura.company_id " \
                                   " and Pago2.state='posted'" \
                                   " and Factura.company_id=Pago.company_id AND Pago.NUMERO_CORTE = " + str(
            num_corte) + "" \
                         " AND Pago.company_id=" + str(self.company_gt.id) + " )-"

        query2 = "(select sum((SELECT COALESCE(Pago2.cambio,0))) from account_move Factura " \
                 " INNER JOIN account_move Pago ON (Factura.NAME=Pago.REF) " \
                 " INNER JOIN account_payment Pago2 on (Pago.name=Pago2.move_name " \
                 " and Pago.ref=Pago2.communication and Pago.amount_total=Pago2.amount) " \
                 " WHERE Pago.journal_id= " + str(id_diario_dolares) + " " \
                                                                       " and Pago.type='entry' and Pago.state='posted' " \
                                                                       " and Factura.STATE='posted' " \
                                                                       " and Factura.type='out_invoice' " \
                                                                       " and Pago.company_id=Factura.company_id " \
                                                                       " and Pago2.state='posted' " \
                                                                       " and Factura.company_id=Pago.company_id " \
                                                                       " AND Pago.NUMERO_CORTE =" + str(
            num_corte) + " AND Pago.company_id=" + str(self.company_gt.id) + ")) As total"
        self.env.cr.execute(query + query2)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["total"])
        except:
            total = 0
        return total

    # Método que calcula el total corte z Menu Abonos Credito.
    def corte_z_credito_visa_credo(self, id_diario, num_corte):
        query = "select sum(Pago.amount_total) from account_move Pago INNER JOIN account_move Factura" \
                " ON (Factura.NAME=Pago.REF) WHERE Pago.state='posted'  and Pago.type='entry' and Factura.state='posted'" \
                " and Factura.type='out_invoice' and Factura.numero_corte<Pago.numero_corte and Pago.company_id = Factura.company_id " \
                "and Pago.journal_id=" + str(id_diario) + " and Pago.NUMERO_CORTE = " + str(
            num_corte) + "and Pago.company_id =" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Método que calcula el total dolares corte z Menu Abonos Credito.
    def corte_z_credito_dolares(self, id_diario, num_corte):
        query = "select  sum(Pago.amount_total_signed) from account_move Pago INNER JOIN account_move Factura" \
                " ON (Factura.NAME=Pago.REF) WHERE Pago.state='posted'  and Pago.type='entry' and Factura.state='posted'" \
                " and Factura.type='out_invoice' and Factura.numero_corte<Pago.numero_corte and Pago.company_id = Factura.company_id " \
                "and Pago.journal_id=" + str(id_diario) + " and Pago.NUMERO_CORTE = " + str(
            num_corte) + "and Pago.company_id =" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Método que calcula el total deposito/transferencia z Menu Abonos Credito.
    def corte_z_credito_deposito_transferencia(self, id_diario, num_corte):
        query = "select sum(Pago.amount_total) from account_move Pago INNER JOIN account_move Factura" \
                " ON (Factura.NAME=Pago.REF) WHERE Pago.state='posted'  and Pago.type='entry' and Factura.state='posted'" \
                " and Factura.type='out_invoice' and Factura.numero_corte<Pago.numero_corte and Pago.company_id = Factura.company_id " \
                "and Pago.journal_id=" + str(id_diario) + " and Pago.NUMERO_CORTE = " + str(
            num_corte) + " and Pago.company_id = " + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    def corte_cxc(self):
        self.cxc_total = 0
        self.cxc_efectivo = self.corte_z_credito_quetzales(95, 98, self.num_corte)
        self.cxc_dolares = self.corte_z_credito_dolares(98, self.num_corte)
        self.cxc_visa = self.corte_z_credito_visa_credo(96, self.num_corte)
        self.cxc_credo = self.corte_z_credito_visa_credo(97, self.num_corte)
        self.cxc_deposito = self.corte_z_credito_deposito_transferencia(99, self.num_corte)
        self.cxc_exencion = self.corte_z_credito_deposito_transferencia(102, self.num_corte)
        self.cxc_retencion = self.corte_z_credito_deposito_transferencia(103, self.num_corte)
        self.cxc_total = self.cxc_dolares + self.cxc_visa + self.cxc_credo + self.cxc_deposito+self.cxc_exencion+self.cxc_retencion

    # Abonos Creditos
    def realizar_corte_cxc(self):
        self.cxc_total = 0
        # self.cxc_efectivo = self.corte_z_credito_quetzales(95, 98, self.num_corte)
        self.cxc_dolares = self.corte_z_credito_dolares(98, self.num_corte)
        self.cxc_visa = self.corte_z_credito_visa_credo(96, self.num_corte)
        self.cxc_credo = self.corte_z_credito_visa_credo(97, self.num_corte)
        self.cxc_deposito = self.corte_z_credito_deposito_transferencia(99, self.num_corte)
        self.cxc_total = self.cxc_dolares + self.cxc_visa + self.cxc_credo + self.cxc_deposito
        return 'Ok'

    # Detalle de Ventas
    def set_total_x_cuentas_detalle_ventas(self, journal_id, no_corte):
        query = "select  sum(FacturaLinea.debit)  from account_move_line FacturaLinea " \
                "inner join account_move FacturaPago on (FacturaPago.id=FacturaLinea.move_id) " \
                "inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                "inner join account_move Factura on (Factura.name=FacturaPago.ref) " \
                "where FacturaPago.state='posted' and FacturaPago.numero_corte=Factura.numero_corte " \
                "and FacturaPago.type='entry' and FacturaLinea.parent_state='posted'" \
                " and FacturaLinea.account_internal_type in ('liquidity','other','receivable') " \
                "and FacturaLinea.company_id=FacturaPago.company_id and FacturaLinea.company_id=Cuenta.company_id " \
                "and FacturaLinea.company_id=1 and FacturaPago.numero_corte= " + str(
            no_corte) + " and FacturaLinea.debit >0 and FacturaPago.journal_id= " + str(
            journal_id) + " group by Cuenta.code,Cuenta.name"
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    def set_total_credito_detalle_ventas(self, no_corte):
        query = "select sum(Factura.amount_residual) from account_move Factura where  Factura.type='out_invoice' " \
                "and Factura.state='posted' and Factura.invoice_payment_state = 'not_paid' " \
                "and Factura.invoice_payment_term_id>1 and Factura.company_id= " + str(self.company_gt.id) + \
                "and Factura.numero_corte =" + str(no_corte)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Detalle de Ventas Haber
    def set_total_x_cuentas_detalle_ventas_col_haber(self, no_cuenta, no_corte):
        query = "select sum(FacturaLinea.credit) from account_move_line FacturaLinea" \
                " inner join account_move Factura on (Factura.id=FacturaLinea.move_id)" \
                " inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id)" \
                " where Factura.state='posted' and Factura.type='out_invoice'" \
                " and FacturaLinea.parent_state='posted' and FacturaLinea.account_internal_type='other'" \
                " and FacturaLinea.company_id=Factura.company_id and FacturaLinea.company_id=Cuenta.company_id " \
                "and FacturaLinea.company_id=" + str(self.company_gt.id) + " and Factura.numero_corte=" + str(
            no_corte) + " and cuenta.code='" + str(
            no_cuenta) + "' group by Cuenta.code,Cuenta.name HAVING sum(FacturaLinea.credit)>0"
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    def set_total_x_cuentas_detalle_ventas_col_haber_otros(self, no_corte):
        query = "select  sum(FacturaLinea.credit) from account_move_line FacturaLinea " \
                "inner join account_move Factura on (Factura.id=FacturaLinea.move_id)" \
                " inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                "where Factura.state='posted' and Factura.type='out_invoice' " \
                "and FacturaLinea.parent_state='posted'" \
                " and FacturaLinea.account_internal_type='other' " \
                "and FacturaLinea.company_id=Factura.company_id " \
                "and FacturaLinea.company_id=Cuenta.company_id " \
                "and FacturaLinea.company_id= " + str(self.company_gt.id) + " and Factura.numero_corte= " + str(
            no_corte) + " and cuenta.code not in('2.01.02.01','2.01.02.09','4.01.06.01','4.01.06.02','4.01.06.05','4.01.06.06','7.02.08.21')"
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    # Partida de pestaña Ventas por Clase.
    def columna_haber(self, num_corte):
        query = "select Cuenta.code \"Cuenta\",Cuenta.name \"Nombre de la Cuenta\", " \
                "sum(FacturaLinea.credit) \"Haber\" from account_move_line FacturaLinea " \
                "inner join account_move Factura on (Factura.id=FacturaLinea.move_id)" \
                " inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                "where Factura.state=\'posted\' and Factura.type=\'out_invoice\' " \
                "and FacturaLinea.parent_state=\'posted\' " \
                "and FacturaLinea.account_internal_type=\'other\' " \
                "and FacturaLinea.company_id = Factura.company_id  " \
                "and FacturaLinea.company_id=Cuenta.company_id " \
                "and FacturaLinea.company_id = " + str(self.company_gt.id) + " and Factura.numero_corte= " \
                + str(num_corte) + " group by Cuenta.code,Cuenta.name HAVING sum(FacturaLinea.credit) > 0"
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchall()
        col_haber = ''
        for item in json_data:
            registro = str(item).replace("'", "\"").replace("}", "</td></tr>").replace("{", "<tr><td>").replace(",",
                                                                                                                "</td><td>").replace(
                "\"Cuenta\":", "").replace("\"Nombre de la Cuenta\":", "").replace("\"Haber\":", "").replace("\"", "")
            col_haber += registro
        return col_haber

    def suma_haber(self, num_corte):
        query = "select  sum(FacturaLinea.credit) \"Haber\" from account_move_line FacturaLinea " \
                "inner join account_move Factura on (Factura.id=FacturaLinea.move_id) " \
                "inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                "where Factura.state='posted' " \
                "and Factura.type='out_invoice' " \
                "and FacturaLinea.parent_state='posted' " \
                "and FacturaLinea.account_internal_type='other' " \
                "and FacturaLinea.company_id=Factura.company_id " \
                "and FacturaLinea.company_id=Cuenta.company_id " \
                "and FacturaLinea.company_id=" + str(self.company_gt.id) + \
                "and Factura.numero_corte=" + str(num_corte)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["Haber"])
        except:
            total = 0
        return total

    # Metodo que retorna las cantidades de los metodos de pago
    def columna_debe(self, num_corte):
        query = "select Cuenta.code \"Cuenta\", Cuenta.name \"Nombre de la Cuenta\", " \
                " sum(FacturaLinea.debit) \"Debe\" from account_move_line FacturaLinea " \
                " inner join account_move FacturaPago on (FacturaPago.id=FacturaLinea.move_id) " \
                " inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                " inner join account_move Factura on (Factura.name=FacturaPago.ref) " \
                " where FacturaPago.state='posted' " \
                " and FacturaPago.numero_corte=Factura.numero_corte " \
                " and FacturaPago.type='entry' " \
                " and FacturaLinea.parent_state='posted' " \
                " and FacturaLinea.account_internal_type in ('liquidity','other') " \
                " and FacturaLinea.company_id=FacturaPago.company_id" \
                " and FacturaLinea.company_id=Cuenta.company_id " \
                " and FacturaPago.journal_id !=6 " \
                " and FacturaLinea.company_id=" + str(self.company_gt.id) + \
                " and FacturaPago.numero_corte=" + str(num_corte) \
                + " group by Cuenta.code,Cuenta.name"
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchall()
        col_debe = ''
        for item in json_data:
            registro = str(item).replace("'", "\"").replace("}", "</td></tr>").replace("{", "<tr><td>").replace(",",
                                                                                                                "</td><td>").replace(
                "\"Cuenta\":", "").replace("\"Nombre de la Cuenta\":", "").replace("\"Debe\":", "").replace("\"", "")
            col_debe += registro
            self.cant_debe.append(str(item))
        return col_debe

    # Coloca el monto total de facturas al credito que se emitieron en el dia actual.
    def complemento_columna_debe(self, num_corte):
        query = "select sum(Factura.amount_residual) \"credito\" from account_move Factura " \
                "where  Factura.type='out_invoice' and Factura.invoice_payment_state = 'not_paid'" \
                "and Factura.invoice_payment_term_id>1 and Factura.company_id=" + str(self.company_gt.id) + \
                "and Factura.numero_corte = " + str(num_corte)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            col_deb = float(valor["credito"])
        except:
            col_deb = 0
        return col_deb

    # Realiza la sumatoria de la columan del Debe de la partida de Ventas en el Corte Z
    def suma_debe(self, num_corte):
        query1 = "select (select  sum(FacturaLinea.debit) \"Debe\" from account_move_line FacturaLinea " \
                 "inner join account_move FacturaPago on (FacturaPago.id=FacturaLinea.move_id) " \
                 "inner join account_account Cuenta on (Cuenta.id=FacturaLinea.account_id) " \
                 "inner join account_move Factura on (Factura.name=FacturaPago.ref) " \
                 "where FacturaPago.state='posted' " \
                 "and FacturaPago.numero_corte=Factura.numero_corte " \
                 "and FacturaPago.type='entry' " \
                 "and FacturaLinea.parent_state='posted' " \
                 "and FacturaLinea.account_internal_type in ('liquidity','other') " \
                 "and FacturaLinea.company_id=FacturaPago.company_id " \
                 "and FacturaLinea.company_id=Cuenta.company_id " \
                 "and FacturaLinea.company_id= " + str(self.company_gt.id) + " " + "  \
                  and FacturaPago.numero_corte = " + str(num_corte) + ")+"
        query2 = "(SELECT COALESCE(" \
                 "(select sum(Factura.amount_residual) \"Total en Credito\" from account_move Factura " \
                 " where  Factura.type='out_invoice'" \
                 " and Factura.invoice_payment_state = 'not_paid'" \
                 " and Factura.invoice_payment_term_id>1" \
                 " and Factura.company_id= " + str(self.company_gt.id) + " and Factura.numero_corte = " + str(
            num_corte) + " ),0))"
        self.env.cr.execute(query1 + query2)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["?column?"])
        except:
            total = 0
        return total

    def saldos_pendientes_x_cobrar(self):
        query = "select sum(Factura.amount_residual) from account_move Factura where Factura.amount_residual>0" \
                " and Factura.invoice_payment_state = 'not_paid' and Factura.state='posted' and Factura.type='out_invoice' " \
                "and (Factura.invoice_payment_term_id is null or Factura.invoice_date = Factura.invoice_date_due) " \
                "and Factura.numero_corte is null  and Factura.company_id=" + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    def saldos_vencidos_de_facturas(self):
        query = "select sum(Factura.amount_residual) from account_move Factura " \
                "where Factura.amount_residual>0 and Factura.invoice_payment_state = 'not_paid' " \
                "and Factura.state='posted' and Factura.type='out_invoice' and Factura.invoice_payment_term_id>1" \
                "and Factura.invoice_date_due = (select current_date) and Factura.numero_corte =" \
                + str(self.num_corte) + " and Factura.company_id= " + str(self.company_gt.id)
        self.env.cr.execute(query)
        json_data = self.env.cr.dictfetchone()
        result = str(json_data).replace("'", "\"")
        try:
            valor = json.loads(result)
            total = float(valor["sum"])
        except:
            total = 0
        return total

    def action_transferencia_entre_diarios(self):
        if self.pd_efectivo:
            pago = self.env['account.move'].create({
                'ref': 'Movimiento Corte Z numero: ' + self.num_corte_mostrado,
                'date': self.get_creacion_corte(self.num_corte),
                'line_ids': [
                    (0, 0, {

                        'account_id': 35,
                        'name': 'No. Boleta: ' + self.no_boleta_ventas,
                        'debit': self.monto_quetzales,
                        'credit': 0.0,
                        #'currency_id': 170,
                        #'partner_id': self.partnet_id.name,
                        'amount_currency': 0.0,

                    }),
                    (0, 0, {
                        'date': '2022-02-01',
                        'account_id': 939,
                        'name': 'Corte Z ' + self.num_corte_mostrado,
                        'debit': 0.0,
                        'credit': self.monto_quetzales,
                        #'currency_id': 170,
                        #'partner_id': self.partnet_id.name,
                        'amount_currency': 0.0,

                        #'date': self.get_creacion_corte(self.num_corte),
                    }),
                ],

            })
            pago.post()
            self.validar_trasferecia_detalle_ventas()

    def validar_visa(self):
        self.val_visa = True
        self.usuario_conta = str(self.env.user.name)
        self.cs_diferencia = self.cs_liquido - self.cs_recibido
        if self.cant_total_visa:
            pago = self.env['account.move'].create({
                'ref': 'Movimiento Visanet Corte Z No.: ' + self.num_corte_mostrado,
                'date': self.get_creacion_corte(self.num_corte),
                'line_ids': [
                    (0, 0, {
                        'account_id': 286,
                        'name': 'Corte Z ' + self.num_corte_mostrado,
                        'debit': self.cant_total_visa,
                        'credit': 0.0,
                        #'currency_id': 147,
                        'company_id': 1,
                        'amount_currency': 0.0,
                    }),
                    (0, 0, {
                        'account_id': 950,
                        'name': 'Corte Z ' + self.num_corte_mostrado,
                        'debit': 0.0,
                        'credit': self.cant_total_visa,
                        #'currency_id': 147,
                        'company_id': 1,
                        'amount_currency': 0.0,
                    }),
                ],

            })
            pago.post()
        return "ok"

    def validar_credo(self):
        self.val_credo = True
        self.usuario_conta = str(self.env.user.name)
        self.cs_diferencia_c = self.cs_liquido_c - self.cs_recibido_c
        if self.cant_total_credo:
            pago = self.env['account.move'].create({
                'ref': 'Movimiento Credomatic Corte Z No.: ' + self.num_corte_mostrado,
                'date': self.get_creacion_corte(self.num_corte),
                'line_ids': [
                    (0, 0, {
                        'account_id': 286,
                        'name': 'Corte Z ' + self.num_corte_mostrado,
                        'debit': self.cant_total_credo,
                        'credit': 0.0,
                        #'currency_id': 147,
                        'company_id': 1,
                        'amount_currency': 0.0,
                    }),
                    (0, 0, {
                        'account_id': 950,
                        'name': 'Corte Z ' + self.num_corte_mostrado,
                        'debit': 0.0,
                        'credit': self.cant_total_credo,
                        #'currency_id': 147,
                        'company_id': 1,
                        'amount_currency': 0.0,
                    }),
                ],

            })
            pago.post()
        return "ok"

    def recalcular_visa(self):
        self.conciliacion_targetas()

    def recalcular_credo(self):
        self.conciliacion_targetas()


    def validar_trasferecia_detalle_ventas(self):
        self.vali_trans_venta = True
        self.usuario_conta = str(self.env.user.name)
        return "ok"

    def conciliacion_targetas(self):
        cmv = 0.026
        cmc = 0.030
        ret = 0.15
        it = 1.12
        pr = 124.08

        self.cs_consumo_v = self.cant_total_visa - self.cs_inguat  # self.cant_visa + self.anticipo_visa + self.cxc_visa
        self.cs_consumo_c = self.cant_total_credo - self.cs_inguat_c  # self.cant_visa + self.anticipo_visa + self.cxc_visa

        cs = self.cs_consumo_v
        csc = self.cant_credo

        self.cs_comision_v = self.cs_consumo_v * cmv
        self.cs_comision_sin_iva = (self.cs_comision_v / 1.12)
        self.cs_iva = self.cs_comision_sin_iva * 0.12
        self.cs_retencion = (self.cs_consumo_v / 1.12) * (0.12) * (0.15)
        self.cs_liquido = self.cant_total_visa - self.cs_comision_sin_iva - self.cs_iva - self.cs_retencion

        self.cs_comision_c = self.cs_consumo_c * cmc
        self.cs_comision_sin_iva_c = (self.cs_comision_c / 1.12)
        self.cs_iva_c = self.cs_comision_sin_iva_c * 0.12
        self.cs_retencion_c = (self.cs_consumo_c / 1.12) * (0.12) * (0.15)
        self.cs_liquido_c = self.cant_total_credo - self.cs_comision_sin_iva_c - self.cs_iva_c - self.cs_retencion_c

    @api.onchange('cs_recibido')
    def diferencia_consiliacion(self):
        diferencia = self.cs_liquido - self.cs_recibido
        self.cs_diferencia = diferencia
        return

    def procesar_cambio(self, monto, pago):
        pago = self.env['account.move'].create({
            'ref': 'Apunte Contable cambio en pago: '+str(pago)+' en Corte No. '+str(self.num_corte),
            'date': self.set_fecha_actual(),
            'line_ids': [
                (0, 0, {
                    'date': self.set_fecha_actual(),
                    'account_id': 939,
                    'debit': 0.0,
                    'credit': monto,
                    'amount_currency': 0.0,

                }),
                (0, 0, {
                    'date': self.set_fecha_actual(),
                    'account_id': 2,
                    'debit': monto,
                    'credit': 0.0,
                    'amount_currency': 0.0,

                    # 'date': self.get_creacion_corte(self.num_corte),
                }),
            ],

        })
        pago.post()
        return

    def envia_correo(self):
        # create message object instance
        msg = MIMEMultipart()
        message = "Thank you"
        # setup the parameters of the message
        password = "Programador2022"
        msg['From'] = "informatica2sarita@gmail.com"
        msg['To'] = "gonibal15@gmail.com"
        msg['Subject'] = "Subscription"
        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        # create server
        server = smtplib.SMTP('smtp.gmail.com: 587')
        server.starttls()
        # Login Credentials for sending the mail
        server.login(msg['From'], password)
        # send the message via the server.
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()
        print("successfully ")
        return


# ---------------REPORTERIA CORTE Z---------------------------------#
# Reporte Corte Z
class hotelReport(models.AbstractModel):
    _name = 'report.mark3.report_hotel_card'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('mark3.report_hotel_card')
        # DICCIONARIO
        # Pasamo los doc Ids
        return {
            'doc_ids': docids,
            'doc_model': self.env['corte_z.hotel'],
            'docs': self.env['corte_z.hotel'].browse(docids)
        }


# Reporte de Facturas Corte Z
class hotelReportFacturas(models.AbstractModel):
    _name = 'report.mark3.report_hotel_facturas_card'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('mark3.report_hotel_facturas_card')
        # DICCIONARIO
        # Pasamo los doc Ids
        return {
            'doc_ids': docids,
            'doc_model': self.env['corte_z.hotel'],
            'docs': self.env['corte_z.hotel'].browse(docids)
        }


# Reporte de Pagos Corte Z
class hotelReportPagos(models.AbstractModel):
    _name = 'report.mark3.report_hotel_pagos_card'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        # report = report_obj._get_report_from_name('mark3.report_hotel_pagos_card').with_context(landscape=True)
        # DICCIONARIO
        # Pasamo los doc Ids
        return {
            'doc_ids': docids,
            'doc_model': self.env['corte_z.hotel'],
            'docs': self.env['corte_z.hotel'].browse(docids)
        }

    # Reporte de Cuentas Por Cobrar Corte Z
    class hotelReportCXC(models.AbstractModel):
        _name = 'report.mark3.report_hotel_cxc_card'

        @api.model
        def _get_report_values(self, docids, data=None):
            return {
                'doc_ids': docids,
                'doc_model': self.env['corte_z.hotel'],
                'docs': self.env['corte_z.hotel'].browse(docids)
            }

    # Reporte de Anticipos Corte Z
    class hotelReportAnticipos(models.AbstractModel):
        _name = 'report.mark3.report_hotel_anticipos_card'

        @api.model
        def _get_report_values(self, docids, data=None):
            return {
                'doc_ids': docids,
                'doc_model': self.env['corte_z.hotel'],
                'docs': self.env['corte_z.hotel'].browse(docids)
            }

    # Reporte de facturas vencidas
    class hotelReportFacturasVencidas(models.AbstractModel):
        _name = 'report.mark3.report_hotel_fac_vencidas_card'

        @api.model
        def _get_report_values(self, docids, data=None):
            return {
                'doc_ids': docids,
                'doc_model': self.env['corte_z.hotel'],
                'docs': self.env['corte_z.hotel'].browse(docids)
            }


# Se añadio el campo numero_corte a la tabla account.move
class Corte(models.Model):
    _inherit = 'account.move'

    numero_corte = fields.Integer(string='No. corte')


# Se agregaron campos a la vista de Pago (account.payment)
class ImagenesPagos(models.Model):
    _inherit = 'account.payment'

    id_diario = fields.Integer(string='Id Diario')
    boleta_deposito = fields.Char(string='No. Boleta de Deposito')
    fecha_deposito = fields.Datetime(string='Fecha de Boleta')
    facturas_asociadas = fields.Many2many(readonly=True)
    info_pago = []
    info = ''

    num_tarjeta = fields.Char(string='No. de Tarjeta')
    num_autorizacion = fields.Char(string='No. de Autorización')

    num_transaccion = fields.Char(string='No. de Boleta/Transacción', )
    fecha_transaccion = fields.Datetime(string='Fecha de Boleta/Transacción')
    informacion = fields.Char(string='Información del Pago', readonly=True)

    def post(self):
        res = super(ImagenesPagos, self).post()
        self.info_pago.append('No. Deposito/Transferencia: ' + str(self.boleta_deposito))
        self.info_pago.append('Fecha: ' + str(self.fecha_deposito))
        self.info_pago.append('No. Targeta: ' + str(self.num_tarjeta))
        self.info_pago.append('Autorización: ' + str(self.num_autorizacion))
        self.info_pago.append('Fecha: ' + str(self.fecha_transaccion))

        if self.boleta_deposito is False:
            print('')
        else:
            self.info += str(self.info_pago[0]) + " "

        if self.fecha_deposito is False:
            print('')
        else:
            self.info += str(self.info_pago[1]) + " "

        if self.num_tarjeta is False:
            print('')
        else:
            self.info += str(self.info_pago[2]) + " "

        if self.num_autorizacion is False:
            print('')
        else:
            self.info += str(self.info_pago[3]) + " "

        if self.fecha_transaccion is False:
            print('')
        else:
            self.info += str(self.info_pago[4]) + " "

        self.informacion = str(self.info)
        self.info_pago.clear()
        return res

    @api.onchange('journal_id')
    def elegir_diario(self):
        ides = self.env['account.journal'].search([('id', '=', self.journal_id.id)])
        self.id_diario = int(ides.id)
        self.monto_pago = 0
        return
