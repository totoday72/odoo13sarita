# -*- coding: utf-8 -*-
{
    'name': "Factura Electrónica",

    'summary': """
        Certificación de documentos electrónicos por medio de Infile
        Módulo desarrollado por BesmarGT""",

    'description': """
        Debe registrar en la ficha de Compañía las credenciales para certificar
        Las facturas se certifican al momento de cambiar el estado de Borrador a Publicado
        La anulación se lleva a cabo cuando se una factura Publicada se cambia a borrador y es Cancelada  
    """,

    'author': "Besmart GT | Carlos García || Modificaciones a partir de Julio de 2020 Por Erick Hernandez",
    'website': "http://www.besmart.com.gt",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account','point_of_sale','info__fel','web_notify'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/cron.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/account_invoice_view.xml',
        'views/res_company_view.xml',
        'views/fel_report.xml',
        'views/view_tax_form.xml',
        'views/account_journal_form.xml',
        'views/account_payment_form.xml',
    ],
}
