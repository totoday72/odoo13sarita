# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import datetime
import base64
import json
import uuid
import qrcode
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from io import BytesIO
import requests


class FacturaElectronicaPayment(models.Model):
    _inherit = 'account.payment'

    generar_fel_en_este_pago = fields.Boolean('Fel Generado en este Pago: ', default='False')

    def post(self):
        res = super(FacturaElectronicaPayment, self).post()
        for invoice in self.invoice_ids:
            # # print("Nombre: ",invoice.name)
            if invoice.type == 'in_invoice':
                # Para Compras continúa el proceso
                return True
            elif invoice.type in ['out_invoice', 'out_refund']:
                # Aplica para ventas y notas de crédito Erick->Siempre y cuando el cheque de procesar fel este activado.
                if invoice.inf_numero_autorizacion:
                    return res # Retorna el objeto.
                else:
                    if invoice.journal_id.procesar_fel:
                        if 0 == invoice.amount_residual: # Erick > Verifica si el residuo  es 0, si es 0 es porque la deuda esta saldada y se generara fel.
                            invoice.procedureCertificaFEL() # genera el fel para la factura seleccionada.
                            return res # Retorna el objeto.
                        else:
                            return res # Retorna el objeto.
                    else:
                        return res # Retorna el objeto.
            else:
                return True