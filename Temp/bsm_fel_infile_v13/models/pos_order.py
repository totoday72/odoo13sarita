# -*- coding: utf-8 -*-

from odoo import models


class facturaElectronicaPOS(models.Model):
    _inherit = 'pos.order'

    def action_pos_order_invoice(self):
        res = super(facturaElectronicaPOS, self).action_pos_order_invoice()
        self.account_move.procedureCertificaFEL()
        return res