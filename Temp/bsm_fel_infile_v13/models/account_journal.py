# -*- coding: utf-8 -*- account.journal

from odoo import api, fields, models, _

class establecimientoDiario(models.Model):
    _inherit = 'account.journal'

    fel_inf_codigo_establecimiento = fields.Char(string="Código establecimiento")
    procesar_fel = fields.Boolean(string="Procesar Fel en este diario:", store=True)
