# -*- coding: utf-8 -*-
# from numpy.core import size
from odoo import http
from odoo.http import request

import dateutil.utils
import pytz

from odoo import api, fields, models, _
import datetime
import base64
import json
import uuid
import qrcode
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from io import BytesIO
import requests


# import numpy
XML_ID = "muk_web_theme._assets_primary_variables"
SCSS_URL = "/muk_web_theme/static/src/scss/colors.scss"

class FacturaElectronica(models.Model):
    _inherit = ['account.move']
    # _inherit = ['mail.thread']

    # inf_fecha_certifica = fields.Char(string='Fecha certifica')  # Devuelta por SAT
    # inf_serie = fields.Char(sgring='Serie')
    # inf_numero_autorizacion = fields.Char(string='Número autorización')
    # inf_numero_factura = fields.Char(string='Número FEL')
    # inf_ruta_doc = fields.Char(string="Link factura")
    #
    # inf_anula_numero = fields.Char(string='Número Anula')
    # inf_anula_autoriza = fields.Char(string='Serie Anula')
    # inf_anula_serie = fields.Char(string='Serie Anula')
    # inf_anula_fecha = fields.Char(string="Fecha Anula")
    # inf_anula_codigoUUID = fields.Char(string="Codigo unico UUID: ")
    # inf_certifica_codigoUUID = fields.Char(string="Codigo unico UUID: ")
    # inf_certificador_nombre = fields.Char(string="Nombre certificador")
    # inf_certificador_nit = fields.Char(string="NIT certificador")
    inf_image_qr = fields.Binary("QR FEL", compute="_computeGenerarQR")

    # inf_fecha_certificaorg = fields.Char(string='Fecha Larga certifica')  # Devuelta por SAT
    # generar_fel_factura = fields.Boolean(string="Generar Fel al Publicar Factura", default='True')
    # numero_corte = fields.Integer(string='No. corte')
    # Eliminar estos campos
    # inf_campo_test = fields.Char(string='Campito de prueba')
    # inf_campo_ejemplo = fields.Char(string='Ejemplo de campo', default='Hola mundo!')

    # def action_reverse(self):
    #     action = self.env.ref('account.action_view_account_move_reversal').read()[0]
    #
    #     if self.is_invoice():
    #         action['name'] = _('Credit Note')
    #   return action
    # Erick

    @api.model
    def fac_vencidas_pendientes_cobro(self, condicion, message_head, message_empty, type, channel_text, alias_name):
        self.inbox_message(False, self.tabla_facturas_pagos_html(condicion, message_head, message_empty, type), channel_text, alias_name)


    @api.model
    def fac_credito_borrador_envio(self, condicion, message_head, message_empty, type, channel_text, alias_name):
        self.inbox_message(False, self.tabla_facturas_pagos_html(condicion, message_head, message_empty, type), channel_text, alias_name)

    @api.model
    def recibos_pago_anticipo_deposito_transferencia(self, condicion, message_head, message_empty, type, channel_text, alias_name):
        self.inbox_message(False, self.tabla_facturas_pagos_html(condicion, message_head, message_empty, type), channel_text, alias_name)

    @api.model
    def recibos_pago_credito_deposito_transferencia(self, condicion, message_head, message_empty, type, channel_text, alias_name):
        self.inbox_message(False, self.tabla_facturas_pagos_html(condicion, message_head, message_empty, type), channel_text, alias_name)

    def tabla_facturas_pagos_html(self, condicion, message_head, message_empty, type): # Erick condicion: el dominio, message_success, el mensaje exitoso a usar en tabla, message_empty mensaje si no encuentra ndada, type: si es pago o factura
        mensaje_text = "<b>Mensaje enviado a: </b>" + str(
                    datetime.datetime.now(pytz.timezone("America/Guatemala"))) + \
                               "<br/>" \
                               "<strong>"+message_head+"</strong>"
        if type: # si la opcion son facturas
            facturas = self.env['account.move'].search(condicion)
            if len(facturas) == 0:
                mensaje_text = "<strong>"+message_empty+"</strong>"
                return mensaje_text
            mensaje_text = mensaje_text + '<table>' \
                           '<table class="table table-striped" style="font-size: 11px;">' \
                           '<thead>' \
                           '<tr>' \
                           '<td style="text-align:center;">' \
                           '<b>Fecha de creación</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Fecha de vencimiento</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Cliente</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Factura</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Estado</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Monto Pendiente</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Plazo de Pago</b>' \
                           '</td>' \
                           '</tr>' \
                           '</thead>'

            mensaje_text = mensaje_text + '<tbody>'
            for fac in facturas:
                mensaje_text = mensaje_text + '<tr>' \
                                              '<td style="text-align:center;">' \
                               + str(fac.invoice_date) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(fac.invoice_date_due) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(fac.partner_id.name) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               '<b><a target="_blank" href="/web#cids=1&id=' + str(
                    fac.id) + '&type=form&model=account.move"> Factura </a></b>' \
                              '</td>' \
                              '<td style="text-align:center;">' \
                               + self.posted_or_draft(str(fac.state), lambda x: x == 'posted') + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(fac.amount_residual) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(fac.invoice_payment_term_id.name) + \
                               '</td>' \
                               '</tr>'
            mensaje_text = mensaje_text + '</tbody>' \
                                          '<tfoot>'
            mensaje_text = mensaje_text + '<tr><td colspan="7">********** Ultima Linea **********</td></tr>'
            mensaje_text = mensaje_text + '</tfoot>' + '</table>'
            return mensaje_text
        else:   # si la opcion son pagos
            pagos = self.env['account.payment'].search(condicion)
            if len(pagos) == 0:
                mensaje_text = "<b>Mensaje enviado a: </b>" + str(
                    datetime.datetime.now(pytz.timezone("America/Guatemala"))) + \
                               "<br/>" \
                               "<strong>"+message_empty+"</strong>"
                return mensaje_text
            mensaje_text = mensaje_text + '<table>' \
                           '<table class="table table-striped" style="font-size: 11px;">' \
                           '<thead>' \
                           '<tr>' \
                           '<td style="text-align:center;">' \
                           '<b>Fecha de pago</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Cliente</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Forma de Pago</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Importe</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Informacion de Pago</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Estado</b>' \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b>Creado Por</b>' \
                           '</td>' \
                           '</tr>' \
                           '</thead>'
            mensaje_text = mensaje_text + '<tbody>'
            for pag in pagos:
                mensaje_text = mensaje_text + '<tr>' \
                                              '<td style="text-align:center;">' \
                               + str(pag.payment_date) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(pag.partner_id.name) + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(pag.journal_id.name) + \
                               '</td>' \
                               '<td style="text-align:right;">' \
                               '<b><a target="_blank" href="/web#cids=1&id=' + str(
                    pag.id) + '&type=form&model=account.payment">'+ str(pag.amount) +'</a></b>' \
                                '<td style="text-align:center;">'
                if pag.num_transaccion:
                    mensaje_text = mensaje_text + str(pag.num_transaccion)
                else:
                    mensaje_text = mensaje_text + str(pag.informacion)
                mensaje_text = mensaje_text + '</td>' \
                               '</td>' \
                              '<td style="text-align:center;">' \
                               + self.posted_or_draft(str(pag.state), lambda x: x == 'posted') + \
                               '</td>' \
                               '<td style="text-align:center;">' \
                               + str(pag.create_uid.name) + \
                               '</td>' \
                               '</tr>'
            mensaje_text = mensaje_text + '</tbody>' \
                                          '<tfoot>'
            mensaje_text = mensaje_text + '<tr><td colspan="7"style="text-align:center; >********** Ultima Linea **********</td></tr>'
            mensaje_text = mensaje_text + '</tfoot>' + '</table>'
            return mensaje_text


    @api.onchange("publicar_1ero_mes")
    def verify_property_payment_term_id(self): # Erick
        if self.partner_id:
            # # # print(self.partner_id.property_payment_term_id.name, " ", self.partner_id.property_payment_term_id.id)
            if self.partner_id.property_payment_term_id.id <= 1:
                raise ValidationError(
                    f'El cliente: ' + self.partner_id.name + ' no tiene plazo de pago, por tanto esta opción no se puede habilitar.')

    @api.model
    def verify_1st_invoices(self, channel_text, alias_name, facturacion):  # Erick
        mensaje_text = "<b>Mensaje enviado a: </b>" + str(datetime.datetime.now(pytz.timezone("America/Guatemala"))) + \
                       "<br/>"
        if not facturacion:
            mensaje_text = mensaje_text + "<strong>Las siguientes Facutras estan pendientes de publicar</strong>"
        else:
            mensaje_text = mensaje_text + "<strong>Las siguientes facturas se han publicado automaticamente.</strong>" \
                                          "<p>pero las facturas aun estan pendientes de cobro</p>"
        mensaje_text = mensaje_text + '<table>' \
                       '<table class="table table-striped" style="font-size: 11px;">' \
                       '<thead>' \
                       '<tr>' \
                       '<td style="text-align:center;">' \
                       '<b>Fecha de creación</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Fecha de vencimiento</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Cliente</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Factura</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Estado</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Monto Pendiente</b>' \
                       '</td>' \
                       '<td style="text-align:center;">' \
                       '<b>Plazo de Pago</b>' \
                       '</td>' \
                       '</tr>' \
                       '</thead>'
        # facturas = self.env['account.move'].search([ '|','&', ('publicar_1ero_mes', '=', True),'&', ('type','=','out_invoice'), ('state', '=', 'draft'),'&',('invoice_payment_state', '!=', 'paid'), '&' ,('state', '=', 'posted'),'&', ('type','=','out_invoice'), ('invoice_date','<=',datetime.datetime.now())])
        facturas = self.env['account.move'].search(
            ['|', '&', ('publicar_1ero_mes', '=', True), '&', ('type', '=', 'out_invoice'), ('state', '=', 'draft'),
             '&', ('invoice_payment_state', '!=', 'paid'), '&', ('state', '=', 'posted'), '&',
             ('type', '=', 'out_invoice'), '&', ('publicar_1ero_mes', '=', True),
             ('invoice_date_due', '<=', datetime.datetime.now())])
        if len(facturas) == 0:
            mensaje_text = "<b>Mensaje enviado a: </b>" + str(
                datetime.datetime.now(pytz.timezone("America/Guatemala"))) + \
                           "<br/>" \
                           "<strong>No hay facturas pendientes de cobro.</strong>"
            self.inbox_message(False, mensaje_text, channel_text, alias_name)
            return
        mensaje_text = mensaje_text + '<tbody>'
        for fac in facturas:
            mensaje_text = mensaje_text + '<tr>' \
                                          '<td style="text-align:center;">' \
                           + str(fac.invoice_date) + \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           + str(fac.invoice_date_due) + \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           + str(fac.partner_id.name) + \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           '<b><a target="_blank" href="/web#cids=1&id=' + str(
                fac.id) + '&type=form&model=account.move"> Factura </a></b>' \
                          '</td>' \
                          '<td style="text-align:center;">' \
                           + self.posted_or_draft(str(fac.state), lambda x: x=='posted') + \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           + str(fac.amount_residual) + \
                           '</td>' \
                           '<td style="text-align:center;">' \
                           + str(fac.invoice_payment_term_id.name) + \
                           '</td>' \
                           '</tr>'
        mensaje_text = mensaje_text + '</tbody>' \
                                      '<tfoot>'
        if facturacion:
            if self.post_invoices(facturas):
                mensaje_text = mensaje_text + '<tr><td colspan="7">**Algunas facturas tuvieron error al publicar por favor intente publicarlas manualmente.</td></tr>'
            else:
                mensaje_text = mensaje_text + '<tr><td colspan="7">**Recuerde que las facturas que estan en estado borrador el dia 5 del presente mes se facturarán automáticamente.</td></tr>'
        else:
            mensaje_text = mensaje_text + '<tr><td colspan="7">**Recuerde que las facturas que estan en estado borrador el dia 5 del presente mes se facturarán automáticamente.</td></tr>'
        mensaje_text = mensaje_text + '</tfoot>' + '</table>'
        self.inbox_message(False, mensaje_text, channel_text, alias_name)

        # users = self.env['res.users'].search([])
        # for user in users:
        #     self.inbox_message(False, mensaje_text, 'Picking Validated')
        #     user.notify_success(message="Existen facturas pendientes de Facturar", title="Hay facturas de clientes no facturadas", sticky=False)
        # return self.env.user.notify_success(message='My success message')

    def post_invoices(self, invoices): # Erick
        error_fel = False
        for invoice in invoices:
            if invoice.intentos_publicar < 3:
                invoice.intentos_publicar = invoice.intentos_publicar + 1
                if not error_fel:
                    try:
                        error_fel = invoice.action_post()
                    except:
                        return True
        return error_fel


    def posted_or_draft(self,var,fn): # Erick
        if fn(var):
            return "Publicado"
        else:
            return "Borrador"

    def inbox_message(self, user, message_text=f'<strong>Title Groups</strong> <p>This Groups!</p>',
                      channel_txt='Groups', alias_name='group'): # Erick
        """
        Send user chat notification on picking validation.
        """
        # odoo runbot
        odoobot_id = self.env['ir.model.data'].sudo().xmlid_to_res_id("base.partner_root")

        # find if a channel was opened for this user before
        channel = None
        if user:
            channel = self.env['mail.channel'].sudo().search([
                ('name', '=', channel_txt),
                ('channel_partner_ids', 'in', [user.partner_id.id])
            ],
                limit=1,
            )
            channel.sudo().message_post(
                body=message_text,
                author_id=odoobot_id,
                message_type="comment",
                subtype="mail.mt_comment",
            )
            # users = channel.channel_last_seen_partner_ids
            # for user in users:
                # user_id = self.env['res.users'].sudo().search([('partner_id','=',user.partner_id.id)])
                # user_id.notify_success(message="Existen facturas pendientes de Facturar",
                #                     title="Hay facturas de clientes no facturadas", sticky=False)
        else:
            channel = self.env['mail.channel'].sudo().search([
                ('name', '=', channel_txt), ('alias_name', "=", alias_name)
            ],
                limit=1,
            )
            channel.sudo().message_post(
                body=message_text,
                author_id=odoobot_id,
                message_type="comment",
                subtype="mail.mt_comment",
            )
            # users = channel.channel_last_seen_partner_ids
            # for user in users:
                # user_id = self.env['res.users'].sudo().search([('partner_id', '=', user.partner_id.id)])
                # user_id.notify_success(message="Existen facturas pendientes de Facturar",
                #                                   title="Hay facturas de clientes no facturadas", sticky=False)
        # if not channel:
        #     # print("intentando crear canal")
        #     # create a new channel
        #     channel = self.env['mail.channel'].with_context(mail_create_nosubscribe=True).sudo().create({
        #         'channel_partner_ids': [(4, self.env.user.partner_id.id), (4, odoobot_id)],
        #         'public': 'private',
        #         'channel_type': 'chat',
        #         'email_send': False,
        #         'name': f'Picking Validated',
        #         'display_name': f'Picking Validated',
        #     })

        # send a message to the related user
        # channel.sudo().message_post(
        #     body=message_text,
        #     author_id=odoobot_id,
        #     message_type="comment",
        #     subtype="mail.mt_comment",
        # )

    def action_send_notification(self):
        self.env['mail.message'].create({
            'email_from': self.env.user.partner_id.email,  # add the sender email
            'author_id': self.env.user.partner_id.id,  # add the creator id
            'model': 'mail.channel',  # model should be mail.channel
            'type': 'comment',
            'subtype_id': self.env.ref('mail.mt_comment').id,  # Leave this as it is
            'body': "Body of the message",  # here add the message body
            'channel_ids': [(4, self.env.ref('module_name.channel_accountant_group').id)],
            # This is the channel where you want to send the message and all the users of this channel will receive message
            'res_id': self.env.ref('modulename.channel_accountant_group').id,  # here add the channel you created.
        })

    @api.onchange('partner_id')  # editado
    def colocar_fecha_auto(self):
        if self.type == 'out_invoice':
            self.invoice_date = datetime.datetime.now(pytz.timezone("America/Guatemala"))  # Erick se coloco para GT datetime.datetime.utcnow()
        return

    def btnActionPruebita(self): # Erick
        # # print('Pruebas desde factura')
        str_archivo_xml = self.functionCadenaXMLInfile()
        # self.inf_image_qr = self.generate_qr_code(value_qr)
        raise ValidationError(str_archivo_xml)

    def set_fel_Pruebas(self): # Erick
        if self.env.cr.dbname == "PRODUCCION":
            raise ValidationError(
                "NO se puede aplicar esta configuracion, la base de datos es de: " + self.env.cr.dbname)
        else:
            self.ref = 'Se coloco FEL en estado Prueba, ahora saldran las facturas con serie \"Pruebas\"'
            company_id = self.env.company
            company_id.partner_id.vat = '30449901'
            company_id.fel_usuario_infile = '30449901'
            company_id.fel_llave = '44675ADB3BE212FAC24FD24B6E1CBAD7'
            company_id.fel_identificador = 'e22c484cc8ba1949b431422f6aae7f6a'
            variables = [
                {'name': 'o-brand-odoo', 'value': '#06a749'},
                {'name': 'o-brand-primary', 'value': "#5D8DA8"},
                {'name': 'mk-required-color', 'value': "#d1dfe6"},
                {'name': 'mk-apps-color', 'value': "#f8f9fa"},
                {'name': 'mk-appbar-color', 'value': "#dee2e6"},
                {'name': 'mk-appbar-background', 'value': '#06a749'},
            ]
            self.env['muk_utils.scss_editor'].replace_values(SCSS_URL, XML_ID, variables)
            return
            {
                'type': 'ir.actions.client',
                'tag': 'reload',
            }

    def _computeGenerarQR(self):
        qr_url_sat = "https://felpub.c.sat.gob.gt/verificador-web/publico/vistas/verificacionDte.jsf"
        qr_tipo_consulta = "?tipo=autorizacion&"
        qr_numero = f"numero={self.inf_numero_autorizacion}&"
        qr_emisor = f"emisor={self.company_id.vat.replace('-', '')}&"
        qr_receptor = f"receptor={self.partner_id.vat.replace('-', '')}&"
        qr_monto = f"monto={'{:.6f}'.format(self.amount_total)}"

        value_qr = qr_url_sat + qr_tipo_consulta + qr_numero + qr_emisor + qr_receptor + qr_monto

        # # print("URL para consulta: ",value_qr)
        self.inf_image_qr = self.generate_qr_code(value_qr)

    def generate_qr_code(self, value):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=30,
            border=4,
        )
        qr.add_data(value)
        qr.make(fit=True)
        img = qr.make_image()
        temp = BytesIO()
        img.save(temp, format="PNG")
        qr_img = base64.b64encode(temp.getvalue())
        return qr_img

    def notificacion_contingencia(self):
        # print('Entramos a la función de message...')
        notification = {
            'type': 'ir.actions.client',
            'tag': 'display_notification',
            'params': {
                'title': ('Your Custom Title'),
                'message': 'Your Custom Message',
                'type': 'success',  # types: success,warning,danger,info
                'sticky': True,  # True/False will display for few seconds if false
            },
        }
        return notification

    def action_post(self):
        # return self.write({'fac_campo_test': 'Esto es una prueba'})
        if self.partner_id:
            if not self.partner_id.vat:
                texto = f'El cliente '+ self.partner_id.name +' no tiene NIT, agregue uno antes de continuar o coloque \"CF\".'
                raise ValidationError(texto)
        res = super(FacturaElectronica, self).action_post()
        if self.generar_fel_factura:
            if self.type == 'in_invoice':
                # Para Compras continúa el proceso
                return True
            elif self.type in ['out_invoice', 'out_refund']:
                # Aplica para ventas y notas de crédito Erick->Siempre y cuando el cheque de procesar fel este activado.
                if not self.inf_numero_autorizacion and self.journal_id.procesar_fel:  # Erick
                    self.procedureCertificaFEL()
                return res
            else:
                return True
        else:
            return res

    def funcDatosNota(self, pIDReversa):
        # self.reversed_entry_id.id
        account_move = self.env['account.move'].search([('id', '=', pIDReversa)])
        FechaEmisionDocumentoOrigen = account_move.invoice_date
        AutrizacionOrigen = account_move.inf_numero_autorizacion
        MotivoAjuste = self.ref

        datosNCRE = {
            'FechaEmisionDocumentoOrigen': FechaEmisionDocumentoOrigen,
            'AutrizacionOrigen': AutrizacionOrigen,
            'MotivoAjuste': MotivoAjuste
        }

        # print('Datos NCRE: ', datosNCRE)

        return datosNCRE

    def create(self, vals):
        new_id = super(FacturaElectronica, self).create(vals)
        new_id.generarUUID()
        if new_id.type == 'out_invoice':
            new_id.invoice_date = datetime.datetime.now(pytz.timezone("America/Guatemala"))
        return new_id

    @api.onchange('name')
    def generarUUID(self):
        if not self.inf_certifica_codigoUUID:  # Erick-> aca se genera el codigo uuid para poder tener una factura unica asociada a un uuid
            codigoUUID = uuid.uuid1()  # "A45384A4-52B5-11EC-8FD6-AC728926D4CC"  # uuid.uuid1()
            codigoUUID = str(codigoUUID).upper()
            self.inf_certifica_codigoUUID = codigoUUID
        # print(self.inf_certifica_codigoUUID)

    def procedureCertificaFEL(self):
        if not self.partner_id.vat:
            texto = f'El cliente '+ self.partner_id.name +' no tiene NIT, agregue uno antes de continuar o coloque \"CF\".'
            raise ValidationError(texto)
        invoice_date = datetime.datetime.now(pytz.timezone("America/Guatemala"))
        # # print(invoice_date.strftime("%Y-%m-%d"))
        # # print(invoice_date)
        # # print(datetime.datetime.utcnow())
        facturas = self.env['account.move'].search([
            '&',
            ('invoice_date', '<', invoice_date.strftime("%Y-%m-%d")),
            '&',
            '|',
            '&',
            ('numero_corte', '=', False),
            ('state', '=', 'posted'),
            '&',
            ('numero_corte', '=', 0),
            ('state', '=', 'posted'),
            '|',
            ('type', '=', 'out_invoice'),
            ('type', '=', 'out_refund'),
        ], order='name asc')

        # '&',
        # ('numero_corte', '=', 0),
        # ('state', '=', 'posted'),

        # for x in facturas:
        #     # print(str(len(facturas)))
        #     # print(x.name)
        #     # print(x.invoice_date)
        if len(facturas) > 0:
            raise ValidationError(
                f'Existen facturas que aun no tienen corte, realice antes un corte para poder facturar.')
        # if not self.inf_certifica_codigoUUID:  # Erick-> aca se genera el codigo uuid para poder tener una factura unica asociada a un uuid
        #     codigoUUID = uuid.uuid1() #"A45384A4-52B5-11EC-8FD6-AC728926D4CC"  # uuid.uuid1()
        #     codigoUUID = str(codigoUUID).upper()
        #     self.inf_certifica_codigoUUID = codigoUUID
        # # print(self.inf_certifica_codigoUUID)
        encabezado = {
            'usuario': self.company_id.fel_usuario_infile,
            'llave': self.company_id.fel_llave,
            'Content-Type': 'application/json',
            'identificador': self.inf_certifica_codigoUUID,  # self.company_id.fel_identificador,
        }

        cadena_xml = self.functionSolicitaFirma('N')

        contenido_json = {
            'nit_emisor': self.company_id.vat.replace("-", ""),
            'correo_copia': 'ofisistemas.gt@gmail.com',
            'xml_dte': cadena_xml
        }
        # # print('--- CadenaXML --- ',cadena_xml)

        try:
            response = requests.post(self.company_id.fel_url_certifica, headers=encabezado, json=contenido_json,
                                     verify=False)
        except Exception as err:
            raise ValidationError(f'Other error occurred: {err}')
        else:
            # # print('Estado: ',response.status_code)
            # print("Response certificado...")
            # print(response.text)
            resultadoJson = json.loads(response.text)
            cantidadErrores = resultadoJson['cantidad_errores']
            if cantidadErrores > 0:
                descErroes = resultadoJson['descripcion_errores']
                raise ValidationError(f'Se encontraron: {cantidadErrores} errores. {descErroes}')
            else:
                self.inf_serie = resultadoJson['serie']
                self.inf_numero_factura = resultadoJson['numero']
                self.inf_numero_autorizacion = resultadoJson['uuid']
                self.inf_ruta_doc = "https://report.feel.com.gt/ingfacereport/ingfacereport_documento?uuid=" + \
                                    resultadoJson['uuid']
                self.inf_certificador_nombre = "INFILE, S.A."
                self.inf_certificador_nit = "1252133-7"

                qr_url_sat = "https://felpub.c.sat.gob.gt/verificador-web/publico/vistas/verificacionDte.jsf"
                qr_tipo_consulta = "?tipo=autorizacion&"
                qr_numero = f"numero={resultadoJson['uuid']}&"  # self.inf_numero_autorizacion
                qr_emisor = f"emisor={self.company_id.vat.replace('-', '')}&"
                qr_receptor = f"receptor={self.partner_id.vat.replace('-', '')}&"
                qr_monto = f"monto={'{:.6f}'.format(self.amount_total)}"

                value_qr = qr_url_sat + qr_tipo_consulta + qr_numero + qr_emisor + qr_receptor + qr_monto

                # # print("URL para consulta: ",value_qr)
                # self.inf_image_qr = self.generate_qr_code(value_qr)
                # # print('resultadoJson: ',resultadoJson)
                fechas = resultadoJson["fecha"]
                self.inf_fecha_certificaorg = fechas.replace('#', " ")
                self.inf_fecha_certifica = fechas.replace('T', " ").replace('-06:00',
                                                                            '')  # + datetime.datetime.utcnow().strftime("%H:%M:%S") # editado

    def functionSolicitaFirma(self, psEsAnulacion):
        vsUrlFirma = self.company_id.fel_url_solicita_firma

        if psEsAnulacion == 'N':
            cadena_xml = self.functionCadenaXMLInfile()
        else:
            cadena_xml = self.functionCadenaAnulaInfile()

        # print(cadena_xml)
        cadenaBytesCodificada = base64.b64encode(cadena_xml.encode('utf-8'))
        stringCodificado = str(cadenaBytesCodificada, 'utf-8')

        encabezado = {
            'Content-Type': 'application/json',
        }

        json_solicitud = {
            "llave": self.company_id.fel_identificador,
            "archivo": stringCodificado,
            "codigo": self.name,
            "alias": self.company_id.fel_usuario_infile,
            "es_anulacion": psEsAnulacion
        }

        try:
            response = requests.post(vsUrlFirma, headers=encabezado, json=json_solicitud, verify=False)
        except Exception as err:
            raise ValidationError(f'Other error occurred: {err}')
        else:
            # # print('Estado: ', response.status_code)
            # print('Solicita firma..... ', response.text)
            resultadoJson = json.loads(response.text)

            # # print(resultadoJson["resultado"])
            if resultadoJson["resultado"] != True:
                raise ValidationError(f'Error en solicitud de firma: {resultadoJson["descripcion"]}')

        return resultadoJson["archivo"]

    def button_draft(self):  # erick
        if (self.numero_corte is not 0) and self.type in ('out_invoice', 'out_refund'):
            raise ValidationError(f'Error en anulacion: No se puede anular una factura que ya esta en un corte!')
        if not self.inf_fecha_certificaorg:  # Erick-> si el campo fecha certificaorg esta vacio que proceda a cancelar solo la factura sin hacer cancelacion fel
            res = super(FacturaElectronica, self).button_draft()
            return res
        if not self.inf_anula_serie:
            if self.company_id.fel_anula_con_infile and self.type in ('out_invoice', 'out_refund'):
                # # print("action anula")
                self.procedureAnulaFEL()
                res = super(FacturaElectronica, self).button_cancel()
                # hacer un backup antes de eliminar la informacion de fel
                var1 = self.inf_anula_serie
                var2 = self.inf_anula_numero
                var3 = self.inf_anula_autoriza
                var4 = self.inf_serie
                var5 = self.inf_numero_factura
                var6 = self.inf_numero_autorizacion
                var7 = self.inf_fecha_certifica
                var8 = self.inf_fecha_certificaorg
                var9 = self.inf_certifica_codigoUUID
                var10 = self.numero_corte
                var11 = self.inf_anula_codigoUUID
                # eliminar la informacion Fel y duplicar
                self.inf_anula_serie = False
                self.inf_anula_numero = False
                self.inf_anula_autoriza = False
                self.inf_serie = False
                self.inf_numero_factura = False
                self.inf_numero_autorizacion = False
                self.inf_fecha_certifica = False
                self.inf_fecha_certificaorg = False
                self.inf_certifica_codigoUUID = False
                self.numero_corte = False
                self.inf_anula_codigoUUID = False
                duplicate = super(FacturaElectronica,
                                  self).action_duplicate()  # duplica la factura y la guarda en duplicate
                # restaurar la informacion fel a la factura anterior (a la cancelada)
                self.inf_anula_serie = var1
                self.inf_anula_numero = var2
                self.inf_anula_autoriza = var3
                self.inf_serie = var4
                self.inf_numero_factura = var5
                self.inf_numero_autorizacion = var6
                self.inf_fecha_certifica = var7
                self.inf_fecha_certificaorg = var8
                self.inf_certifica_codigoUUID = var9
                self.numero_corte = var10
                self.inf_anula_codigoUUID = var11
            return duplicate  # retorno la factura duplicada para que se vea
        else:
            res = super(FacturaElectronica, self).button_draft()
            return res

    def procedureAnulaFEL(self):
        # print("Procedure anula factura")
        if not self.inf_anula_codigoUUID:
            codigoUUID = uuid.uuid1()  # "A45384A4-52B5-11EC-8FD6-AC728926D4CC"  # uuid.uuid1()
            codigoUUID = str(codigoUUID).upper()
            self.inf_anula_codigoUUID = codigoUUID
        # print(self.inf_anula_codigoUUID)

        encabezado = {
            'usuario': self.company_id.fel_usuario_infile,
            'llave': self.company_id.fel_llave,
            'Content-Type': 'application/json',
            'identificador': self.inf_anula_codigoUUID,  # self.company_id.fel_identificador,
        }

        stringCodificado = self.functionSolicitaFirma('S')

        contenido_json = {
            'nit_emisor': self.company_id.vat.replace("-", ""),
            'correo_copia': 'erickhernandez@saritarestaurante.com',
            'xml_dte': stringCodificado
        }

        try:
            response = requests.post(self.company_id.fel_url_anula, headers=encabezado, json=contenido_json,
                                     verify=False)
        except Exception as err:
            raise ValidationError(f'Other error occurred: {err}')
        else:
            # print('Estatus response: ', response.status_code)
            obj_json = json.loads(response.text)
            # print('obj_json: ', obj_json)

            cantidadErrores = obj_json['cantidad_errores']
            if cantidadErrores > 0:
                descErroes = obj_json['descripcion_errores']
                raise ValidationError(f'Se encontraron: {cantidadErrores} errores. {descErroes}')
            else:
                self.inf_anula_serie = obj_json['serie']
                self.inf_anula_numero = obj_json['numero']
                self.inf_anula_autoriza = obj_json['uuid']
                self.inf_anula_fecha = obj_json["fecha"]

    def functionCadenaXMLInfile(self):
        # # print('Fecha factura',self.invoice_date)
        if (self.partner_id.vat) == False:
            receptorNIT = "CF"
        else:
            receptorNIT = self.partner_id.vat.replace("-", "")

        if (self.partner_id.email) == False:
            correoReceptor = ''
        else:
            correoReceptor = self.partner_id.email

        horaGuatemala = self.invoice_date - datetime.timedelta(hours=6)  # editado
        fechaEmision = horaGuatemala.strftime("%Y-%m-%dT%H:%M:%S") + '.000-06:00'
        nitEmisor = self.company_id.vat.replace("-", "")
        direccionEmisor = self.company_id.street
        receptorNombre = self.partner_id.name
        nombreEmisor = self.company_id.fel_razon_social
        nombreComercial = self.company_id.fel_nombre_comercial
        TotalesBruto = self.amount_total
        TotalesIva = 0
        TotalesOtros = 0

        # resultado = valor_si if condicion else valor_no
        # Si el tipo de doc es NABN, se valida más adelante, cuando se agregan las frases e impuestos
        tipoDocumento = 'FACT' if self.company_id.fel_afiliacion_iva == 'GEN' else 'FPEQ'

        AfiliacionIVA = self.company_id.fel_afiliacion_iva
        codigoMoneda = self.currency_id.name  # 'GTQ'
        codigoUnidadGravable = 1

        codigoEstablecimiento = self.journal_id.fel_inf_codigo_establecimiento
        if not codigoEstablecimiento:
            raise ValidationError("Error: Debe registrar un establecimiento en el diario de facturas correspondiente")

        # ------------   Definición de detalle  ----------------------
        # Pequeño contribuyente: f"<dte:Frase CodigoEscenario='1' TipoFrase='3' />"
        # Contribuyente normal: f"<dte:Frase CodigoEscenario='1' TipoFrase='1' />"
        if AfiliacionIVA == 'GEN':  # Erick
            if self.company_id.vat == '30449901':  # Verificar si el nit de emisor es sarita y agregarle la frase tipo 2
                fraseAfiliacion = ("<dte:Frase CodigoEscenario='1' TipoFrase='1' />"
                                   f"<dte:Frase CodigoEscenario='1' TipoFrase='2' />")
            else:  # erick
                fraseAfiliacion = "<dte:Frase CodigoEscenario='1' TipoFrase='1' />"
        elif AfiliacionIVA == 'PEQ':
            fraseAfiliacion = "<dte:Frase CodigoEscenario='1' TipoFrase='3' />"
        else:
            # En TipoFrase se coloca 0 pero hay que colocar la correcta
            fraseAfiliacion = "<dte:Frase CodigoEscenario='1' TipoFrase='0' />"

        tagFrases = ("<dte:Frases>"
                     f"{fraseAfiliacion}"
                     f"</dte:Frases>")

        if self.type == 'out_refund':
            tipoDocumento = 'NCRE'  # Nota de crédito

        if codigoMoneda == 'USD':
            codigoUnidadGravable = 2
        elif codigoMoneda == 'GTQ':
            codigoUnidadGravable = 1

        numeroLinea = 0
        detalleXML = ''
        numeroLinea += 1
        total_iva = 0
        total_inguat = 0
        existeImpuestoInguat = False

        if self.journal_id.code == 'NABN':
            tipoDocumento = 'NABN'

        if self.journal_id.code == 'NCRE':
            tipoDocumento = 'NCRE'

        for line in self.invoice_line_ids:
            tag_impuestos = ""
            impuestoInguat = False
            for impuesto in line.tax_ids:
                # # print("tax detalle ", impuesto.fel_tax_nemonico)
                if impuesto.fel_tax_nemonico == 'ITH':
                    impuestoInguat = True
                    existeImpuestoInguat = True

            det_total = line.price_total  # line.price_unit*line.quantity #
            det_inguat = 0

            if impuestoInguat:
                det_base = line.price_unit / 1.22  # det_total / 1.22
                det_inguat = (det_base * 0.1) * line.quantity
                ImporteBruto = line.price_unit * line.quantity - det_inguat
                TotalesBruto = TotalesBruto - det_inguat
                precioUnitario = '{:.6f}'.format(det_base + det_base * 0.12)
                precioTotal = '{:.6f}'.format((det_base + det_base * 0.12) * line.quantity)
            else:
                det_base = det_total / 1.12
                det_inguat = 0
                ImporteBruto = line.price_unit * line.quantity
                precioUnitario = '{:.6f}'.format(line.price_unit)
                precioTotal = '{:.6f}'.format(line.price_unit * line.quantity)

            det_iva = line.price_subtotal * 0.12
            TotalesIva = TotalesIva + det_iva
            TotalesOtros = TotalesOtros + det_inguat

            total_iva = total_iva + det_iva
            total_inguat = total_inguat + det_inguat

            monto_impuesto = line.price_total - line.price_subtotal
            str_monto_impuesto = '{:.6f}'.format(monto_impuesto)
            # precioUnitario = '{:.6f}'.format(line.price_unit)
            # precioTotal = '{:.6f}'.format(line.price_unit * line.quantity)
            # precioTotal = '{:.6f}'.format(precioUnitario * line.quantity)

            if codigoMoneda == 'USD':
                montoGravable = line.price_total
                montoImpuesto = 0
            else:
                montoGravable = line.price_subtotal
                montoImpuesto = str_monto_impuesto

            if line.product_id.type == 'service':
                tipoProducto = "S"
            elif line.product_id.type == "product":
                tipoProducto = "B"
            else:
                tipoProducto = "B"

            # # print('Producto ',line.product_id.name)
            # Asignar variables
            det_total = line.price_total
            det_base = det_total / 1.12
            det_iva = '{:.6f}'.format(montoGravable * 0.12)

            # Armar xml
            tag_total_impuesto = ""

            tag_impuesto_inguat = ""
            if impuestoInguat:
                tag_impuesto_inguat = ("<dte:Impuesto>"
                                       f"<dte:NombreCorto>TURISMO HOSPEDAJE</dte:NombreCorto>"
                                       f"<dte:CodigoUnidadGravable>{codigoUnidadGravable}</dte:CodigoUnidadGravable>"
                                       f"<dte:MontoGravable>{'{:.6f}'.format(montoGravable)}</dte:MontoGravable>"
                                       f"<dte:MontoImpuesto>{'{:.6f}'.format(det_inguat)}</dte:MontoImpuesto>"
                                       f"</dte:Impuesto>")

            if AfiliacionIVA == 'GEN':
                tag_impuestos = ("<dte:Impuestos>"
                                 f"<dte:Impuesto>"
                                 f"<dte:NombreCorto>IVA</dte:NombreCorto>"
                                 f"<dte:CodigoUnidadGravable>{codigoUnidadGravable}</dte:CodigoUnidadGravable>"
                                 f"<dte:MontoGravable>{'{:.6f}'.format(montoGravable)}</dte:MontoGravable>"
                                 f"<dte:MontoImpuesto>{det_iva}</dte:MontoImpuesto>"
                                 f"</dte:Impuesto>"
                                 f"{tag_impuesto_inguat}"
                                 f"</dte:Impuestos>")

            if tipoDocumento == 'NABN':
                # Se omiten frases e impuestos en la nota de abono
                tagFrases = ""
                tag_impuestos = ""

            detalleXML = detalleXML + ("<dte:Item "
                                       f"BienOServicio='{tipoProducto}' NumeroLinea='{numeroLinea}'>"
                                       f"<dte:Cantidad>{line.quantity}</dte:Cantidad>"
                                       # f"<dte:UnidadMedida>{line.uom_id.name}</dte:UnidadMedida>"
                                       f"<dte:UnidadMedida>UNI</dte:UnidadMedida>"
                                       f"<dte:Descripcion>{line.name}</dte:Descripcion>"
                                       f"<dte:PrecioUnitario>{precioUnitario}</dte:PrecioUnitario>"
                                       f"<dte:Precio>{precioTotal}</dte:Precio>"
                                       f"<dte:Descuento>0.00</dte:Descuento>"
                                       f"{tag_impuestos}"
                                       f"<dte:Total>{'{:.6f}'.format(line.price_total)}0</dte:Total>"
                                       f"</dte:Item>")

        tag_total_impuesto_inguat = ""
        if existeImpuestoInguat:
            tag_total_impuesto_inguat = f"<dte:TotalImpuesto NombreCorto='TURISMO HOSPEDAJE' TotalMontoImpuesto='{str('{:.6f}'.format(total_inguat))}' />"

        tag_total_impuesto = ("<dte:TotalImpuestos>"
                              f"<dte:TotalImpuesto NombreCorto='IVA' TotalMontoImpuesto='{'{:.6f}'.format(total_iva)}' />"
                              f"{tag_total_impuesto_inguat}"
                              f"</dte:TotalImpuestos>")

        # self.inf_fecha_certifica = horaGuatemala.strftime("%Y-%m-%d %H:%M:%S") #comentado

        strComplementos = ""

        if tipoDocumento == 'NABN':
            tag_total_impuesto = ""

        if tipoDocumento == 'NCRE':
            account_move = self.env['account.move'].search([('id', '=', self.reversed_entry_id.id)])
            FechaEmisionDocumentoOrigen = account_move.invoice_date  # .fac_edx_fecha_emision
            AutrizacionOrigen = account_move.inf_numero_autorizacion
            MotivoAjuste = self.ref

            tagFrases = ""

            strComplementos = (""
                               f"<dte:Complementos>"
                               f"<dte:Complemento IDComplemento='1' NombreComplemento='NOTA CREDITO' URIComplemento='http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0'>"
                               f"<cno:ReferenciasNota xmlns:cno='http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0' "
                               f"FechaEmisionDocumentoOrigen='{FechaEmisionDocumentoOrigen}' MotivoAjuste='{MotivoAjuste}' "
                               f"NumeroAutorizacionDocumentoOrigen='{AutrizacionOrigen}' Version='1'/>"
                               f"</dte:Complemento>"
                               f"</dte:Complementos>")

        esquemaXML = (""
                      f"<dte:GTDocumento xmlns:ds='http://www.w3.org/2000/09/xmldsig#' xmlns:dte='http://www.sat.gob.gt/dte/fel/0.2.0' "
                      f" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' Version='0.1' "
                      f"  xsi:schemaLocation='http://www.sat.gob.gt/dte/fel/0.2.0' >"
                      f"<dte:SAT ClaseDocumento='dte'>"
                      f"<dte:DTE ID='DatosCertificados'>"
                      f"<dte:DatosEmision ID='DatosEmision'>"
                      f"<dte:DatosGenerales CodigoMoneda='{codigoMoneda}' FechaHoraEmision='{fechaEmision}' Tipo='{tipoDocumento}' />"
                      f"<dte:Emisor AfiliacionIVA='{AfiliacionIVA}' CodigoEstablecimiento='{codigoEstablecimiento}' CorreoEmisor='' NITEmisor='{nitEmisor}' NombreComercial='{nombreComercial}' NombreEmisor='{nombreEmisor}'>"
                      f"<dte:DireccionEmisor>"
                      f"<dte:Direccion>{direccionEmisor}</dte:Direccion>"
                      f"<dte:CodigoPostal>0</dte:CodigoPostal>"
                      f"<dte:Municipio>{self.company_id.city}</dte:Municipio>"
                      f"<dte:Departamento>{self.company_id.state_id.name}</dte:Departamento>"
                      f"<dte:Pais>GT</dte:Pais>"
                      f"</dte:DireccionEmisor>"
                      f"</dte:Emisor>"
                      f"<dte:Receptor CorreoReceptor='{correoReceptor}' IDReceptor='{receptorNIT}' NombreReceptor='{receptorNombre}'>"
                      f"<dte:DireccionReceptor>"
                      f"<dte:Direccion>{self.partner_id.street}</dte:Direccion>"
                      f"<dte:CodigoPostal>0</dte:CodigoPostal>"
                      f"<dte:Municipio />"
                      f"<dte:Departamento />"
                      f"<dte:Pais>GT</dte:Pais>"
                      f"</dte:DireccionReceptor>"
                      f"</dte:Receptor>"
                      f"{tagFrases}"
                      f"<dte:Items>"
                      f"{detalleXML}"
                      f"</dte:Items>"
                      f"<dte:Totales>"
                      f"{tag_total_impuesto}"
                      f"<dte:GranTotal>{'{:.6f}'.format(self.amount_total)}</dte:GranTotal>"
                      f"</dte:Totales>"
                      f"{strComplementos}"
                      f"</dte:DatosEmision>"
                      f"</dte:DTE>"
                      f"</dte:SAT>"
                      f"</dte:GTDocumento>")
        return esquemaXML

    def functionCadenaAnulaInfile(self):
        nitEmisor = self.company_id.vat.replace("-", "")

        if (self.partner_id.vat) == False:
            nitReceptor = "CF"
        else:
            nitReceptor = self.partner_id.vat.replace("-", "")

        # Aplica para facturas de exportación
        # if self.currency_id.name == 'USD':
        #    nitReceptor = 'CF'

        horaGMT = datetime.datetime.utcnow()
        horaGT = horaGMT - datetime.timedelta(hours=6)
        fechaHoraAnula = horaGT.strftime("%Y-%m-%dT%H:%M:%S") + '.000-06:00'
        fecha = self.invoice_date.strftime('%Y-%m-%d')
        hora = '00:00:00'  # fields.Datetime.context_timestamp(self, timestamp=datetime.now()).strftime('%H:%M:%S')
        # fecha_hora = self.inf_fecha_certifica
        fecha_hora = self.inf_fecha_certificaorg
        documentoAAnular = 'AEF671C4-D1D6-4965-BE62-C08CC0ED223E'
        cadenaAnulaXML = (
            "<dte:GTAnulacionDocumento xmlns:ds='http://www.w3.org/2000/09/xmldsig#' xmlns:dte='http://www.sat.gob.gt/dte/fel/0.1.0' xmlns:n1='http://www.altova.com/samplexml/other-namespace' "
            f" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' Version='0.1' xsi:schemaLocation='http://www.sat.gob.gt/dte/fel/0.1.0'>"
            f"<dte:SAT>"
            f"<dte:AnulacionDTE ID='DatosCertificados'>"
            f"<dte:DatosGenerales ID='DatosAnulacion'"
            f" NumeroDocumentoAAnular='{self.inf_numero_autorizacion}'"
            # f" NumeroDocumentoAAnular='{documentoAAnular}'"
            f" NITEmisor='{nitEmisor}'"
            f" IDReceptor='{nitReceptor}'"
            f" FechaEmisionDocumentoAnular='{fecha_hora}'"
            f" FechaHoraAnulacion='{fechaHoraAnula}'"
            f" MotivoAnulacion='Anulación de factura'>"
            f"</dte:DatosGenerales>"
            f"</dte:AnulacionDTE>"
            f"</dte:SAT>"
            f"</dte:GTAnulacionDocumento>")
        return cadenaAnulaXML
