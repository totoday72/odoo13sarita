# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class Company(models.Model):
    _inherit = 'res.company'

    fel_usuario_infile = fields.Char(string = "Usuario")
    fel_llave = fields.Char(string = "Llave")
    fel_identificador = fields.Char(string="Identificador")
    fel_id_maquina = fields.Char(string="Id Maquina",default='1')
    fel_url_solicita_firma = fields.Char(string="URL Solicita Firma")
    fel_url_certifica = fields.Char(string = "URL Certifica")
    fel_url_anula = fields.Char(string="URL Anula")
    fel_afiliacion_iva = fields.Selection([('GEN','IVA General'),('PEQ','Pequeño Contribuyente'),('EXE','Cliente Exento'),('GENS','IVA General Sarita Restaurante')],
                                          "Tipo afiliación IVA",default='GEN')
    fel_factura_con_infile = fields.Boolean(string="Facturar con éste módulo", default=True,
                                          help="Activo: Indica que éste módulo se utiliza por defecto para certificar facturas")
    fel_anula_con_infile = fields.Boolean(string="Anular con éste módulo", default=True,
                                          help="Activo: Indica que éste módulo se utiliza por defecto para ANULAR facturas")
    fel_nombre_comercial = fields.Char(string="Nombre Comercial")
    fel_razon_social = fields.Char(string="Razón Social",help="Nombre emisor")


