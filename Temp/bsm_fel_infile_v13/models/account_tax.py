# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ImpuestosFEL(models.Model):
    _inherit = 'account.tax'

    fel_tax_nemonico = fields.Char(string="Nemónico FEL",default="")
