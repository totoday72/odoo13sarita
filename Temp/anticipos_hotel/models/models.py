# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
import base64
import json
import uuid
import qrcode
from odoo.exceptions import RedirectWarning, MissingError, UserError, ValidationError
from io import BytesIO
import requests

class Visit(models.Model):
    _name = 'anticipo_hotel.gt'
    _description = 'Anticipo Hotel'
    _inherit = ["mail.thread", "mail.activity.mixin"]


    id_anticipo = fields.Char(string='No. Anticipo', default=lambda self: self.set_nuevo_anticipo(),readonly=True)
    company_gt = fields.Many2one(
        'res.company', string='Empresa:', required=True, index=True, store=True, ondelete="restrict",
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'), readonly=True)
    fecha = fields.Datetime(string='Fecha:', default=lambda self: self.set_fecha_actual(), readonly=True)
    usuario = fields.Char(string='Usuario:', default=lambda self: self._nombre_del_usuario(), readonly=True)
    cliente = fields.Char(string='Nombre del Cliente:')
    fecha_entrada = fields.Datetime("Fecha de Check In", required=True)
    fecha_salida = fields.Datetime("Fecha de Check Out", required=True)
    descripcion = fields.Char(string='Descripción del Anticipo:', default='Anticipo de Reservación ')
    partner_id = fields.Many2one('res.partner', string='Cliente', required=True)
    amount = fields.Float(string='Monto',  required=True)
    #currency_id = fields.Many2one('res.currency', string='Moneda')
    tipo_pago = fields.Selection([('efectivo', 'Efectivo (GTQ)'), ('dolares', 'Dolares (USD)'),('visa', 'Tarjeta Visa (GTQ)'), ('credo', 'Tarjeta Credo (GTQ)'),('dep_tra', 'Deposito/Transferencia (GTQ)'), ], 'Forma de Pago', default='efectivo',equired=True)
    detalle = fields.Char(string='Deposito, Transferencia o Autorizacion',  required=True)
    no_corte = fields.Integer(string='No. Corte', )
    estado_cliente = fields.Boolean(string='El Cliente esta en el Hotel', )
    no_folio = fields.Char(string='No. Folio', )

    # Método establece el nombre de usuario logueado.
    @api.model
    def _nombre_del_usuario(self):
        return str(self.env.user.name)

    # Retorna la fecha actual del sistema
    def set_fecha_actual(self):
        return datetime.datetime.utcnow()

    @api.onchange('tipo_pago')
    def _moneda(self):

        print('pago visa',self.tipo_pago)

        return


    # Establece el número de corte interno
    @api.model
    def set_nuevo_anticipo(self):
        query = "select max(id) from anticipo_hotel_gt"
        self.env.cr.execute(str(query))
        json_query = self.env.cr.dictfetchone()
        try:
            valor = json.loads(str(json_query).replace("'", "\""))
            correlativo = int(valor["max"]) + 1
        except:
            correlativo = 1
        return 'AH-' + str(correlativo)


class VisitReport(models.AbstractModel):

    _name='report.anticipo_hotel.report_visit_card'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('anticipo_hotel.report_visit_card')
        return {
            'doc_ids': docids,
            'doc_model': self.env['anticipo_hotel.gt'],
            'docs': self.env['anticipo_hotel.gt'].browse(docids)
        }