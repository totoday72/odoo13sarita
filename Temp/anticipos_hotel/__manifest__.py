# -*- coding: utf-8 -*-
{
    'name': "Anticipos Hotel",
    'version': '13.0.0.0.1',
    'summary': 'Anticipos Hotel Sarita',
    'author': 'Aníbal Gómez Morales',
    'maintainer': 'Aníbal Gómez Morales',
    'Company': 'RESASA',
    'website': 'https://saritarestaurante.com/',

    # any module necessary for this one to work correctly
    'depends': ['base','sale_management'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'reports/anticipo_hotel.xml',


    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

    "images": [
        'static/description/icon.png'],

    'application': True,
}
