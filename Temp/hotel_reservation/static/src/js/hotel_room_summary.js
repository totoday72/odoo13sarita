odoo.define("hotel_reservation.hotel_room_summary", function (require) {
    "use strict";

    var core = require("web.core");
    var registry = require("web.field_registry");
    var basicFields = require("web.basic_fields");
    var FieldText = basicFields.FieldText;
    var QWeb = core.qweb;
    var FormView = require("web.FormView");
    var py = window.py;

    var MyWidget = FieldText.extend({
        events: _.extend({}, FieldText.prototype.events, {
            change: "_onFieldChanged",
        }),
        init: function () {
            this._super.apply(this, arguments);
            if (this.mode === "edit") {
                this.tagName = "span";
            }
            this.set({
                date_to: false,
                date_from: false,
                summary_header: false,
                room_summary: false,
            });
            this.set({
                summary_header: py.eval(this.recordData.summary_header),
            });
            this.set({
                room_summary: py.eval(this.recordData.room_summary),
            });
        },
        start: function () {
            var self = this;
            if (self.setting) {
                return;
            }
            if (!this.get("summary_header") || !this.get("room_summary")) {
                return;
            }
            this.renderElement();
            this.view_loading();
        },
        initialize_field: function () {
            FormView.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:summary_header", self, self.start);
            self.on("change:room_summary", self, self.start);
        },
        view_loading: function (r) {
            return this.load_form(r);
        },

        load_form: function () {
            var self = this;
            this.$el.find(".table_free").bind("click", function () {
                var timeoff = moment.duration("00:01:59"); //Erick
                var timeoff1 = moment.duration("00:01:59"); //Erick
                var timeH = moment($(this).attr("date")).add(15, 'hours').subtract(timeoff).format('Y-MM-DD HH:mm:ss'); //Erick
                var timeG = moment($(this).attr("date")).add(37,'hours').subtract(timeoff1).format('Y-MM-DD HH:mm:ss');
                self.do_action({
                    name: "Hacer una reservacion rapida",
                    type: "ir.actions.act_window",
                    res_model: "quick.room.reservation",
                    views: [[false, "form"]],
                    target: "new",
                    context: {
                        room_id: $(this).attr("data"),
                        date: timeH,
                        date1: timeG,
                        default_adults: 1,
                    },
                });
            });
            // Erick. Se coloco este metodo para las celdas que utilicen el css table_blocked para que busque
            // la habitacion bloqueada por medio del id y la vista generada por el modelo hotel.room
            // hay que tener en cuenta que hay que convertir a numero de lo contrario no se puede encontrar el id, porque
            // devuelve el objeto (duda).
            this.$el.find(".table_blocked").bind("click", function () {
                var room_id = parseInt($(this).attr("room_id"))
                // console.log(room_id)
                // console.log($(this).attr("data"))
                self.do_action({
                    name: "Ver Habitacion bloqueada",
                    type: "ir.actions.act_window",
                    res_model: "hotel.room",
                    views: [[false, "form"]],
                    target: "current",
                    res_id: room_id,
                    context: {
                        room_id: $(this).attr("room_id"),
                        // reservation_id: $(this).attr("reservation_id"),
                    },
                });
            });

            this.$el.find(".table_reserved").bind("click", function () {
                var reservation_id = parseInt($(this).attr("reservation_id")) // el valor de la variable esta en  t-att-reservation_id="status.reservation_id_id" en hotel_room_summary.xml
                // console.log(reservation_id)
                // console.log($(this).attr("reservation_id"))
                self.do_action({
                    name: "Ver Reservacion",
                    type: "ir.actions.act_window",
                    res_model: "hotel.reservation",
                    views: [[false, "form"]],
                    target: "current",
                    res_id: reservation_id,
                    context: {
                        room_id: $(this).attr("reservation_id"),
                        // reservation_id: $(this).attr("reservation_id"),
                    },
                });
            });
        },
        renderElement: function () {
            this._super();
            this.$el.html(
                QWeb.render("RoomSummary", {
                    widget: this,
                })
            );
        },
        _onFieldChanged: function (event) {
            this._super();
            this.lastChangeEvent = event;
            this.set({
                summary_header: py.eval(this.recordData.summary_header),
            });
            this.set({
                room_summary: py.eval(this.recordData.room_summary),
            });
            this.renderElement();
            this.view_loading();

        },

    });

    registry.add("Room_Reservation", MyWidget);
    return MyWidget;
});
