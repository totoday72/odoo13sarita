# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class ReportCheckin(models.AbstractModel):
    _name = "report.hotel_reservation.report_checkin_qweb"
    _description = "Auxiliar to get the check in report"

    def _get_checkin_reservation(self, date_start, date_end):
        reservations = self.env["hotel.reservation"].search(
            [("checkin", ">=", date_start), ("checkin", "<=", date_end)]
        )
        return reservations

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data.get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
                :10
            ],
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_checkin_reservation = rm_act._get_checkin_reservation(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "get_checkin": _get_checkin_reservation,
        }


class ReportCheckout(models.AbstractModel):
    _name = "report.hotel_reservation.report_checkout_qweb"
    _description = "Auxiliar to get the check out report"

    def _get_checkout_reservation(self, date_start, date_end):
        reservations = self.env["hotel.reservation"].search(
            [("checkout", ">=", date_start), ("checkout", "<=", date_end)]
        )
        return reservations

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data.get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
                :10
            ],
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_checkout_reservation = rm_act._get_checkout_reservation(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "get_checkout": _get_checkout_reservation,
        }


class ReportMaxroom(models.AbstractModel):
    _name = "report.hotel_reservation.report_maxroom_qweb"
    _description = "Auxiliar to get the room report"

    def _get_room_used_detail(self, date_start, date_end):
        room_used_details = []
        hotel_room_obj = self.env["hotel.room"]
        for room in hotel_room_obj.search([]):
            counter = 0
            details = {}
            if room.room_reservation_line_ids:
                end_date = datetime.strptime(
                    date_end, DEFAULT_SERVER_DATETIME_FORMAT
                )
                start_date = datetime.strptime(
                    date_start, DEFAULT_SERVER_DATETIME_FORMAT
                )
                counter = len(
                    room.room_reservation_line_ids.filtered(
                        lambda l: start_date <= l.check_in <= end_date
                    )
                )
            if counter >= 1:
                details.update(
                    {"name": room.name or "", "no_of_times_used": counter}
                )
                room_used_details.append(details)
        return room_used_details

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
                :10
            ],
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_room_used_detail = rm_act._get_room_used_detail(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "get_room_used_detail": _get_room_used_detail,
        }


class ReportRoomReservation(models.AbstractModel):
    _name = "report.hotel_reservation.report_room_reservation_qweb"
    _description = "Auxiliar to get the room report"

    def _get_reservation_data(self, date_start, date_end):
        reservation = self.env["hotel.reservation"].search(
            [("checkin", ">=", date_start), ("checkout", "<=", date_end)]
        )
        return reservation

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data.get("date_start", fields.Date.today())
        date_end = data["form"].get(
            "date_end",
            str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[
                :10
            ],
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_reservation_data = rm_act._get_reservation_data(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "get_data": _get_reservation_data,
        }

class Report_huespedes_0(models.AbstractModel):
    _name = "report.hotel_reservation.report_huespedes_0_qweb"
    _description = "Obtener los datos para el reporte de huespedes"

    def _get_reservation_data(self, date_start, date_end):
        # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        reservation = self.env["hotel.reservation"].search(
            ['&', ("checkout", ">=", str(date_start)[0:10]), ("checkin", "<=", str(date_end)[0:10])]
        )
        return reservation

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )
        date_end = data["form"].get(
            "date_end"
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_reservation_data = rm_act._get_reservation_data(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(date_start)[0:10],
            "end_date": str(date_end)[0:10],
            "reservations": _get_reservation_data,
            "company": self.env.user.company_id,
        }
class Reservacion_reporte(): # Erick -> esta clase se creo para poder colocar dentro de la clase habitacion una o muchas reservaciones.
    cuenta = ""
    tipo = ""
    adultos = ""
    ninos = ""
    tipo_tarifa = ""
    nombre_huesped = ""
    ingreso = ""
    salida = ""
    observaciones = ""
    status = ""
    operador = ""

    def __init__(self):
        self.cuenta = ""
        self.tipo = ""
        self.adultos = ""
        self.ninos = ""
        self.tipo_tarifa = ""
        self.nombre_huesped = ""
        self.ingreso = ""
        self.salida = ""
        self.observaciones = ""
        self.status = ""
        self.operador = ""

class Habitacion_reporte(): # Erick-> esta clase se creo para poder tener un objeto para la creacion de reportes
    planta = ""
    bloqueado_desde = ""
    bloqueado_hasta = ""
    observaciones = ""
    nombre_habitacion = ""
    status = ""
    bloqueado = False
    mensaje = ""
    reservaciones = []


    def __init__(self):
        self.nombre_habitacion = ""
        self.status = ""
        self.reservaciones = []
        self.planta = ""
        self.bloqueado_desde = ""
        self.bloqueado_hasta = ""
        self.observaciones = ""
        self.bloqueado = False
        self.mensaje = ""

class Report_habitaciones_Class(models.AbstractModel):
    _name = "report.hotel_reservation.report_habitaciones_control_class"
    _description = "Obtener los datos para el reporte de control de habitacioens"

    def _get_room_data(self, date_start, date_end):
        # # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        # # # print(str(date_start), " 2da fecha  ", str(date_end))
        reservation = self.env["hotel.room"].search(
                []
            )
        return reservation


    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )
        date_end = data["form"].get(
            "date_end"
        )
        # date_start = str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        # date_end = str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_room_data = rm_act._get_room_data(date_start, date_end)
        habitaciones = []
        for habitacion in _get_room_data:
            x = Habitacion_reporte()
            habname = habitacion.name.split(' ')
            # # print(habname)
            x.nombre_habitacion = ""
            if habname[0].upper() == "HOSPEDAJE":
                try:
                    x.nombre_habitacion = habname[2]
                except:
                    x.nombre_habitacion = habitacion.name
            else:
                x.nombre_habitacion = habitacion.name
            # x.nombre_habitacion = habitacion.name
            reservation = habitacion.room_reservation_line_ids.search(
                        ['&', ("check_out", ">=", str(date_start)), ("check_in", "<=", str(date_end)), ("room_id", "=", habitacion.id)]
                    )
            if not reservation:
                # # print("no hay reservaciones")
                if habitacion.repair or habitacion.maintenance:
                    reserve = Reservacion_reporte()
                    reserve.cuenta = "*****"
                    reserve.tipo = ""
                    reserve.adultos = ""
                    reserve.ninos = ""
                    reserve.tipo_tarifa = ""
                    reserve.nombre_huesped = "Habitacion Bloqueada"
                    reserve.ingreso = habitacion.repair_maintenance_from_date.strftime("%d %B de %Y")
                    reserve.salida = habitacion.repair_maintenance_to_date.strftime("%d %B de %Y")
                    reserve.status = str(habitacion.status)
                    reserve.observaciones = habitacion.observations_repair_maintenance
                    # # print("Reservacion: ", str(r.reservation_id.reservation_no), " Habitacion: ", habitacion.name)
                    x.reservaciones.append(reserve)
            for r in reservation:
                reserve = Reservacion_reporte()
                reserve.cuenta = r.reservation_id.reservation_no
                reserve.tipo = habitacion.room_categ_id.name
                reserve.adultos = r.reservation_id.adults
                reserve.ninos = r.reservation_id.children
                reserve.tipo_tarifa = r.reservation_id.pricelist_id.name
                reserve.nombre_huesped = r.reservation_id.partner_id.name
                reserve.ingreso = r.reservation_id.checkin.strftime('%d/%B/%Y')
                reserve.salida = r.reservation_id.checkout.strftime('%d/%B/%Y')
                reserve.status = str(habitacion.status)
                reserve.observaciones = r.reservation_id.observations
                # # print("Reservacion: ", str(r.reservation_id.reservation_no), " Habitacion: ", habitacion.name)
                x.reservaciones.append(reserve)
            # for t in x.reservaciones:
                # # print(t.cuenta)
            habitaciones.append(x)

        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6)),
            "end_date": str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6)),
            "rooms": habitaciones,
            "company": self.env.user.company_id,
        }

class Report_habitaciones_bloqueadas_report(models.AbstractModel):
    _name = "report.hotel_reservation.report_habitaciones_bloqueadas_report"
    _description = "Obtener los datos para el reporte de habitacioens bloqueadas"

    def _get_room_data(self, date_start, date_end):
        # # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        # # # print(str(date_start), " 2da fecha  ", str(date_end))
        reservation = self.env["hotel.room"].search(
                ['|', ("maintenance", "=", True), ("repair", "=", True)]
            )
        return reservation


    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )
        date_end = data["form"].get(
            "date_end"
        )
        # date_start = str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        # date_end = str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_room_data = rm_act._get_room_data(date_start, date_end)
        habitaciones = []
        for habitacion in _get_room_data:
            x = Habitacion_reporte()
            habname = habitacion.name.split(' ')
            # # print(habname)
            x.nombre_habitacion = ""
            if habname[0].upper() == "HOSPEDAJE":
                try:
                    x.nombre_habitacion = habname[2]
                except:
                    x.nombre_habitacion = habitacion.name
            else:
                x.nombre_habitacion = habitacion.name
            # x.nombre_habitacion = habitacion.name
            x.planta = habitacion.floor_id.name or "General"
            x.bloqueado_desde = habitacion.repair_maintenance_from_date.strftime("%d %B de %Y %H:%M")
            x.bloqueado_hasta = habitacion.repair_maintenance_to_date.strftime("%d %B de %Y %H:%M")
            x.observaciones = habitacion.observations_repair_maintenance
            x.bloqueado = habitacion.maintenance or habitacion.repair
            habitaciones.append(x)

        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6)),
            "end_date": str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6)),
            "rooms": habitaciones,
            "company": self.env.user.company_id,
        }


class Report_reservaciones_diarias_report(models.AbstractModel):
    _name = "report.hotel_reservation.reporte_reservaciones_diarias"
    _description = "Obtener los datos para el reporte de habitacioens bloqueadas"

    def _get_room_data(self, date_start, date_end):
        # # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        # # # print(str(date_start), " 2da fecha  ", str(date_end))
        reservation = self.env["hotel.room"].search(
                []
            )
        return reservation


    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )[0:10] + " 00:00:00"
        date_end = data["form"].get(
            "date_end"
        )[0:10] + " 23:59:59"
        # date_start = str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        # date_end = str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_room_data = rm_act._get_room_data(date_start, date_end)
        habitaciones = []
        for habitacion in _get_room_data:
            x = Habitacion_reporte()
            habname = habitacion.name.split(' ')
            # # print(habname)
            habcomp = ""
            if habname[0].upper() == "HOSPEDAJE":
                try:
                    x.nombre_habitacion = habname[2]
                except:
                    x.nombre_habitacion = habitacion.name
            else:
                x.nombre_habitacion = habitacion.name
            # x.nombre_habitacion = habitacion.name
            if habitacion.repair:
                x.bloqueado = habitacion.repair
            else:
                x.bloqueado = habitacion.maintenance
            if habitacion.repair or habitacion.maintenance:
                x.mensaje = "Habitacion Bloqueada"
                x.planta = habitacion.floor_id.name or "General"
                x.bloqueado_desde = habitacion.repair_maintenance_from_date.strftime("%d %B de %Y %H:%M")
                # # # print(x.bloqueado_desde, "Bloqueado desde....", str(x.bloqueado))
                x.bloqueado_hasta = habitacion.repair_maintenance_to_date.strftime("%d %B de %Y %H:%M")
                x.observaciones = habitacion.observations_repair_maintenance
            reservation = habitacion.room_reservation_line_ids.search(
                        ['&', "&", ("check_in", ">=", date_start), ("check_in", "<=", (date_end)), ("room_id", "=", habitacion.id)]
                    )

            for r in reservation:
                reserve = Reservacion_reporte()
                reserve.operador = r.reservation_id.operator.name
                reserve.cuenta = r.reservation_id.reservation_no
                reserve.tipo = habitacion.room_categ_id.name
                reserve.adultos = r.reservation_id.adults
                reserve.ninos = r.reservation_id.children
                reserve.tipo_tarifa = r.reservation_id.pricelist_id.name
                if habitacion.repair or habitacion.maintenance:
                    reserve.nombre_huesped = habitacion.observations_repair_maintenance
                    reserve.ingreso = habitacion.repair_maintenance_from_date.strftime("%d %B de %Y %H:%M")
                    reserve.salida = habitacion.repair_maintenance_to_date.strftime("%d %B de %Y %H:%M")
                else:
                    reserve.nombre_huesped = r.reservation_id.partner_id.name
                    reserve.ingreso = r.reservation_id.checkin.strftime('%d/%B/%Y')
                    reserve.salida = r.reservation_id.checkout.strftime('%d/%B/%Y')
                reserve.status = str(habitacion.status)
                reserve.observaciones = r.reservation_id.observations
                # # print("Reservacion: ", str(r.reservation_id.reservation_no), " Habitacion: ", habitacion.name)
                x.reservaciones.append(reserve)
            # for t in x.reservaciones:
                # # print(t.cuenta)
            habitaciones.append(x)

        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S'))[0:10],
            "end_date": str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))[0:10],
            "rooms": habitaciones,
            "company": self.env.user.company_id,
        }

class Report_reservaciones_mes_report(models.AbstractModel):
    _name = "report.hotel_reservation.reporte_reservaciones_mes"
    _description = "Obtener los datos para el reporte de reservaciones por mes"

    def _get_reservation_data(self, anio, mes,ultimodia):
        reservation = self.env["hotel.reservation"].search(
            ["&", "&", ("checkout", ">=", str(anio) + '-' + str(mes) + '-' + '1'),
             ("checkin", "<=", str(anio) + '-' + str(mes) + '-' + str(ultimodia)),
             ("state", "=", "confirm")], order='checkin asc'
        )
        # for i in reservation:
        #     # print("Reservation no: ", i.reservation_no, " Checkin: ", str(i.checkin))
        return reservation

    def _get_room_data(self):
        # # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        # # # print(str(date_start), " 2da fecha  ", str(date_end))
        room = self.env["hotel.room"].search(
                []
            )
        return room

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )[0:10] + " 00:00:00"
        date_end = data["form"].get(
            "date_end"
        )[0:10] + " 23:59:59"
        # date_start = str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        # date_end = str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))
        fechainicial = date_start[0:10].split('-')
        anio = int(fechainicial[0])
        mes = int(fechainicial[1])
        mes_siguiente = int(fechainicial[1]) + 1
        ultimodia = 0
        dias = [31, 30, 29, 28, 27]
        for i in dias:
            try:
                ultimodia = int(datetime(anio, mes, i).day)
                break
            except ValueError:
                continue
        # # print("Anio:", anio)
        # # print("mes: ", mes)
        # # print("Ultimo dia del mes:", ultimodia)
        # # print("Mes siguiente:", mes_siguiente)
        rm_act = self.with_context(data["form"].get("used_context", {}))
        rooms = rm_act._get_room_data()
        habitaciones = []
        encabezado = Habitacion_reporte()
        encabezado.nombre_habitacion = "Hab."
        for t in range(1, ultimodia + 1):
            reserve = Reservacion_reporte()
            reserve.status = str(t)
            encabezado.reservaciones.append(reserve)
        for room in rooms:
            x = Habitacion_reporte()
            habitacionname = room.name.split(' ')
            if habitacionname[0].upper() == "HOSPEDAJE":
                try:
                    x.nombre_habitacion = habitacionname[2]
                except:
                    x.nombre_habitacion = room.name
            else:
                x.nombre_habitacion = room.name
            x.bloqueado = False
            # # print(x.nombre_habitacion)
            habitaciones.append(x)
        for t in range(1, ultimodia + 1):
            # # print("Valor de Dia", t)

            reservationss = self.env["hotel.reservation"].search(
                [
                    "|", "|",
                    "&", "&",
                    ("checkin", "<", str(anio) + '-' + str(mes) + '-' + str(1) + " 22:00:00"),
                    ("checkout", ">=", str(anio) + '-' + str(mes) + '-' + str(t) + " 18:00:00"),
                    "|",
                    ("state", "=", "confirm"),
                    ("state", "=", "done"),
                    "&", "&",
                    ("checkin", "<=", str(anio) + '-' + str(mes) + '-' + str(t) + " 22:00:00"),
                    ("checkout", ">=", str(anio) + '-' + str(mes) + '-' + str(t) + " 18:00:00"),
                    "|",
                    ("state", "=", "confirm"),
                    ("state", "=", "done"),
                    "&", "&",
                    ("checkin", "<=", str(anio) + '-' + str(mes) + '-' + str(t) + " 22:00:00"),
                    ("checkout", ">", str(anio) + '-' + str(mes) + '-' + str(ultimodia) + " 18:00:00"),
                    "|",
                    ("state", "=", "confirm"),
                    ("state", "=", "done"),
                 ], order='checkin asc'
            )

            # reservationss = self.env["hotel.reservation"].search(
            #     ["&", "&", ("checkout", ">=", str(anio) + '-' + str(mes) + '-' + str(t) + " 00:00:00"),
            #      ("checkin", "<=", str(anio) + '-' + str(mes) + '-' + str(t) + " 23:59:59"),
            #      ("state", "=", "confirm")], order='checkin asc'
            # )
            if reservationss:
                for i in reservationss:
                    room_reserve = []
                    # # print(str(i.id), "*****************Reservation no: ", i.reservation_no, " Checkin: ", str(i.checkin), " Checkout: ", str(i.checkout))
                    query = '''
                            SELECT id FROM hotel_reservation_line
                            WHERE line_id = %s
                            '''
                    self.env.cr.execute(query, (i.id,))
                    line_reservation = self.env.cr.dictfetchall()
                    # # print(line_reservation)
                    for line in line_reservation:
                        id = line.get("id")
                        query = '''
                                    SELECT room_id FROM hotel_reservation_line_room_rel
                                    WHERE hotel_reservation_line_id = %s
                                '''
                        self.env.cr.execute(query, (id,))
                        rooms_ids = self.env.cr.dictfetchall()
                        # # print(rooms_ids)
                        for room in rooms_ids:
                            id_room = room.get("room_id")
                            # print(id_room)
                            room_temp = self.env["hotel.room"].search(
                                [("id", "=", id_room)], order='id asc'
                            )
                            # # print(room_temp.name)
                            room_reserve.append(room_temp)
                    for r in room_reserve:
                        # # print(r.name)
                        habname = []
                        for rr in habitaciones:
                            try:
                                habname = r.name.split(' ')
                            except:
                                habname[0] = r.name
                            # print(habname)
                            habcomp = ""
                            if habname[0].upper() == "HOSPEDAJE":
                                try:
                                    habcomp = habname[2]
                                except:
                                    habcomp = r.name
                            else:
                                habcomp = r.name
                            if rr.nombre_habitacion == habcomp and not rr.bloqueado:
                                # # print(habcomp, " **** ", rr.nombre_habitacion)
                                reserve = Reservacion_reporte()
                                reserve.status = "√"  # "√" i.reservation_no + " Dia: " + str(t)
                                rr.bloqueado = True
                                rr.reservaciones.append(reserve)
                                # # print("Habitacion: ", rr.nombre_habitacion, " Reservation: ", i.reservation_no, " Dia: ", str(t))
                for ro in habitaciones:
                    if not ro.bloqueado:
                        reserve = Reservacion_reporte()
                        reserve.status = " "  # "X" + " Dia: " + str(t)
                        ro.reservaciones.append(reserve)
                        # # print("Habitacion: ", ro.nombre_habitacion, " NO Bloqueado: ", "X", " Dia: ", str(t))
                    # else:
                    #     # print("Habitacion: ", ro.nombre_habitacion, " SI BLOQUEADO: ", "X", " Dia: ", str(t))
                    ro.bloqueado = False
            else:
                for i in habitaciones:
                    reserve = Reservacion_reporte()
                    reserve.status = " "  # "X" + " Dia: " + str(t)
                    i.reservaciones.append(reserve)
                    # # print("Habitacion: ", i.nombre_habitacion, " Reservation: ", "X", " Dia: ", str(t))
        # contador = 1
        # for r in habitaciones:
        #     # print("Revision habitacion: ", r.nombre_habitacion)
        #     for x in r.reservaciones:
        #         # print(str(contador), "=>", x.status)
        #         contador += 1
        #     contador = 1

        # # print("****************************************************************************************")
        # raise ValidationError(" DEBUG ")
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S').strftime('%B - %Y')),
            "end_date": str(datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S') - timedelta(hours=6))[0:10],
            "rooms": habitaciones,
            "encabezado": encabezado,
            "company": self.env.user.company_id,
        }

class Report_espera_habitaciones_0(models.AbstractModel):
    _name = "report.hotel_reservation.reporte_espera_habitaciones"
    _description = "Obtener los huespedes que esperan habitacion cuando estan ocupadas."

    def _get_reservation_data(self, date_start, date_end):
        # # print(str(date_start)[0:10], "   ", str(date_end)[0:10])
        reservation = self.env["hotel.reservation"].search(
            ['&', ("checkout", ">=", str(date_start)[0:10]), '&', ("espera_de_ocupacion", "=", True), '&', ("state", "=", 'waiting_room'), ("checkin", "<=", str(date_end)[0:10])]
        )
        return reservation

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        if not docids:
            docids = data["form"].get("docids")
        folio_profile = self.env["hotel.reservation"].browse(docids)
        date_start = data["form"].get(
            "date_start"
        )
        date_end = data["form"].get(
            "date_end"
        )
        rm_act = self.with_context(data["form"].get("used_context", {}))
        _get_reservation_data = rm_act._get_reservation_data(
            date_start, date_end
        )
        return {
            "doc_ids": docids,
            "doc_model": active_model,
            "data": data["form"],
            "docs": folio_profile,
            "time": time,
            "start_date": str(date_start)[0:10],
            "end_date": str(date_end)[0:10],
            "reservations": _get_reservation_data,
            "company": self.env.user.company_id,
        }