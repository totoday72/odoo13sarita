# See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class HotelReservationWizard(models.TransientModel):
    _name = "hotel.reservation.wizard"
    _description = "Allow to generate a reservation"

    date_start = fields.Datetime("Start Date", required=True)
    date_end = fields.Datetime("End Date", required=True)

    def report_reservation_detail(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.hotel_roomres_details"
        ).report_action(self, data=data)

    def report_checkin_detail(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.hotel_checkin_details"
        ).report_action(self, data=data)

    def report_checkout_detail(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.hotel_checkout_details"
        ).report_action(self, data=data)

    def report_maxroom_detail(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.hotel_maxroom_details"
        ).with_context(landscape=True).report_action(self, data=data)
# *************************************************SARITA HOTEL*********************************************************************
    def report_control_huespedes_0(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.hotel_huespedes_0"
        ).with_context(landscape=True).report_action(self, data=data)

    def report_control_habitaciones(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.report_control_habitaciones_report"
        ).with_context(landscape=True).report_action(self, data=data)

    def report_control_habitaciones_bloqueadas(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.report_habitaciones_bloqueadas_report"
        ).with_context(landscape=True).report_action(self, data=data)

    def report_control_reservaciones_diarias(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.report_reservaciones_diarias"
        ).with_context(landscape=True).report_action(self, data=data)


    def report_control_reservaciones_mes(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.report_reservaciones_mes"
        ).with_context().report_action(self, data=data)

    def report_espera_habitacion(self):
        data = {
            "ids": self.ids,
            "model": "hotel.reservation",
            "form": self.read(["date_start", "date_end"])[0],
        }
        return self.env.ref(
            "hotel_reservation.report_espera_habitacion"
        ).with_context(landscape=True).report_action(self, data=data)
# *************************************************SARITA HOTEL*********************************************************************

class MakeFolioWizard(models.TransientModel):
    _name = "wizard.make.folio"
    _description = "Allow to generate the folio"

    grouped = fields.Boolean("Group the Folios")

    def make_folios(self):
        reservation_obj = self.env["hotel.reservation"]
        newinv = [
            order.id
            for order in reservation_obj.browse(
                self.env.context.get("active_ids", [])
            ).mapped("folios_ids")
        ]
        return {
            "domain": "[('id','in', [" + ",".join(map(str, newinv)) + "])]",
            "name": "Folios",
            "view_type": "form",
            "view_mode": "tree,form",
            "res_model": "hotel.folio",
            "view_id": False,
            "type": "ir.actions.act_window",
        }
