from odoo import api, models, fields

class AccountMove(models.Model):
    _inherit = "sale.order"

    inguat_cards = fields.Char("Boletas de Inguat", help="Ingrese los numeros separados por , (coma) sin espacios.")