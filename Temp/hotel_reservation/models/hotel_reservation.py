# See LICENSE file for full copyright and licensing details.

from datetime import timedelta, datetime

import simplejson
from dateutil.relativedelta import relativedelta
from emoji import emojize
from lxml import etree

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from odoo.addons.website.tools import get_video_embed_code

DEFAULT_MONTH_FORMAT = '%m/%Y'

class HotelReservation(models.Model):
    _name = "hotel.reservation"
    _rec_name = "reservation_no"
    _description = "Reservation"
    _order = "reservation_no desc"
    _inherit = ["mail.thread", "mail.activity.mixin"]

    def _default_warehouse(self):
        warehouses = self.env["stock.warehouse"].search([('company_id', '=', self.env.company.id)])
        if len(warehouses) > 0:
            return warehouses[0].id
        else:
            return False
    def _compute_folio_id(self):
        for res in self:
            res.update({"no_of_folio": len(res.folios_ids.ids)})

    reservation_no = fields.Char("Reservation No", readonly=True, copy=False)
    date_order = fields.Datetime(
        "Date Ordered",
        readonly=True,
        required=True,
        index=True,
        default=lambda self: fields.Datetime.now(),
    )
    observations = fields.Char("Notas y Observaciones", track_visibility='onchange')
    inguat_cards = fields.Char("Tarjetas de Inguat", required=True, default="0", help="Ingrese los numeros separados por , (coma) sin espacios.")
    warehouse_id = fields.Many2one(
        "stock.warehouse",
        "Hotel",
        readonly=True,
        required=True,
        default=_default_warehouse,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
    )
    partner_id = fields.Many2one(
        "res.partner",
        "Guest Name",
        readonly=True,
        required=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]}, track_visibility='onchange'
    )
    pricelist_id = fields.Many2one(
        "product.pricelist",
        "Scheme",
        required=True,
        readonly=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
        help="Pricelist for current reservation.", track_visibility='onchange'
    )
    partner_invoice_id = fields.Many2one(
        "res.partner",
        "Invoice Address",
        readonly=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
        help="Invoice address for current reservation.", track_visibility='onchange'
    )
    partner_order_id = fields.Many2one(
        "res.partner",
        "Ordering Contact",
        readonly=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
        help="The name and address of the "
             "contact that requested the order "
             "or quotation.", track_visibility='onchange'
    )
    partner_shipping_id = fields.Many2one(
        "res.partner",
        "Delivery Address",
        readonly=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
        help="Delivery address" "for current reservation. ",
    )
    checkin = fields.Datetime(
        "Expected-Date-Arrival",
        required=True,
        readonly=False, track_visibility='onchange'
    )
    checkout = fields.Datetime(
        "Expected-Date-Departure",
        required=True, track_visibility='onchange'
    )
    # Erick este codigo estaba en checkout y checkin -> states = {"draft": [("readonly", False)]},
    adults = fields.Integer(
        "Adults",
        readonly=True,
        states={"draft": [("readonly", False)], "waiting_room": [("readonly", False)]},
        help="Number of adults there in the guest list. ", track_visibility='onchange'
    )
    children = fields.Integer(
        "Niños",
        readonly=True,
        states={"draft": [("readonly", False)]},
        help="Number of children there in guest list.", track_visibility='onchange'
    )
    reservation_line_ids = fields.One2many(
        "hotel_reservation.line",
        "line_id",
        "Reservation Line",
        help="Hotel room reservation details.", track_visibility='onchange'
    )
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("waiting_room", "Esperando Habitación"),
            ("confirm", "Confirm"),
            ("cancel", "Cancel"),
            ("done", "Done"),
        ],
        "State",
        readonly=True,
        default="draft", track_visibility='onchange'
    )
    folios_ids = fields.Many2many(
        "hotel.folio",
        "hotel_folio_reservation_rel",
        "order_id",
        "invoice_id",
        string="Folio",
    )
    no_of_folio = fields.Integer("No. Folio", compute="_compute_folio_id")

    operator = fields.Many2one(
        "res.users", "Operador", default=lambda self: self.env.user
    )
    invoiced = fields.Boolean("Facturado: ", default=False, help="Verifica si este campo ya se facturo")
    credit_card_date = fields.Char("Fecha de Vencimiento: ", track_visibility='onchange' )
    credit_card_number = fields.Char("Número de Tarjeta: ", track_visibility='onchange')
    espera_de_ocupacion = fields.Boolean("En espera de Habitación: ", default=False)
    embed_code = fields.Char(compute="_charge_video")
    video_url = fields.Char('Video Url', help="Agregar link de video")

    @api.depends("video_url")
    def _charge_video(self):
        for rec in self:
            rec.embed_code = get_video_embed_code(rec.video_url)


    @api.onchange("espera_de_ocupacion")
    def cambiar_estado(self):
        # print(self.env.context)
        # print(self._context)
        reservacion = self.env["hotel.reservation"].search([("id", "=", self._origin.id)])
        if self.espera_de_ocupacion:
            self.state = "waiting_room"
            reservacion.state = "waiting_room"
        else:
            self.state = "draft"
            reservacion.state = "draft"

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(HotelReservation, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        # print(self.env.context.get("params"))
        if view_type == 'form':
            # print(self._context)

            doc = etree.XML(result['arch'])
            node = doc.xpath("//field[@name='invoiced']")[0]
            node.set('invisible', '0')
            node.set('modifiers', simplejson.dumps({"invisible": True}))
            result['arch'] = etree.tostring(doc)
        # for i in result:
        #     print("Data:" + str(i))
        # raise ValidationError(
        #     _(
        #         "La capacidad de la habitacion a sido superada!\n"
        #         " Por favor selecciona otra habitacion o cambia la cantidad de personas"
        #         " ."
        #     )
        # )
        # if view_type == 'form' and self._module == 'hotel_reservation':
        #     if doc.xpath("//field[@name='reservation_no']"):
        #
        #         # for placeholder in doc.xpath(
        #         #         "//field[@name='amount_currency']"):
        #         #     elem = etree.Element(
        #         #         'field', {
        #         #             'name': 'balance',
        #         #             'readonly': 'True'
        #         #         })
        #         #     orm.setup_modifiers(elem)
        #         #     placeholder.addprevious(elem)
        #     result['arch'] = etree.tostring(doc)
        return result

    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        for reserv_rec in self:
            if reserv_rec.state != "draft":
                raise ValidationError(
                    _(
                        "Sorry, you can only delete the reservation when it's draft!"
                    )
                )
        return super(HotelReservation, self).unlink()

    def copy(self):
        ctx = dict(self._context) or {}
        ctx.update({"duplicate": True})
        return super(HotelReservation, self.with_context(ctx)).copy()

    @api.constrains("reservation_line_ids", "adults", "children")
    def check_reservation_rooms(self):
        """
        This method is used to validate the reservation_line_ids.
        -----------------------------------------------------
        @param self: object pointer
        @return: raise a warning depending on the validation
        """
        ctx = dict(self._context) or {}
        for reservation in self:
            cap = 0
            for rec in reservation.reservation_line_ids:
                if not rec.reserve:
                    raise ValidationError(
                        _("Seleccione una o mas habitaciones.")
                    )
                cap = sum(room.capacity for room in rec.reserve)
            if not ctx.get("duplicate"):
                if (reservation.adults + reservation.children) > cap:
                    raise ValidationError(
                        _(
                            "La capacidad de la habitacion a sido superada!\n"
                            " Por favor selecciona otra habitacion o cambia la cantidad de personas"
                            " ."
                        )
                    )
            if reservation.adults <= 0:
                raise ValidationError(
                    _("La cantidad de adultos o hijos debe de ser un numero entero positivo.")
                )

    @api.constrains("checkin", "checkout")
    def check_in_out_dates(self):
        """
        When date_order is less then check-in date or
        Checkout date should be greater than the check-in date.
        """
        if self.checkout and self.checkin:
            # if self.checkin < self.date_order:
            #     raise ValidationError(
            #         _(
            #             "Check-in date should be greater than \
            #                              the current date."
            #         )
            #     )
            if self.checkout < self.checkin:
                raise ValidationError(
                    _(
                        "Check-out date should be greater \
                                         than Check-in date."
                    )
                )

    @api.onchange("partner_id")
    def _onchange_partner_id(self):
        """
        When you change partner_id it will update the partner_invoice_id,
        partner_shipping_id and pricelist_id of the hotel reservation as well
        ---------------------------------------------------------------------
        @param self: object pointer
        """
        if not self.partner_id:
            self.update(
                {
                    "partner_invoice_id": False,
                    "partner_shipping_id": False,
                    "partner_order_id": False,
                }
            )
        else:
            addr = self.partner_id.address_get(
                ["delivery", "invoice", "contact"]
            )
            self.update(
                {
                    "partner_invoice_id": addr["invoice"],
                    "partner_shipping_id": addr["delivery"],
                    "partner_order_id": addr["contact"],
                    "pricelist_id": self.partner_id.property_product_pricelist.id,
                }
            )

    @api.onchange("checkin", "checkout")
    def modificar_fechas(self):  # Erick, Este codigo verifica las habitaciones y le cambia la fecha a reservar
        room_res_line_obj = self.env["hotel.room.reservation.line"]
        reserv_checkin = self.checkin
        reserv_checkout = self.checkout
        if not self.state == "draft" or not self.state == "waiting_room":
            # print("no es borrador")
            room_reservation_line = room_res_line_obj.search(
                [("reservation_id", "in", self.ids)]
            )
            for x in room_reservation_line:
                room_reservation_lines = room_res_line_obj.search(
                    [
                        ("room_id", "=", x.room_id.id),
                        ("status", "=", "confirm"),
                    ]
                )
                for t in room_reservation_lines:
                    # print("Habitacion con Reservas en ese horario")
                    # print(t.room_id)
                    # print(t.room_id.name)
                    # print(t.check_in)
                    # print(t.check_out)
                    # print(t.reservation_id.reservation_no)
                    reservtype = 0
                    if not t.reservation_id.reservation_no == x.reservation_id.reservation_no:
                        # print("No es igual al procesado")
                        room_bool = False
                        if t.check_in <= reserv_checkin <= t.check_out:
                            reservtype = 1
                            room_bool = True
                        if t.check_in <= reserv_checkout <= t.check_out:
                            reservtype = 2
                            room_bool = True
                        if (
                                reserv_checkin <= t.check_in and reserv_checkout >= t.check_out
                        ):
                            reservtype = 3
                            room_bool = True
                        timezonegt = t.check_out - timedelta(hours=6)  # erick
                        pharse = "entrada o salida"
                        if (reservtype == 1):
                            timezonegt = t.check_out - timedelta(hours=6)  # erick
                            pharse = "salida"
                        if (reservtype == 2):
                            pharse = "entrada"
                            timezonegt = t.check_in - timedelta(hours=6)  # erick
                        if room_bool:
                            raise ValidationError(
                                _(
                                    "No se puede reservar la habitacion0, verifique que el dia y la hora no esten reservadas por otra persona. \n\nLa %s de la otra reservacion en conflicto con la que desea confirmar es esta: %s"

                                )
                                % (pharse, timezonegt.strftime("%d-%b %H:%M:%S"))
                                # Erick hay que ponerle parentesis cuando es mas de un parametro
                            )
                            return
                # if reserv_checkin > reserv_checkout:
                #     raise ValidationError(
                #         _(
                #             "La fecha y hora del checkin %s, no puede ser mayor a la fecha y hora del checkout, seleccione denuevo otra fecha y hora menor a la checkout."
                #
                #         )
                #         % (reserv_checkin.strftime("%d-%b %H:%M:%S"))
                #         # Erick hay que ponerle parentesis cuando es mas de un parametro
                #     )
                #     return
                # print("guardar horarios")
                x.check_in = reserv_checkin
                x.check_out = reserv_checkout
                # if len(room_reservation_lines) is 1:
                #     print("Solo hay un valor en la lista y se va a cambiar")
                #     print(x.room_id.name)
                #     x.check_in = reserv_checkin
                #     x.check_out = reserv_checkout
                #     reservtype = 0
                #     check_in = t.check_in
                #     check_out = t.check_out
                #     if check_in <= reserv_checkin <= check_out:
                #         reservtype = 1
                #         room_bool = True
                #     if check_in <= reserv_checkout <= check_out:
                #         reservtype = 2
                #         room_bool = True
                #     if (
                #             reserv_checkin <= check_in
                #             and reserv_checkout >= check_out
                #     ):
                #         reservtype = 3
                #         room_bool = True
                #     r_checkin = (t.checkin).date()
                #     r_checkout = (t.checkout).date()
                #     check_intm = (x.check_in).date()
                #     check_outtm = (x.check_out).date()
                #     range1 = [r_checkin, r_checkout]
                #     range2 = [check_intm, check_outtm]
                #     overlap_dates = self.check_overlap(
                #         *range1
                #     ) & self.check_overlap(*range2)
                #     timezonegt = t.check_out - timedelta(hours=6)  # erick
                #     pharse = "entrada o salida"
                #     if (reservtype == 1):
                #         timezonegt = t.check_out - timedelta(hours=6)  # erick
                #         pharse = "salida"
                #     if (reservtype == 2):
                #         pharse = "entrada"
                #         timezonegt = t.check_in - timedelta(hours=6)  # erick
                #     if room_bool:
                #         raise ValidationError(
                #             _(
                #                 "No se puede reservar la habitacion, verifique que el dia y la hora no esten reservadas por otra persona. \n\nLa %s de la otra reservacion en conflicto con la que desea confirmar es esta: %s"
                #
                #             )
                #             % (pharse, timezonegt.strftime("%d-%b %H:%M:%S"))
                #             # Eerick hay que ponerle parentesis cuando es mas de un parametro
                #         )
        return

    @api.model
    def create(self, vals):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        """
        vals["reservation_no"] = (
                self.env["ir.sequence"].next_by_code("hotel.reservation") or "New"
        )
        return super(HotelReservation, self).create(vals)

    def check_overlap(self, date1, date2):
        delta = date2 - date1
        return {date1 + timedelta(days=i) for i in range(delta.days + 1)}

    def confirm_reservation(self):
        """
        This method create a new record set for hotel room reservation line
        -------------------------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel room reservation line.
        """
        if self.espera_de_ocupacion and self.state == "draft":
            return
        reservation_line_obj = self.env["hotel.room.reservation.line"]
        vals = {}
        for reservation in self:
            reserv_checkin = reservation.checkin
            reserv_checkout = reservation.checkout
            room_bool = False
            for line_id in reservation.reservation_line_ids:
                for room in line_id.reserve:
                    # ********************************************* ERICK ************************************
                    habitacion = self.env["hotel.room"].search([('id', '=', room.id)])
                    # print(str(room.name) + str(room.id))
                    if habitacion.maintenance or habitacion.repair:
                        if habitacion.repair_maintenance_to_date >= reserv_checkin >= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                        elif habitacion.repair_maintenance_to_date >= reserv_checkout >= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                        elif habitacion.repair_maintenance_to_date <= reserv_checkout and reserv_checkin <= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                    # raise ValidationError(f'La habitacion no esta bloqueada!')

                    # ********************************************* ERICK ************************************
                    if room.room_reservation_line_ids:
                        for reserv in room.room_reservation_line_ids.search(
                                [
                                    ("status", "in", ("confirm", "done")),
                                    ("room_id", "=", room.id),
                                ]
                        ):
                            reservtype = 0
                            check_in = reserv.check_in
                            check_out = reserv.check_out
                            if check_in <= reserv_checkin <= check_out:
                                reservtype = 1
                                room_bool = True
                            if check_in <= reserv_checkout <= check_out:
                                reservtype = 2
                                room_bool = True
                            if (
                                    reserv_checkin <= check_in
                                    and reserv_checkout >= check_out
                            ):
                                reservtype = 3
                                room_bool = True
                            r_checkin = (reservation.checkin).date()
                            r_checkout = (reservation.checkout).date()
                            check_intm = (reserv.check_in).date()
                            check_outtm = (reserv.check_out).date()
                            range1 = [r_checkin, r_checkout]
                            range2 = [check_intm, check_outtm]
                            overlap_dates = self.check_overlap(
                                *range1
                            ) & self.check_overlap(*range2)
                            timezonegt = reserv.check_out - timedelta(hours=6)  # erick
                            pharse = "entrada o salida"
                            if (reservtype == 1):
                                timezonegt = reserv.check_out - timedelta(hours=6)  # erick
                                pharse = "salida"
                            if (reservtype == 2):
                                pharse = "entrada"
                                timezonegt = reserv.check_in - timedelta(hours=6)  # erick
                            if room_bool:
                                raise ValidationError(
                                    _(
                                        "No se puede reservar la habitacion, verifique que el dia y la hora no esten reservadas por otra persona. \n\nLa %s de la otra reservacion en conflicto con la que desea confirmar es esta: %s"

                                    )
                                    % (pharse, timezonegt.strftime("%d-%b %H:%M:%S"))
                                    # Eerick hay que ponerle parentesis cuando es mas de un parametro
                                )
                            else:
                                if self.state == "draft" or self.state == "waiting_room":
                                    self.state = "confirm"
                                vals = {
                                    "room_id": room.id,
                                    "check_in": reservation.checkin,
                                    "check_out": reservation.checkout,
                                    "state": "assigned",
                                    "reservation_id": reservation.id,
                                }
                                room.write(
                                    {"isroom": False, "status": "occupied"}
                                )
                        else:
                            if self.state == "draft" or self.state == "waiting_room":
                                self.state = "confirm"
                            vals = {
                                "room_id": room.id,
                                "check_in": reservation.checkin,
                                "check_out": reservation.checkout,
                                "state": "assigned",
                                "reservation_id": reservation.id,
                            }
                            room.write({"isroom": False, "status": "occupied"})
                    else:
                        if self.state == "draft" or self.state == "waiting_room":
                            self.state = "confirm"
                        vals = {
                            "room_id": room.id,
                            "check_in": reservation.checkin,
                            "check_out": reservation.checkout,
                            "state": "assigned",
                            "reservation_id": reservation.id,
                        }
                        room.write({"isroom": False, "status": "occupied"})
                    reservation_line_obj.create(vals)
        return True

    @api.onchange("reservation_no")
    def set_reservation_dates(self):  # Erick-> Setea las fechas automaticamente
        self.checkin = datetime(fields.datetime.today().year, fields.datetime.today().month,
                                fields.datetime.today().day) + timedelta(hours=21)
        self.checkout = datetime(fields.datetime.today().year, fields.datetime.today().month,
                                 fields.datetime.today().day) + timedelta(hours=43)

    @api.onchange("checkout", "reservation_line_ids")
    def read_invoice_state(self):  # Erick-> Verifica si la reserva tiene una factura y evita que la modifiquen
        # print(self._origin.id
        # print(self.id)
        # print(self._origin.checkout)
        # print(self.checkout)
        if self._origin.no_of_folio > 0:
            folios = self.env["hotel.folio"].search([("reservation_id", "=", self._origin.id)])
            for f in folios:
                if f.invoice_status != "no":
                    self.checkout = self._origin.checkout
                    self.reservation_line_ids = self._origin.reservation_line_ids
                    # self.invoiced = True
                    self._origin.write({"invoiced": True})
                    self.write({"invoiced": True})
                    # raise ValidationError(f'Folio: ' + str(f.name) +" no se puede modificar porque ya tiene una factura asociada.")

    def reconfirm_reservation(self):  # Erick> ese codigo es para reconfirmacion de una reserva
        """
        Este metodo actualiza los datos de la reservacion, agrega o quita habitaciones y cambia fechas de reservacion.
        -------------------------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel room reservation line.
        """
        # print(self)
        if self.espera_de_ocupacion and self.state == "draft":
            return
        if self.no_of_folio > 0:
            folio = self.env["hotel.folio"].search([("reservation_id", "=", self.id)])
            # print(folio.name)
            if folio.invoice_status in ["invoiced", "upselling", "to invoice"]:
                raise ValidationError(
                    f'Folio:' + str(folio.name) + " no se puede modificar porque ya tiene una factura asociada.")
        reservation_line_obj = self.env["hotel.room.reservation.line"]
        vals = {}
        for reservation in self:
            reserv_checkin = reservation.checkin
            reserv_checkout = reservation.checkout
            for line_id in reservation.reservation_line_ids:
                for room in line_id.reserve:
                    # ********************************************* ERICK ************************************
                    habitacion = self.env["hotel.room"].search([('id', '=', room.id)])
                    # print(str(room.name) + str(room.id))
                    if habitacion.maintenance or habitacion.repair:
                        if habitacion.repair_maintenance_to_date >= reserv_checkin >= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                        elif habitacion.repair_maintenance_to_date >= reserv_checkout >= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                        elif habitacion.repair_maintenance_to_date <= reserv_checkout and reserv_checkin <= habitacion.repair_maintenance_from_date:
                            raise ValidationError(
                                f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                                    habitacion.repair_maintenance_from_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                                    habitacion.repair_maintenance_to_date.strftime(
                                        '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                    # raise ValidationError(f'La habitacion no esta bloqueada!')

                    # ********************************************* ERICK ************************************
                    eselmismo = False
                    if room.room_reservation_line_ids:
                        room_bool = False
                        # se modifico la consulta porque retornaba la misma reserva entonces siempre se encontraba ella misma.
                        for reserv in room.room_reservation_line_ids.search(
                                [
                                    "&",
                                    "&",
                                    ("status", "in", ("confirm", "done")),
                                    ("room_id", "=", room.id),
                                    ("reservation_id", "!=", self.id),
                                ]
                        ):
                            # print(room.name)
                            reservtype = 0
                            check_in = reserv.check_in
                            check_out = reserv.check_out
                            if check_in <= reserv_checkin <= check_out:
                                reservtype = 1
                                room_bool = True
                            if check_in <= reserv_checkout <= check_out:
                                reservtype = 2
                                room_bool = True
                            if (
                                    reserv_checkin <= check_in
                                    and reserv_checkout >= check_out
                            ):
                                reservtype = 3
                                room_bool = True
                            r_checkin = (reservation.checkin).date()
                            r_checkout = (reservation.checkout).date()
                            check_intm = (reserv.check_in).date()
                            check_outtm = (reserv.check_out).date()
                            range1 = [r_checkin, r_checkout]
                            range2 = [check_intm, check_outtm]
                            overlap_dates = self.check_overlap(
                                *range1
                            ) & self.check_overlap(*range2)
                            timezonegt = reserv.check_out - timedelta(hours=6)  # erick
                            pharse = "entrada o salida"
                            if (reservtype == 1):
                                timezonegt = reserv.check_out - timedelta(hours=6)  # erick
                                pharse = "salida"
                            if (reservtype == 2):
                                pharse = "entrada"
                                timezonegt = reserv.check_in - timedelta(hours=6)  # erick
                            # print(room_bool)
                            # print(reservtype)
                            if room_bool and not reservation.reservation_no == reserv.reservation_id.reservation_no:
                                # print("ACA ESTA EL ERROR")
                                raise ValidationError(
                                    _(
                                        "No se puede reservar la habitacion, verifique que el dia y la hora no esten reservadas por otra persona. \n\nLa %s de la otra reservacion en conflicto con la que desea confirmar es esta: %s"

                                    )
                                    % (pharse, timezonegt.strftime("%d-%b %H:%M:%S"))
                                    # Eerick hay que ponerle parentesis cuando es mas de un parametro
                                )
                            else:
                                reserv = room.room_reservation_line_ids.search(
                                    [
                                        "&",
                                        "&",
                                        ("status", "in", ("confirm", "done")),
                                        ("room_id", "=", room.id),
                                        ("reservation_id", "=", self.id),
                                    ]
                                )
                                if not reservation.reservation_no == reserv.reservation_id.reservation_no:
                                    # print("Se ejecuto 1")
                                    if self.state == "draft" or self.state == "waiting_room":
                                        self.state = "confirm"
                                    vals = {
                                        "room_id": room.id,
                                        "check_in": reservation.checkin,
                                        "check_out": reservation.checkout,
                                        "state": "assigned",
                                        "reservation_id": reservation.id,
                                    }
                                    room.write(
                                        {"isroom": False, "status": "occupied"}
                                    )
                        else:
                            reserv = room.room_reservation_line_ids.search(
                                [
                                    "&",
                                    "&",
                                    ("status", "in", ("confirm", "done")),
                                    ("room_id", "=", room.id),
                                    ("reservation_id", "=", self.id),
                                ]
                            )
                            eselmismo = True
                            if not reservation.reservation_no == reserv.reservation_id.reservation_no:
                                eselmismo = False
                                # print("Se ejecuto 2")
                                if self.state == "draft" or self.state == "waiting_room":
                                    self.state = "confirm"
                                vals = {
                                    "room_id": room.id,
                                    "check_in": reservation.checkin,
                                    "check_out": reservation.checkout,
                                    "state": "assigned",
                                    "reservation_id": reservation.id,
                                }
                                room.write({"isroom": False, "status": "occupied"})
                    else:
                        # print("Se ejecuto 3")
                        if self.state == "draft" or self.state == "waiting_room":
                            self.state = "confirm"
                        vals = {
                            "room_id": room.id,
                            "check_in": reservation.checkin,
                            "check_out": reservation.checkout,
                            "state": "assigned",
                            "reservation_id": reservation.id,
                        }
                        room.write({"isroom": False, "status": "occupied"})
                    if not eselmismo:
                        reservation_line_obj.create(vals)

            if reservation.no_of_folio > 0:
                folio = self.env["hotel.folio"].search([("reservation_id", "=", reservation.id)])
                folio_lines = []
                checkin_date = reservation["checkin"]
                checkout_date = reservation["checkout"]
                duration_vals = self._onchange_check_dates(
                    checkin_date=checkin_date,
                    checkout_date=checkout_date,
                    duration=False,
                )
                duration = duration_vals.get("duration") or 0.0
                for line in reservation.reservation_line_ids:
                    for r in line.reserve:
                        folio_lines.append(
                            (
                                0,
                                0,
                                {
                                    "checkin_date": checkin_date,
                                    "checkout_date": checkout_date,
                                    "product_id": r.product_id and r.product_id.id,
                                    "name": reservation["reservation_no"],
                                    "price_unit": r.list_price,
                                    "product_uom_qty": duration,
                                    "is_reserved": True,
                                },
                            )
                        )
                        r.write({"status": "occupied", "isroom": False})
                for room in folio.room_line_ids:  # Erick-> Se eliminan todas las habitacioens colocadas antes de poner las nuevas
                    room.unlink()
                folio.write({"checkout_date": checkout_date, "room_line_ids": folio_lines})
                for rm_line in folio.room_line_ids:
                    rm_line.product_id_change() # Erick> Actualiza los producto y habitacones en el folio
                # raise ValidationError(f'folio:!' + str(folio.name))
        self.write_message_post()
        return True



    # def write(self, vals):
    #     res = super(HotelReservation, self).write(vals)
    #     self.write_message_post()
    #     return res

    def write_message_post(self):
        body_habitaciones = emojize(":key:") + "Habitaciones de Reservacion: "
        encontrado = False
        for x in self.reservation_line_ids:
            for p in x.reserve:
                encontrado = True
                num_habitacion = []
                if p.name:
                    num_habitacion = p.name.split(" ")
                    if num_habitacion[0].upper() == "HOSPEDAJE":
                        body_habitaciones = body_habitaciones + " ||  " + str(num_habitacion[1]) + " " + str(num_habitacion[2])
                    else:
                        body_habitaciones = body_habitaciones + " ||  " + str(p.name)
        if encontrado:
            self.message_post(body=body_habitaciones)
        return True

    def cancel_reservation(self):
        """
        This method cancel record set for hotel room reservation line
        ------------------------------------------------------------------
        @param self: The object pointer
        @return: cancel record set for hotel room reservation line.
        """
        room_res_line_obj = self.env["hotel.room.reservation.line"]
        hotel_res_line_obj = self.env["hotel_reservation.line"]
        self.state = "cancel"
        room_reservation_line = room_res_line_obj.search(
            [("reservation_id", "in", self.ids)]
        )
        room_reservation_line.write({"state": "unassigned"})
        room_reservation_line.unlink()
        reservation_lines = hotel_res_line_obj.search(
            [("line_id", "in", self.ids)]
        )
        for reservation_line in reservation_lines:
            reservation_line.reserve.write(
                {"isroom": True, "status": "available"}
            )
        return True

    def set_to_draft_reservation(self):
        self.update({"state": "draft"})

    def action_send_reservation_mail(self):
        """
        This function opens a window to compose an email,
        template message loaded by default.
        @param self: object pointer
        """
        self.ensure_one(), "This is for a single id at a time."
        template_id = self.env.ref(
            "hotel_reservation.email_template_hotel_reservation"
        ).id
        compose_form_id = self.env.ref(
            "mail.email_compose_message_wizard_form"
        ).id
        ctx = {
            "default_model": "hotel.reservation",
            "default_res_id": self.id,
            "default_use_template": bool(template_id),
            "default_template_id": template_id,
            "default_composition_mode": "comment",
            "force_send": True,
            "mark_so_as_sent": True,
        }
        return {
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "mail.compose.message",
            "views": [(compose_form_id, "form")],
            "view_id": compose_form_id,
            "target": "new",
            "context": ctx,
            "force_send": True,
        }

    @api.model
    def _reservation_reminder_24hrs(self):
        """
        This method is for scheduler
        every 1day scheduler will call this method to
        find all tomorrow's reservations.
        ----------------------------------------------
        @param self: The object pointer
        @return: send a mail
        """
        now_date = fields.Date.today()
        template_id = self.env.ref(
            "hotel_reservation.mail_template_reservation_reminder_24hrs"
        )
        for reserv_rec in self:
            checkin_date = reserv_rec.checkin
            difference = relativedelta(now_date, checkin_date)
            if (
                    difference.days == -1
                    and reserv_rec.partner_id.email
                    and reserv_rec.state == "confirm"
            ):
                template_id.send_mail(reserv_rec.id, force_send=True)
        return True

    def create_folio(self):
        """
        This method is for create new hotel folio.
        -----------------------------------------
        @param self: The object pointer
        @return: new record set for hotel folio.
        """
        if self.inguat_cards == "0":
            raise ValidationError(
                _(
                    "Error, Ingrese las boletas de INGUAT.")
            )
        cards = self.inguat_cards.replace(" ", "").split(",")
        num_hab = 0
        for x in self.reservation_line_ids:
            for val in x.reserve:
                num_hab = num_hab + 1
        if len(cards) == 0:
            raise ValidationError(
                    _(
                        "Error, no hay numeros de boletas de INGUAT, por favor ingrese "
                    + str(num_hab))
                )
        else:
            if len(cards) != num_hab:
                raise ValidationError(
                        _(
                            "La cantidad de boletas ingresadas no es igual a la cantidad de habitaciones de la reserva! deben de ser "
                        + str(num_hab)+ " boletas de INGUAT ingresadas.")
                    )
        hotel_folio_obj = self.env["hotel.folio"]
        folio = False
        for reservation in self:
            folio_lines = []
            checkin_date = reservation["checkin"]
            checkout_date = reservation["checkout"]
            duration_vals = self._onchange_check_dates(
                checkin_date=checkin_date,
                checkout_date=checkout_date,
                duration=False,
            )
            duration = duration_vals.get("duration") or 0.0
            folio_vals = {
                "date_order": reservation.date_order,
                "warehouse_id": reservation.warehouse_id.id,
                "partner_id": reservation.partner_id.id,
                "pricelist_id": reservation.pricelist_id.id,
                "partner_invoice_id": reservation.partner_invoice_id.id,
                "partner_shipping_id": reservation.partner_shipping_id.id,
                "checkin_date": reservation.checkin,
                "checkout_date": reservation.checkout,
                "duration": duration,
                "reservation_id": reservation.id,
                "inguat_cards": reservation.inguat_cards,  # erick
                "credit_card_date": reservation.credit_card_date,  # erick
                "credit_card_number": reservation.credit_card_number,  # erick
            }
            for line in reservation.reservation_line_ids:
                for r in line.reserve:
                    folio_lines.append(
                        (
                            0,
                            0,
                            {
                                "checkin_date": checkin_date,
                                "checkout_date": checkout_date,
                                "product_id": r.product_id and r.product_id.id,
                                "name": reservation["reservation_no"],
                                "price_unit": r.list_price,
                                "product_uom_qty": duration,
                                "is_reserved": True,
                            },
                        )
                    )
                    r.write({"status": "occupied", "isroom": False})
            folio_vals.update({"room_line_ids": folio_lines})
            folio = hotel_folio_obj.create(folio_vals)
            for rm_line in folio.room_line_ids:
                rm_line.product_id_change()
            self.write({"folios_ids": [(6, 0, folio.ids)], "state": "done"})
        sale_obj = self.env["sale.order"].search([("id","=",folio.order_id.id)]) #Erick se pasan los numeros de inguat a ventas.
        sale_obj.inguat_cards = self.inguat_cards
        return True

    def _onchange_check_dates(
            self, checkin_date=False, checkout_date=False, duration=False
    ):
        """
        This method gives the duration between check in checkout if
        customer will leave only for some hour it would be considers
        as a whole day. If customer will checkin checkout for more or equal
        hours, which configured in company as additional hours than it would
        be consider as full days
        --------------------------------------------------------------------
        @param self: object pointer
        @return: Duration and checkout_date
        """
        value = {}
        configured_addition_hours = (
            self.warehouse_id.company_id.additional_hours
        )
        duration = 0
        if checkin_date and checkout_date:
            dur = checkout_date - checkin_date
            duration = dur.days + 1
            if configured_addition_hours > 0:
                additional_hours = abs(dur.seconds / 60)
                if additional_hours <= abs(configured_addition_hours * 60):
                    duration -= 1
        value.update({"duration": duration})
        return value

    def open_folio_view(self):
        folios = self.mapped("folios_ids")
        action = self.env.ref("hotel.open_hotel_folio1_form_tree_all").read()[
            0
        ]
        if len(folios) > 1:
            action["domain"] = [("id", "in", folios.ids)]
        elif len(folios) == 1:
            action["views"] = [
                (self.env.ref("hotel.view_hotel_folio_form").id, "form")
            ]
            action["res_id"] = folios.id
        else:
            action = {"type": "ir.actions.act_window_close"}
        return action


class HotelReservationLine(models.Model):
    _name = "hotel_reservation.line"
    _description = "Reservation Line"

    name = fields.Char("Name")
    line_id = fields.Many2one("hotel.reservation")
    reserve = fields.Many2many(
        "hotel.room",
        "hotel_reservation_line_room_rel",
        "hotel_reservation_line_id",
        "room_id",
        domain="[('isroom','=',True),\
                               ('categ_id','=',categ_id)]",
    )
    categ_id = fields.Many2one("hotel.room.type", "Room Type")

    @api.onchange("categ_id") # erick_context
    def _onchange_categ(self):
        """
        When you change categ_id it check checkin and checkout are
        filled or not if not then raise warning
        -----------------------------------------------------------
        @param self: object pointer
        """
        # print(self._context['espera_de_ocupacion'])
        espera_de_ocupacion = self._context['espera_de_ocupacion']
        if not self.line_id.checkin:
            raise ValidationError(
                _(
                    "Before choosing a room,\n You have to \
                                     select a Check in date or a Check out \
                                     date in the reservation form."
                )
            )
        hotel_room_ids = self.env["hotel.room"].search(
            [("room_categ_id", "=", self.categ_id.id)]
        )
        room_ids = []
        if espera_de_ocupacion:
            for room in hotel_room_ids:
                room_ids.append(room.id)
        else:
            for room in hotel_room_ids:
                assigned = False
                for line in room.room_reservation_line_ids:
                    if line.status != "cancel":
                        if (
                                self.line_id.checkin
                                <= line.check_in
                                <= self.line_id.checkout
                        ) or (
                                self.line_id.checkin
                                <= line.check_out
                                <= self.line_id.checkout
                        ):
                            assigned = True
                        elif (
                                line.check_in <= self.line_id.checkin <= line.check_out
                        ) or (
                                line.check_in
                                <= self.line_id.checkout
                                <= line.check_out
                        ):
                            assigned = True
                for rm_line in room.room_line_ids:
                    if rm_line.status != "cancel":
                        if (
                                self.line_id.checkin
                                <= rm_line.check_in
                                <= self.line_id.checkout
                        ) or (
                                self.line_id.checkin
                                <= rm_line.check_out
                                <= self.line_id.checkout
                        ):
                            assigned = True
                        elif (
                                rm_line.check_in
                                <= self.line_id.checkin
                                <= rm_line.check_out
                        ) or (
                                rm_line.check_in
                                <= self.line_id.checkout
                                <= rm_line.check_out
                        ):
                            assigned = True
                if not assigned:
                    room_ids.append(room.id)
        domain = {"reserve": [("id", "in", room_ids)]}
        return {"domain": domain}

    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        hotel_room_reserv_line_obj = self.env["hotel.room.reservation.line"]
        for reserv_rec in self:
            for rec in reserv_rec.reserve:
                lines = hotel_room_reserv_line_obj.search(
                    [
                        ("room_id", "=", rec.id),
                        ("reservation_id", "=", reserv_rec.line_id.id),
                    ]
                )
                if lines:
                    rec.write({"isroom": True, "status": "available"})
                    lines.unlink()
        return super(HotelReservationLine, self).unlink()


class HotelRoomReservationLine(models.Model):
    _name = "hotel.room.reservation.line"
    _description = "Hotel Room Reservation"
    _rec_name = "room_id"

    room_id = fields.Many2one("hotel.room", string="Room id")
    check_in = fields.Datetime("Check In Date", required=True)
    check_out = fields.Datetime("Check Out Date", required=True)
    state = fields.Selection(
        [("assigned", "Assigned"), ("unassigned", "Unassigned")], "Room Status"
    )
    reservation_id = fields.Many2one("hotel.reservation", "Reservation")
    status = fields.Selection(string="state", related="reservation_id.state")
