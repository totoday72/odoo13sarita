# See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class QuickRoomReservationView(models.TransientModel):
    _name = "quick.room.reservation.view"
    _description = "Quick Room Reservation"
    room_id = fields.Many2one("hotel.room", "Room")
    reservation_id = fields.Char("Doc")


    @api.model
    def default_get(self, fields):
        val = self._context.get("reservation_id", True)
        print(val)
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        res = super(QuickRoomReservationView, self).default_get(fields)
        print(
            self._context["reservation_id"]
        )
        keys = self._context.keys()
        if "reservation_id" in keys:
            res.update({"reservation_id": self._context["reservation_id"]})
            # res.update({"check_out": self._context["date1"]}) #Erick
        if "room_id" in keys:
            roomid = self._context["room_id"]
            res.update({"room_id": int(roomid)})
        return res


    def view_reservation(self):  # erick
        print("entro en el metodo")
        rec = False
        hotel_res_obj = self.env["hotel.reservation"].search([])  # erick
        for res in hotel_res_obj:
            print(res.reservation_no)
        reservation_line_obj = self.env["hotel.room.reservation.line"]
        for res in reservation_line_obj:
            print(res.reservation_no)
        return
