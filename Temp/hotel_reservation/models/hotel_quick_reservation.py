# See LICENSE file for full copyright and licensing details.
from datetime import datetime, timedelta

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class QuickRoomReservation(models.TransientModel):
    _name = "quick.room.reservation"
    _description = "Quick Room Reservation"

    partner_id = fields.Many2one("res.partner", "Customer", required=True)
    check_in = fields.Datetime("Check In", required=True)
    check_out = fields.Datetime("Check Out", required=True)
    room_id = fields.Many2one("hotel.room", "Room", required=True)
    warehouse_id = fields.Many2one("stock.warehouse", "Hotel", required=True)
    pricelist_id = fields.Many2one("product.pricelist", "pricelist")
    partner_invoice_id = fields.Many2one(
        "res.partner", "Invoice Address", required=True
    )
    partner_order_id = fields.Many2one(
        "res.partner", "Ordering Contact", required=True
    )
    partner_shipping_id = fields.Many2one(
        "res.partner", "Delivery Address", required=True
    )
    adults = fields.Integer("Adults")
    children = fields.Integer("Niños")
    observations = fields.Char("Notas y Observaciones")
    operator = fields.Many2one(
        "res.users", "Operador", default=lambda self: self.env.user
    )
    inguat_cards = fields.Char("Boletas de Inguat", default="0",help="Ingrese los numeros separados por , (coma) sin espacios.")
    credit_card_date = fields.Char("Fecha de Vencimiento: ", )
    credit_card_number = fields.Char("Número de Tarjeta: ")
    espera_de_ocupacion = fields.Boolean("En espera de Habitación: ", default=False)
    @api.onchange("inguat_cards")
    def check_inguat_cards(self):
        if self.inguat_cards and self.inguat_cards != "0":
            cards = self.inguat_cards.replace(" ", "").split(",")
            if len(cards) == 0:
                raise ValidationError(
                    _(
                        "Error, no hay numeros de boletas de inguat!"
                    )
                )
            else:
                if len(cards) > 1:
                    raise ValidationError(
                        _(
                            "La cantidad de boletas ingresadas no puede ser mayor a una boleta para esta habitacion!"
                        )
                    )
        return


    @api.onchange("check_out", "check_in")
    def on_change_check_out(self):
        """
        When you change checkout or checkin it will check whether
        Checkout date should be greater than Checkin date
        and update dummy field
        -----------------------------------------------------------
        @param self: object pointer
        @return: raise warning depending on the validation
        """
        if self.check_out and self.check_in:
            if self.partner_id:
                if self.check_out < self.check_in:
                    raise ValidationError(
                        _(
                            "La salida debe ser una fecha despues de \
                                             la fecha de entrada."
                        )
                    )

    @api.onchange("partner_id")
    def _onchange_partner_id_res(self):
        """
        When you change partner_id it will update the partner_invoice_id,
        partner_shipping_id and pricelist_id of the hotel reservation as well
        ---------------------------------------------------------------------
        @param self: object pointer
        """
        if not self.partner_id:
            self.update(
                {
                    "partner_invoice_id": False,
                    "partner_shipping_id": False,
                    "partner_order_id": False,
                }
            )
        else:
            addr = self.partner_id.address_get(
                ["delivery", "invoice", "contact"]
            )
            self.update(
                {
                    "partner_invoice_id": addr["invoice"],
                    "partner_shipping_id": addr["delivery"],
                    "partner_order_id": addr["contact"],
                    "pricelist_id": self.partner_id.property_product_pricelist.id,
                }
            )

    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        warehouse = False
        warehouses = self.env["stock.warehouse"].search([('company_id', '=', self.env.company.id)])
        if len(warehouses) > 0:
            warehouse = warehouses[0].id
        res = super(QuickRoomReservation, self).default_get(fields)
        keys = self._context.keys()
        if "date" in keys:
            fecha = datetime.strptime(self._context["date"], '%Y-%m-%d %H:%M:%S')
            fecha1 = datetime.strptime(self._context["date1"], '%Y-%m-%d %H:%M:%S')
            # print(fecha.strftime("%Y-%B-%d %H:%M:%S"))
            # print(fecha1.strftime("%Y-%B-%d %H:%M:%S"))
            if fecha.strftime("%H:%M:%S") == "08:58:01" and fecha1.strftime("%H:%M:%S") == "06:58:01":
                # print("entro")
                fecha = fecha - timedelta(hours=11, minutes=58, seconds=1)
                fecha1 = fecha + timedelta(hours=22, minutes=0, seconds=0)
                res.update({"check_in": fecha.strftime("%Y-%m-%d %H:%M:%S")})
                res.update({"check_out": fecha1.strftime("%Y-%m-%d %H:%M:%S")})  # Erick
            else:
                res.update({"check_in": self._context["date"]})
                res.update({"check_out": self._context["date1"]})  # Erick
        if "room_id" in keys:
            roomid = self._context["room_id"]
            res.update({"room_id": int(roomid)})
            res.update({"warehouse_id": warehouse})
            res.update({"adults": 2})
        return res

    def room_reserve(self):
        """
        This method create a new record for hotel.reservation
        -----------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel reservation.
        """
        estado = "draft"
        if self.espera_de_ocupacion:
            estado = "waiting_room"

        hotel_res_obj = self.env["hotel.reservation"]
        for res in self:
            habitacion = self.env["hotel.room"].search([('id', '=', res.room_id.id)])
            if habitacion.maintenance or habitacion.repair:
                if habitacion.repair_maintenance_to_date >= res.check_in >= habitacion.repair_maintenance_from_date:
                    raise ValidationError(
                        f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                            habitacion.repair_maintenance_from_date.strftime('%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                            habitacion.repair_maintenance_to_date.strftime(
                                '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                elif habitacion.repair_maintenance_to_date >= res.check_out >= habitacion.repair_maintenance_from_date:
                    raise ValidationError(
                        f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                            habitacion.repair_maintenance_from_date.strftime('%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                            habitacion.repair_maintenance_to_date.strftime(
                                '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
                elif habitacion.repair_maintenance_to_date <= res.check_out and res.check_in <= habitacion.repair_maintenance_from_date:
                    raise ValidationError(
                        f'La habitacion:' + habitacion.name + " No se puede reservar porque esta bloqueada desde " + str(
                            habitacion.repair_maintenance_from_date.strftime('%d/%B/%Y %H:%M:%S')) + " hasta " + str(
                            habitacion.repair_maintenance_to_date.strftime(
                                '%d/%B/%Y %H:%M:%S')) + ", por la siguiente razon: " + habitacion.observations_repair_maintenance)
            # else:
            #     raise ValidationError(f'La habitacion no esta bloqueada!')
            # raise ValidationError(f'DEBUG: Ya paso el bloqueo' + habitacion.name)
            rec = hotel_res_obj.create(
                {
                    "partner_id": res.partner_id.id,
                    "partner_invoice_id": res.partner_invoice_id.id,
                    "partner_order_id": res.partner_order_id.id,
                    "partner_shipping_id": res.partner_shipping_id.id,
                    "checkin": res.check_in,
                    "checkout": res.check_out,
                    "warehouse_id": res.warehouse_id.id,
                    "pricelist_id": res.pricelist_id.id,
                    "adults": res.adults,
                    "children": res.children,
                    "observations": res.observations,  # erick
                    "inguat_cards": res.inguat_cards,  # erick
                    "credit_card_date": res.credit_card_date,  # erick
                    "credit_card_number": res.credit_card_number,  # erick
                    "operator": res.operator.id,  # Erick
                    "espera_de_ocupacion": res.espera_de_ocupacion,
                    "state":  estado,
                    "reservation_line_ids": [
                        (
                            0,
                            0,
                            {
                                "reserve": [(6, 0, [res.room_id.id])],
                                "name": (
                                        res.room_id and res.room_id.name or ""
                                ),
                            },
                        )
                    ],
                }
            )
        return rec

    def confirm_reservation(self):  # erick
        if self.espera_de_ocupacion:
            rec = self.room_reserve()  # erick
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'hotel.reservation',
                'res_id': rec.id,
            }
        else:
            rec = self.room_reserve().confirm_reservation()  # erick
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'room.reservation.summary',
        }
        # return rec

    # return {
    #     'name': _('Return lines'),
    #     'view_type': 'form',
    #     'view_mode': 'form',
    #     'view_id': res and res[1] or False,
    #     'res_model': 'stock.return.picking',
    #     'src_model': "stock.picking",
    #     'type': 'ir.actions.act_window',
    #     'nodestroy': True,
    #     'target': 'current',
    #     'key2': "client_action_multi",
    #     'res_id': picking_id,
    # }

    def view_reservation(self):  # erick
        rec = False
        hotel_res_obj = self.env["hotel.reservation"]  # erick
        # for res in hotel_res_obj:
        #     print(res.reservation_no)
        return rec
