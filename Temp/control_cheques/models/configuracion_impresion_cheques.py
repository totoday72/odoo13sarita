from odoo import models, tools, fields, api, _
from odoo.exceptions import ValidationError


class ControlCheques_impresion(models.Model):
    _name = 'control_cheques.impresion_sarita'
    _description = 'Configuración de Impresión de Cheques'
    _inherit = ["mail.thread", "mail.activity.mixin"]
    name = fields.Char("Nombre")
    user_id = fields.Many2one("res.users", "Usuario", required=True, )
    company_id = fields.Many2one("res.company", "Compañia", required=True, )
    bank_jounal_id = fields.Many2one('account.journal', string='Cuenta Bancaria', domain="[('aplica_cheque','=',True)]")

    tamano_campo = fields.Integer("Tamanio Campo")
    tamano_letra = fields.Integer("Tamanio Letra")
    no_negociable_X = fields.Integer("No negociable X")
    no_negociable_Y = fields.Integer("No negociable Y")
    lugar_fecha_X = fields.Integer("Lugar y Fecha X")
    lugar_fecha_Y = fields.Integer("Lugar y Fecha Y")
    total_q_X = fields.Integer("Total Q X")
    total_q_y = fields.Integer("Total Q Y")
    paguese_a_X = fields.Integer("Paguese A X")
    paguese_a_Y = fields.Integer("Paguese A Y")
    suma_de_X = fields.Integer("Suma de X")
    suma_de_Y = fields.Integer("Suma de Y")

    correlativo_boleta_X = fields.Integer("Correlativo Boleta X")
    correlativo_boleta_Y = fields.Integer("Correlativo Boleta Y")
    linea_partida_1_codigo_X = fields.Integer("Linea partida 1 codigo X")
    linea_partida_1_codigo_Y = fields.Integer("Linea partida 1 codigo Y")
    linea_partida_2_codigo_X = fields.Integer("Linea partida 2 codigo X")
    linea_partida_2_codigo_Y = fields.Integer("Linea partida 2 codigo Y")
    linea_partida_3_codigo_X = fields.Integer("Linea partida 3 codigo X")
    linea_partida_3_codigo_Y = fields.Integer("Linea partida 3 codigo Y")
    linea_partida_4_codigo_X = fields.Integer("Linea partida 4 codigo X")
    linea_partida_4_codigo_Y = fields.Integer("Linea partida 4 codigo Y")
    linea_partida_5_codigo_X = fields.Integer("Linea partida 5 codigo X")
    linea_partida_5_codigo_Y = fields.Integer("Linea partida 5 codigo Y")
    linea_partida_6_codigo_X = fields.Integer("Linea partida 6 codigo X")
    linea_partida_6_codigo_Y = fields.Integer("Linea partida 6 codigo Y")

    linea_partida_1_descripcion_X = fields.Integer("Linea partida 1 descripcion X")
    linea_partida_1_descripcion_Y = fields.Integer("Linea partida 1 descripcion Y")
    linea_partida_2_descripcion_X = fields.Integer("Linea partida 2 descripcion X")
    linea_partida_2_descripcion_Y = fields.Integer("Linea partida 2 descripcion Y")
    linea_partida_3_descripcion_X = fields.Integer("Linea partida 3 descripcion X")
    linea_partida_3_descripcion_Y = fields.Integer("Linea partida 3 descripcion Y")
    linea_partida_4_descripcion_X = fields.Integer("Linea partida 4 descripcion X")
    linea_partida_4_descripcion_Y = fields.Integer("Linea partida 4 descripcion Y")
    linea_partida_5_descripcion_X = fields.Integer("Linea partida 5 descripcion X")
    linea_partida_5_descripcion_Y = fields.Integer("Linea partida 5 descripcion Y")
    linea_partida_6_descripcion_X = fields.Integer("Linea partida 6 descripcion X")
    linea_partida_6_descripcion_Y = fields.Integer("Linea partida 6 descripcion Y")

    linea_partida_1_cargos_X = fields.Integer("Linea partida 1 cargos X")
    linea_partida_1_cargos_Y = fields.Integer("Linea partida 1 cargos Y")
    linea_partida_2_cargos_X = fields.Integer("Linea partida 2 cargos X")
    linea_partida_2_cargos_Y = fields.Integer("Linea partida 2 cargos Y")
    linea_partida_3_cargos_X = fields.Integer("Linea partida 3 cargos X")
    linea_partida_3_cargos_Y = fields.Integer("Linea partida 3 cargos Y")
    linea_partida_4_cargos_X = fields.Integer("Linea partida 4 cargos X")
    linea_partida_4_cargos_Y = fields.Integer("Linea partida 4 cargos Y")
    linea_partida_5_cargos_X = fields.Integer("Linea partida 5 cargos X")
    linea_partida_5_cargos_Y = fields.Integer("Linea partida 5 cargos Y")
    linea_partida_6_cargos_X = fields.Integer("Linea partida 6 cargos X")
    linea_partida_6_cargos_Y = fields.Integer("Linea partida 6 cargos Y")

    linea_partida_1_abonos_X = fields.Integer("Linea partida 1 abonos X")
    linea_partida_1_abonos_Y = fields.Integer("Linea partida 1 abonos Y")
    linea_partida_2_abonos_X = fields.Integer("Linea partida 2 abonos X")
    linea_partida_2_abonos_Y = fields.Integer("Linea partida 2 abonos Y")
    linea_partida_3_abonos_X = fields.Integer("Linea partida 3 abonos X")
    linea_partida_3_abonos_Y = fields.Integer("Linea partida 3 abonos Y")
    linea_partida_4_abonos_X = fields.Integer("Linea partida 4 abonos X")
    linea_partida_4_abonos_Y = fields.Integer("Linea partida 4 abonos Y")
    linea_partida_5_abonos_X = fields.Integer("Linea partida 5 abonos X")
    linea_partida_5_abonos_Y = fields.Integer("Linea partida 5 abonos Y")
    linea_partida_6_abonos_X = fields.Integer("Linea partida 6 abonos X")
    linea_partida_6_abonos_Y = fields.Integer("Linea partida 6 abonos Y")

    revisado_por_X = fields.Integer("Revisado Por X")
    revisado_por_Y = fields.Integer("Revisado Por Y")
    total_cargos_X = fields.Integer("Total cargos X")
    total_cargos_Y = fields.Integer("Total cargos Y")
    total_abonos_X = fields.Integer("Total abonos X")
    total_abonos_Y = fields.Integer("Total abonos Y")

    tipo_letra = fields.Selection(
        [
            ("arial", "Arial"),
            ("arial narrow", "Arial Narrow"),
            ("calibri", "Calibri"),
            ("courier new", "Courier New"),
            ("georgia", "Georgia"),
            ("helvetica", "Helvetica"),
            ("impact", "Impact"),
            ("lucida bright", "Lucida Bright"),
            ("monaco", "Monaco"),
            ("montserrat", "Montserrat"),
            ("tahoma", "Tahoma"),
            ("times", "Times"),
            ("times new roman", "Times New Roman"),
            ("verdana", "Verdana"),
        ],
        "Tipo de letra",
        default="monaco"
    )
    @api.model
    def create(self, vals):
        vals["name"] = (
                self.env["ir.sequence"].next_by_code("control_cheques.impresion_sarita") or "New"
        )
        conf_cheque = self.env["control_cheques.impresion_sarita"].search(
            ['&', ("user_id", "=", vals["user_id"]), '&', ("company_id", "=", vals["company_id"]),
             ("bank_jounal_id", "=", vals["bank_jounal_id"])]
        )
        if len(conf_cheque) > 0:
            raise ValidationError(_("ya existe una configuración igual para el usuario **%s**, la empresa **%s** y la "
                                    "cuenta **%s**, Si desea crear otra plantilla asignela a otro usuario." % (
                                    conf_cheque.user_id.name, conf_cheque.company_id.name,
                                    conf_cheque.bank_jounal_id.name)))
        res = super(ControlCheques_impresion, self).create(vals)
        return res

    def pdf_prueba(self):
        data = {
            "ids": self.ids,
            "model": "control_cheques.impresion_sarita",
        }
        return self.env.ref(
            "control_cheques.report_prueba_cuadricula_cheque"
        ).report_action(self, data=data)

    def cheque_de_prueba(self):
        data = {
            "ids": self.ids,
            "model": "control_cheques.impresion_sarita",
        }
        return self.env.ref(
            "control_cheques.report_cheque_de_pruebas"
        ).report_action(self, data=data)

    def cheque_de_prueba_en_cuadricula(self):
        data = {
            "ids": self.ids,
            "model": "control_cheques.impresion_sarita",
        }
        return self.env.ref(
            "control_cheques.cheque_de_prueba_en_cuadricula"
        ).report_action(self, data=data)

class Report_prueba_pdf_cheques(models.AbstractModel):
    _name = "report.control_cheques.report_prueba_pdf_qweb"
    _description = "Obtener la cuadricula para las coordenadas de cheques"

    @api.model
    def _get_report_values(self, docids, data):
        cheques_impresion = False
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        print(data["ids"])
        if data["ids"]:
            cheques_impresion = self.env["control_cheques.impresion_sarita"].search(
                [("id", "in", data["ids"])]
            )
        print(cheques_impresion.name)
        return {
            "docs": cheques_impresion,
            "company": self.env.user.company_id,
        }

class Report_report_pruebas_de_cheque(models.AbstractModel):
    _name = "report.control_cheques.report_pruebas_de_cheque_qweb"
    _description = "Obtener un cheque de prueba para las coordenadas de cheques"

    @api.model
    def _get_report_values(self, docids, data):
        cheques_impresion = False
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        print(data["ids"])
        if data["ids"]:
            cheques_impresion = self.env["control_cheques.impresion_sarita"].search(
                [("id", "in", data["ids"])]
            )
        return {
            "docs": cheques_impresion,
            "company": self.env.user.company_id,
        }

class Rpruebas_de_cheque_en_cuad(models.AbstractModel):
    _name = "report.control_cheques.report_cheque_en_cuadricula_qweb"
    _description = "Obtener un cheque de prueba para las coordenadas de cheques"

    @api.model
    def _get_report_values(self, docids, data):
        cheques_impresion = False
        active_model = self.env.context.get("active_model")
        if data is None:
            data = {}
        print(data["ids"])
        if data["ids"]:
            cheques_impresion = self.env["control_cheques.impresion_sarita"].search(
                [("id", "in", data["ids"])]
            )
        return {
            "docs": cheques_impresion,
            "company": self.env.user.company_id,
        }