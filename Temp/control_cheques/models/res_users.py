from odoo import models,tools, fields, api, _

class usuarios_internos(models.Model):
    _inherit = "res.users"

    nombre_corto = fields.Char("Nombre Corto")
    