
from odoo import models,tools, fields, api, _

class ConfiguracionCorrelativos(models.TransientModel):
    _name = 'configuracion.correlativos'


    banco = fields.Many2one('account.journal', string='Cuenta Bancaria',
                                        domain="[('aplica_cheque','=',True)]")
    numero = fields.Integer(string="Nuevo Correlativo")
    usuario = fields.Char(string='Usuario', default=lambda self: self._nombre_del_usuario(), readonly=True)
    company_gt = fields.Many2one('res.company', string='Empresa',  index=True, store=True,
                                 ondelete="restrict",
                                 default=lambda self: self.env['res.company']._company_default_get('sdi.channel'),
                                 readonly=True)

    @api.model
    def _nombre_del_usuario(self):
        return str(self.env.user.name)

    def guardar(self):
        self.insertNewCorrelativo((self.numero-1), self.company_gt.id, self.banco.name,self.banco.id, self.banco.bank_account_id.acc_number, True)



    def insertNewCorrelativo(self, codigo, id_empresa, nombre_cuenta, id_cuenta, no_cuenta, consumo):
        query = """
             INSERT INTO CORRELATIVOS_CHEQUES (CODIGO,ID_EMPRESA,NOMBRE_CUENTA,ID_CUENTA,NO_CUENTA,CONSUMO) 
             VALUES (%s, %s, %s, %s, %s, %s)
         """
        params = (codigo, id_empresa, nombre_cuenta, id_cuenta, no_cuenta, consumo)
        self.env.cr.execute(query, params)
        return

