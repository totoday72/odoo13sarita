# -*- coding: utf-8 -*-
import pytz

from odoo import models,tools, fields, api, _
from odoo.exceptions import AccessError, ValidationError
import datetime
import logging
import math
import re
import time
from .num_letras import num_letras_convertion
_logger = logging.getLogger(__name__)


class ControlCheques(models.Model):
    _name = 'control_cheques.sarita'
    _description = 'Control de Cheques'
    _inherit = ["mail.thread", "mail.activity.mixin"]


    #https://www.odoo.com/documentation/13.0/developer/reference/addons/reports.html
    # Almacena la empresa en que se esta trabajando
    company_gt = fields.Many2one('res.company', string='Empresa', required=True, index=True, store=True,
                                 ondelete="restrict",
                                 default=lambda self: self.env['res.company']._company_default_get('sdi.channel'),
                                 readonly=True)
    tipo_cliente = fields.Selection([('proveedor', 'Proveedor'),('empleado', 'Empleado')],required=True, )


    cliente = fields.Many2one('res.partner',string='Proveedor',domain = "[('supplier_rank','>','0') ]")
    empleado = fields.Many2one('hr.employee', string='Empleado')

    nombre_cheque = fields.Char(string="Cheque a nombre de",default=lambda self: self._nombre_en_cheque(),required=True)

    pago_asociado = fields.Many2many('account.payment',domain="[('partner_type','=','supplier')]")
    cuentas_bancarias = fields.Many2one('account.journal', string='Cuenta Bancaria', domain = "[('aplica_cheque','=',True)]")
    correlativo = fields.Integer(string='Correlativo')
    fecha_cheque = fields.Date(string='Fecha en Cheque', required=True, )
    fecha_cheque_entregado = fields.Date(string='Fecha Entrega/Anulación' )
    fecha = fields.Date(string='Fecha en Sistema', default=lambda self: self.set_fecha_actual(), readonly=True)
    descripcion = fields.Text()
    total_cancelado = fields.Float(string ='Total')
    total_reporte = fields.Char(string='Total')
    total_letras = fields.Char(string='Cantidad en Letras')
    usuario = fields.Char(string='Usuario', default=lambda self: self._nombre_del_usuario(), readonly=True)
    code_banco = fields.Char(string='Cod Banco')
    nombre_banco = fields.Char(string='Banco')
    code_cuenta = fields.Char(string='Cod Cuenta')
    nombre_cuenta = fields.Char(string='Nombre Cuenta')
    anulado_x_vencimiento = fields.Boolean(string='Vencido')
    anulacion = fields.Boolean(string='Anulado')
    sin_editar = fields.Boolean(default=True)
    bandera = fields.Boolean(default=False)
    state = fields.Selection(
        [
            ("borrador", "Borrador"),
            ("emitido", "Emitido"),
            ("entregado", "Entregado"),
            ("conciliado", "Conciliado"),
            ("anulado", "Anulado"),
        ],
        "Estado",
        readonly=True, track_visibility='onchange'
    )

    @api.onchange('cliente')
    def _nombre_en_cheque(self):
        id = self.cliente.id
        nom = self.env['res.partner'].search([('id', '=', id)])
        self.nombre_cheque = nom.nombre_en_cheque



    def formato_num(self, num):
        n = '{:<}{:=12,.2f}'.format('', num)
        return n

    @api.onchange('total_cancelado')
    def _total_cancelado(self):
        f = num_letras_convertion()
        self.total_letras = f.numero_a_moneda_partido_100(self.total_cancelado)
        return

    # @api.onchange('total_cancelado')
    # def _compute_amount_in_word(self):
    #     #t = numero_a_letras(21)
    #     #print('*** ',t)
    #     for rec in self:
    #         rec.num_word = str(rec.cuentas_bancarias.currency_id.amount_to_text(rec.total_cancelado)).replace("Quetzal",
    #                                                                                      "Quetzales exactos.").replace(
    #             ".es y ", " con ").replace("Centavo", "Centavos").replace(
    #             ". y ", " con ").replace("Centavoss", "Centavos").replace(
    #             "Quetzaleses", "Quetzales exactos.").replace("Quetzales exactos.es", "Quetzales exactos.")
    #
    # num_word = fields.Char(string="Total en letras:", compute='_compute_amount_in_word')




    # Retorna la fecha actual del sistema
    def set_fecha_actual(self):
        return datetime.datetime.now(pytz.timezone("America/Guatemala"))

    # Método establece el nombre de usuario logueado.
    @api.model
    def _nombre_del_usuario(self):
        return str(self.env.user.name)

    @api.onchange('cuentas_bancarias')
    def get(self):
        try:
            id_cuenta = self.cuentas_bancarias.id
            self.correlativo = self._get_ultimo_correlativo(id_cuenta)
            self.code_banco = self.cuentas_bancarias.default_credit_account_id.code
            self.nombre_banco = self.cuentas_bancarias.name
        except:
            print('')

    @api.onchange('pago_asociado')
    def get_total(self):
        self.code_cuenta = self.pago_asociado.partner_id.property_account_payable_id.code
        self.nombre_cuenta = self.pago_asociado.partner_id.property_account_payable_id.name
        total = str(sum(self.pago_asociado.mapped('amount')))
        self.total_cancelado = total

    @api.model
    def _get_ultimo_correlativo(self,id_cuenta):
        query = "SELECT CODIGO FROM CORRELATIVOS_CHEQUES WHERE ID = (SELECT MAX(ID) FROM CORRELATIVOS_CHEQUES" \
                " WHERE ID_EMPRESA = " + str(self.company_gt.id)+ " AND ID_CUENTA = "+str(id_cuenta)+")"
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchone()
        correlativo = data['codigo']+1
        return correlativo

    def insertNewCorrelativo(self, codigo, id_empresa, nombre_cuenta, id_cuenta, no_cuenta, consumo):
        query = """
             INSERT INTO CORRELATIVOS_CHEQUES (CODIGO,ID_EMPRESA,NOMBRE_CUENTA,ID_CUENTA,NO_CUENTA,CONSUMO) 
             VALUES (%s, %s, %s, %s, %s, %s)
         """
        params = (codigo, id_empresa, nombre_cuenta, id_cuenta, no_cuenta, consumo)
        self.env.cr.execute(query, params)
        return

    def validar(self):
        query = 'select bank_account_id From account_journal where id ='+str(self.cuentas_bancarias.id)
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchone()
        bank_account_id = data['bank_account_id']
        cuenta = self.env['res.partner.bank'].search([('id', '=', str(bank_account_id))])
        self.insertNewCorrelativo(self.correlativo,self.company_gt.id,self.cuentas_bancarias.name,self.cuentas_bancarias.id,cuenta.acc_number,True)
        self.state = 'borrador'
        self.total_cancelado = str(sum(self.pago_asociado.mapped('amount')))
        self.total_reporte = self.formato_num(self.total_cancelado)
        self._nombre_en_cheque()


    @api.model
    def create(self, vals):
        res = super(ControlCheques, self).create(vals)
        res.correlativo = res._get_ultimo_correlativo(res.cuentas_bancarias.id)
        cod_banco = res.pago_asociado.journal_id.default_credit_account_id.code
        nom_banco = res.pago_asociado.journal_id.name
        if res.nombre_banco == nom_banco and res.code_banco == cod_banco:
            res.validar()
        else:
            raise ValidationError(
                _("Las cuentas Bancarias deben ser iguales")
            )
        return res


    def btn_emitido(self):
        self.state = 'emitido'
        pago = self.env['account.payment'].search([('name', '=', str(self.pago_asociado.name))])
        for p in pago:
            p.numero_cheque = self.correlativo
            p.banco = self.nombre_banco
            p.fecha_c=self.fecha_cheque
            bolson = self.env['bolson.bolson'].search([('id', '=', p.bolson_id.id)])
            for asiento in bolson:
                for i in range(len(bolson.facturas)):
                    facturas = self.env['account.move'].search([('name', '=', asiento.facturas[i].name)])
                    for factura in facturas:
                        factura.numero_cheque = self.correlativo
                        factura.banco = self.nombre_banco
                        factura.fecha_c = self.fecha_cheque
        return

    def btn_entregado(self):
        self.state = 'entregado'
        return

    def btn_conciliado(self):
        self.state = 'conciliado'
        return

    def btn_anulado(self):
        if not bool(self.anulacion) != bool(self.anulado_x_vencimiento):
            raise ValidationError(
                _("Debe especificar un solo tipo de anulación")
            )
        else:
            self.state = 'anulado'
            self.bandera = True





class ReportGyT(models.AbstractModel):

    _name='report.control_cheques.report_sarita_gyt'

    def _get_impresion_xy(self, user_id, company_id, bank_journal_id):
        conf_cheque = self.env["control_cheques.impresion_sarita"].search(
            ['&', ("user_id", "=", user_id.id), '&', ("company_id", "=", company_id.id), ("bank_jounal_id", "=", bank_journal_id.id)]
        )
        if len(conf_cheque) > 1:
            return conf_cheque[0]
        return conf_cheque

    @api.model
    def _get_report_values(self, docids, data=None):
        user_id = self.env.user
        company_id = self.env.company
        # print(docids)
        bank_journal_id = False
        for doc in docids:
            cheque = self.env["control_cheques.sarita"].search(
                [("id", "=", doc)]
            )
            bank_journal_id = self.env["account.journal"].search(
                [("id", "=", cheque.cuentas_bancarias.id)]
            )
            break
        # print('User>', user_id.name, " company>", company_id.name, " cuenta bancaria>", bank_journal_id.name)
        valuesxy = self._get_impresion_xy(user_id, company_id, bank_journal_id)
        # print("tamano_campo>", valuesxy.tamano_campo, " usario>", valuesxy.user_id)
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('control_cheques.report_sarita_gyt')
        return {
            'doc_ids': docids,
            'doc_model': self.env['control_cheques.sarita'],
            'docs': self.env['control_cheques.sarita'].browse(docids),
            'xy': valuesxy,
            'user': user_id,
        }

class ReportBi(models.AbstractModel):

    _name='report.control.report_sarita_bi'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('control_cheques.report_sarita_bi')
        return {
            'doc_ids': docids,
            'doc_model': self.env['control_cheques.sarita'],
            'docs': self.env['control_cheques.sarita'].browse(docids)
        }

class ReportBanrural(models.AbstractModel):

    _name='report.control.report_sarita_banrural'

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('control_cheques.report_sarita_banrural')
        return {
            'doc_ids': docids,
            'doc_model': self.env['control_cheques.sarita'],
            'docs': self.env['control_cheques.sarita'].browse(docids)
        }


class CuentaBancaria(models.Model):
    _inherit = ["account.journal"]
    aplica_cheque = fields.Boolean(string='Aplica para Cheques', default=False)

class Pagos(models.Model):
    _inherit = ["account.payment"]
    banco = fields.Char(string="Banco")
    numero_cheque = fields.Integer(string='Cheque Asociado')
    fecha_c = fields.Date(string='Fecha en Cheque')

class Factura(models.Model):
    _inherit = ["account.move"]
    banco = fields.Char(string="Banco")
    numero_cheque = fields.Integer(string='Cheque Asociado')
    fecha_c = fields.Date(string='Fecha en Cheque')

