# -*- coding: utf-8 -*-
{
    'name': "control_cheques",

    'summary': """
        Módulo para el monitoreo de emision de cheques""",

    'description': """
        Control general de cheques...
    """,

    'author': "Anibal Gomez Morales, Erick Hernandez To",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale_management'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        "data/cheques_sequence.xml",
	    'views/account_journal.xml',
        'views/views.xml',
        'views/configuracion_impresion_cheques.xml',
        'wizard/configuracion.xml',
        'views/menu.xml',
        'reports/plantillas.xml',
        'reports/cheque_gyt.xml',
        'reports/cheque_bi.xml',
        'reports/cheque_banrural.xml',
        'reports/custom_header_footer_cheque.xml',
        'reports/pdf_cheque_cuadricula_prueba.xml',
        'reports/pdf_pruebas_cheque.xml',
        'reports/pdf_cheque_en_cuadricula.xml',
        'views/res_partner.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
