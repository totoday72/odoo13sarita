# -*- coding: utf-8 -*-
{
    'name': "estadistica_prosar",

    'summary': """
        Aplicacion para el departamento de estadistica de Restaurantes Sarita""",

    'description': """
        Esta aplicacion contendra todo lo relacionado al departamento de estadistica de restaurante sarita
    """,

    'author': "Erick Daniel Hernandez To",
    'website': "http://www.facebook.com/totoday72",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Statistic',
    "version": "13.0.1.0.0",
    "license": "AGPL-3",
    # any module necessary for this one to work correctly
    'depends': ['base','hotel','hotel_reservation','prosar-odoo','info__fel'],

    # always loaded
    'data': [
        "security/statistic_security.xml",
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        "views/menus.xml",
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "images": ["static/description/icon.png","static/description/src/img/icon.png"],
    "application": True,
}
