

from odoo import models, fields, api

class estadistica_values(models.Model):
    _name = 'estadistica__prosar.estadistica_values'
    _description = 'estadistica__prosar.estadistica_values'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()

    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100

class estadistica__prosar(models.Model):
    _name = 'estadistica__prosar.estadistica__prosar'
    _description = 'estadistica__prosar.estadistica__prosar'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()

    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100

