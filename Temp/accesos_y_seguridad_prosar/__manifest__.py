# -*- coding: utf-8 -*-
{
    'name': "Accesos_y_Seguridad_PROSAR",

    'summary': """
       Modulo para poder modificar el acceso a los usuarios en los modulos de hotel y hotel_reservation, a futuro se podran
       agregar mas modulos que se modifiquen los permisos con este modulo""",

    'description': """
        Modulo para modificar los accesos de los usuarios a las aplicaciones y modulos de odoo Sarita y Proseresa.
    """,

    'author': "Erick Daniel Hernandez To",
    'website': "http://www.facebook.com/totoday72",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Security',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hotel','hotel_reservation','sale', 'prosar-odoo'],

    # always loaded
    'data': [
        "security/hotel_security.xml",
        'security/cheques_security.xml',
        'security/menus_security.xml',
        'security/ir.model.access.csv',
        'data/rules_registry.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/uom_uom.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "images": ["static/description/icon.png", "static/description/src/img/icon.png"],
    "application": True,
}
