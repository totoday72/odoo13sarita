from odoo import fields, models, api, _
from odoo.exceptions import UserError
import re


class unidadesdemedida(models.Model):
    _inherit = 'uom.uom'

    company_id = fields.Many2one(comodel_name='res.company', default=lambda self: self.env.user.company_id)
