# # -*- coding: utf-8 -*-
# #
# from odoo import models, fields, api
#
#
# class accesos_y_seguridad_prosar(models.Model):
#     _inherit = "stock.warehouse"
#     company_id = fields.Many2many(
#         string="Companies",
#         comodel_name="res.company",
#         default=lambda self: self.env.company,
#     )
#     _name = 'accesos_y_seguridad_prosar.accesos_y_seguridad_prosar'
#     _description = 'accesos_y_seguridad_prosar.accesos_y_seguridad_prosar'
#
#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
