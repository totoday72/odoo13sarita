# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, pytz
import json


class ReportCheckin(models.AbstractModel):
    _name = "report.hotel.report_documento_credito_qweb"
    _description = "Auxiliar to get the check in report"

    @api.model
    def _get_report_values(self, docids, data):
        active_model = self.env.context.get("active_model")
        folio_name = data["folio_name"]
        print(folio_name)
        folio_id = data["folio_id"]
        print(str(folio_id))
        folio = self.env["hotel.folio"].search(['&', ('id', '=', folio_id), ('name', '=', folio_name)])
        if folio:
            print(folio.name)
            print(folio.invoice_status)
        return {
            "folio": folio,
            "doc_model": data["model"],
            "company": self.env.user.company_id,
        }


class product():
    fecha_de_cargo = ""
    nombre = ""
    descripcion = ""
    cantidad = ""
    precio_unitario = 0
    cargos = 0
    subtotal = 0
    precio_unitario_text = ""
    cargos_text = ""
    subtotal_text = ""
    saldo_a_favor_text = ""

    def __init__(self, fecha, nombre, descripcion, cantidad, precio_unitario, subtotalanterior):
        self.fecha_de_cargo = fecha
        self.nombre = nombre
        self.descripcion = descripcion
        self.cantidad = '{:,.0f}'.format(cantidad)
        self.precio_unitario = precio_unitario
        self.precio_unitario_text = self.account_aligment(precio_unitario)
        self.cargos = precio_unitario * cantidad
        self.cargos_text = self.account_aligment(self.cargos)
        self.subtotal = subtotalanterior
        self.subtotal_text = self.account_aligment(subtotalanterior)
        self.saldo_a_favor_text = self.account_aligment(0.00)

    def account_aligment(self, numero):
        return '{:<}{:*>15,.2f}'.format('', numero).replace('*', u'\xa0').encode('utf-8')

class saldo():
    movimiento = ""
    fecha = ""
    informacion = ""
    forma_pago = 0
    saldo_a_favor = 0
    subtotal = 0
    total = 0
    precio_unitario_text = ""
    cargos_text = ""
    saldo_a_favor_text = ""
    subtotal_text = ""

    def __init__(self, movimiento, fecha, informacion, forma_pago, saldo_a_favor, subtotal):
        self.movimiento = movimiento
        self.fecha = fecha
        self.informacion = informacion
        self.forma_pago = forma_pago
        self.saldo_a_favor = saldo_a_favor
        self.subtotal = subtotal
        self.precio_unitario_text = self.account_aligment(0.00)
        self.cargos_text = self.account_aligment(0.00)
        self.saldo_a_favor_text = self.account_aligment(saldo_a_favor)
        self.subtotal_text = self.account_aligment(subtotal)

    def account_aligment(self, numero):
        return '{:<}{:*>15,.2f}'.format('', numero).replace('*', u'\xa0').encode('utf-8')


class Report_report_estado_de_cuenta(models.AbstractModel):
    # Erick> ESTA CLASE SE LLAMA POR MEDIO DE NAME Y FILE EN <report id = "report_estado_de_cuenta"
    _name = "report.hotel.report_report_estado_de_cuenta_qweb"
    _description = "Auxiliar to get the check in report"

    def account_aligment(self, numero):
        return '{:<}{:*>15,.2f}'.format('Q', numero).replace('*', u'\xa0').encode('utf-8')

    @api.model
    def _get_report_values(self, docids, data):  # Erick
        active_model = self.env.context.get("active_model")
        folio_name = data["folio_name"]
        folio_id = data["folio_id"]
        folio = self.env["hotel.folio"].search(['&', ('id', '=', folio_id), ('name', '=', folio_name)])
        products = []
        saldo_abonado = []
        habitacion = ""
        totalsaldo = 0
        if folio:
            total_pesonas = folio.reservation_id.adults + folio.reservation_id.children
            checkin = folio.checkin_date - timedelta(hours=6)
            checkout = folio.checkout_date - timedelta(hours=6)
            for p in folio.room_line_ids:
                habitacion = habitacion + " " + p.name.split(' ')[3]
                dhab = p.checkin_date - timedelta(hours=6)
                totalsaldo = totalsaldo + p.product_uom_qty * p.price_unit
                p1 = product(dhab.strftime("%d/%B/%Y, %H:%M"), p.name, p.product_id.name, p.product_uom_qty,
                             round(p.price_unit, 2), round(totalsaldo, 2))
                products.append(p1)
            for p in folio.service_line_ids:
                totalsaldo = totalsaldo + p.product_uom_qty * p.price_unit
                dhab = p.serv_date_in - timedelta(hours=6)
                p1 = product(dhab.strftime("%d/%B/%Y, %H:%M"), p.name, p.product_id.name, p.product_uom_qty,
                             round(p.price_unit, 2), round(totalsaldo, 2))
                products.append(p1)
            if folio.partner_id.parent_id:
                query = ("""
                        SELECT a.name, a.currency_id, a.create_date, a.journal_id, b.amount_residual, a.informacion  
                            FROM account_payment A
                            inner join account_move_line B
                            ON A.id=B.payment_id
                            where A.state='posted'
                            and B.parent_state='posted'
                            and A.journal_id=B.journal_id
                            and A.payment_type='inbound'
                            and B.ref is null
                            and B.account_internal_type='receivable'
                            and B.reconciled='false'
                            and B.amount_residual<0
                            and (B.partner_id=%s or B.partner_id=%s)
                       """ % (folio.partner_id.parent_id.id, folio.partner_id.id))
            else:
                query = ("""
                                        SELECT a.name, a.currency_id, a.create_date, a.journal_id, b.amount_residual, a.informacion  
                                            FROM account_payment A
                                            inner join account_move_line B
                                            ON A.id=B.payment_id
                                            where A.state='posted'
                                            and B.parent_state='posted'
                                            and A.journal_id=B.journal_id
                                            and A.payment_type='inbound'
                                            and A.partner_id=B.partner_id
                                            and B.ref is null
                                            and B.account_internal_type='receivable'
                                            and B.reconciled='false'
                                            and B.amount_residual<0
                                            and B.partner_id=%s
                                       """ % (folio.partner_id.id))
            self.env.cr.execute(query)
            query_result = self.env.cr.dictfetchall()
            totalsaldo_texto = ""
            currency_id = 0
            for x in query_result:
                currency_id = x["currency_id"]
                f = x["create_date"] - timedelta(hours=6)
                diario_s = self.env["account.journal"].search([('id', '=', x['journal_id'])])
                totalsaldo = totalsaldo + x['amount_residual']
                s = saldo(x['name'], f.strftime("%d/%B/%Y, %H:%M"), x['informacion'], diario_s.name,
                          x['amount_residual'], totalsaldo)
                saldo_abonado.append(s)
                # print(totalsaldo)
            moneda = self.env["res.currency"].search([('id', '=',
                                                       170)])  # se cambio el valor currency_id a 170 porque puede ser que el diario no tenga moneda
            # print(moneda)
            # d = '{:<}{:=12,.2f}'.format('Q', 123.326) para poder colocar la Q y el numero de que se quiere ordenar.
            d = '{:<}{:=12,.2f}'.format('Q', 123.326)
            print(d)
            totalsaldo_texto = str(moneda.amount_to_text(totalsaldo)).replace("Quetzal",
                                                                              "Quetzales exactos.").replace(
                ".es y ", " con ").replace("Centavo", "Centavos").replace(
                ". y ", " con ").replace("Centavoss", "Centavos").replace(
                "Quetzaleses", "Quetzales exactos.").replace("Quetzales exactos.es", "Quetzales exactos.")

        return {
            "folio": folio,
            "products": products,
            "habitacion": habitacion,
            "doc_model": data["model"],
            "abono": saldo_abonado,
            "total_saldo": self.account_aligment(totalsaldo),
            "total_saldo_texto": totalsaldo_texto,
            "total_pesonas": str(total_pesonas),
            "company": self.env.user.company_id,
            "check_in": checkin.strftime("%d/%B/%Y, %H:%M"),
            "check_out": checkout.strftime("%d/%B/%Y, %H:%M"),
        }
