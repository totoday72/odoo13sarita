# -*- coding: utf-8 -*-

from . import models
from . import mark1_product_category
from . import mark1_product_template
from . import mark1_purchase_order
from . import mark1_res_partner
from . import mark1_sale_order
from . import mark1_account_move

