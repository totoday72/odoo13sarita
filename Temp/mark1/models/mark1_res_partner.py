from odoo import fields, models, api
from odoo.exceptions import UserError
import json


# Se realizan Cambios en modulo Compras/ Pedidos/Proveedor
class CCompraProveedor(models.Model):
    _inherit = 'res.partner'

    # Vista "Crear Proveedor".
    # Se Agrega el campo: "Nombre en Cheque".
    nombre_en_cheque = fields.Char(string="Nombre en Cheque:")
    # Estable la direccion de Facturacion por defecto
    type = fields.Selection(default='invoice')
    # Estable valores por defecto

    zip = fields.Char(default="502", readonly=True, store=True)
    state_id = fields.Many2one(default=lambda self: self._set_country(), store=True)


    # Establece la Compañia por defecto.
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'), readonly=True)


    # Establece el pais de Gutemala.
    @api.model
    def _set_country(self):
        # Id de Guatemala dentro del sistema Odoo
        return 185

    # Se sobre escribe el valor de la variable name.
    name = fields.Char(default="")

    @api.onchange('name')
    def set_account(self):
        # Coloca el nombre en Mayuscula
        self.name = str(self.name).upper()
        # Set Plazo de pago inmediato
        self.property_payment_term_id = 1



