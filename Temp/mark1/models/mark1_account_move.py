from odoo import fields, models, api
from odoo.exceptions import UserError
import json



class CAccountMove(models.Model):
    _inherit = 'account.move'

    boleta_inguat = fields.Char(string='No. Boleta Inguat', default="", store=True)

    # invoice_payment_term_id = fields.Many2one(readonly=True)