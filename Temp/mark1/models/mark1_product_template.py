
from odoo import fields, models, api
from odoo.exceptions import UserError
import json


class SeteosGenerales(models.Model):
    _inherit = 'product.template'


    name = fields.Char(string='Name', default=lambda self: self._get_default_name())
    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'), readonly=True)

    # default_code = fields.Char(required=True, readonly=True)
    nuevoCodigo = []

    @api.onchange('name')
    def set_account(self):
        self.name = str(self.name).upper()
        if self.env.company.name == "HOTEL SARITA, S.A.":
            self.default_code =self.getCodigoProductoHotel()
        return

    @api.model
    def _get_default_name(self):
        return ""

    @api.model
    def _get_default_property_account_income_id(self):
        return ""  # 435

    @api.model
    def _get_default_property_account_expense_id(self):
        return ""  # 377

    @api.model
    def _get_default_categ(self):
        return ""

    @api.model
    def _get_default_code(self):
        return ""


    def generaCodigoUnico(self, value):
        bodega = ""
        cateSeleccionada = ""
        datos = value.split("/")
        for nombre in datos:
            if nombre.strip() == "All":
                return "error"
        for x in range(0, len(datos) - 1):
            # Busco la Bodega
            if datos[0].strip() == "SECO":
                bodega = "1"
            if datos[0].strip() == "FRIO":
                bodega = "2"
            if datos[0].strip() == "CONGELADO":
                bodega = "3"
            if datos[0].strip() == "NO ALMACENABLE":
                bodega = "4"
            # Busco la categoria
            if datos[1].strip() == "PRODUCTO TERMINADO":
                cateSeleccionada = "1"
            if datos[1].strip() == "MATERIA PRIMA":
                cateSeleccionada = "2"
            if datos[1].strip() == "MATERIAL DE EMPAQUE":
                cateSeleccionada = "3"
            if datos[1].strip() == "MATERIAL DE LIMPIEZA":
                cateSeleccionada = "4"
            if datos[1].strip() == "MATERIAL DE PRODUCCION":
                cateSeleccionada = "5"
            if datos[1].strip() == "GASTO":
                cateSeleccionada = "6"
            if datos[1].strip() == "SERVICIO":
                cateSeleccionada = "7"
            if datos[1].strip() == "REPUESTO":
                cateSeleccionada = "8"
            if datos[1].strip() == "PAPELERIA Y UTILIES":
                cateSeleccionada = "9"
            if datos[1].strip() == "OTROS":
                cateSeleccionada = "0"
        if bodega == "" or cateSeleccionada == "":
            raise UserError(('Debe seleccionar un bodega con su respectiva categoria'))
        else:
            num_correlatvo = self.getCorrelativo(bodega, cateSeleccionada)
        self.nuevoCodigo.append(bodega)
        self.nuevoCodigo.append(cateSeleccionada)
        self.nuevoCodigo.append(str(num_correlatvo))
        codigoAutomatico = bodega + cateSeleccionada + str(num_correlatvo)
        return codigoAutomatico

    def getCorrelativo(self, bodega, categoria):
        query="select max(correlativo) from codigo_producto_proseresa where bodega =" + str(bodega) + " and categoria =" + str(
            categoria) + ";"
        print("*************************************",query)
        self.env.cr.execute(str(query.replace("'", " ")))
        valor = self.env.cr.dictfetchone()
        y = json.loads(str(valor).replace("'", "\""))
        correlativo = int(y["max"]) + 1
        return correlativo

    def insertNewCodigo(self, bodega, categoria, correlativo):
        query = """
              INSERT INTO codigo_producto_proseresa(bodega, categoria, correlativo)
              VALUES (%s, %s, %s)
          """
        params = (bodega, categoria, correlativo)
        self.env.cr.execute(query, params)
        return ""

    @api.onchange('categ_id')
    def _set_codigo(self):
        if self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
            id_categoria = self.categ_id.id
            categorias = self.env['product.category'].search([('id', '=', id_categoria)])
            categoriaCompleta = ""
            for conpleteCateg in categorias:
                categoriaCompleta += conpleteCateg.complete_name
                self.default_code = self.generaCodigoUnico(categoriaCompleta)
            #     if default_code == "error":
            #         self.default_code = "Código Invalido"
            #         return
            #
            # self.default_code = self.generaCodigoUnico(categoriaCompleta)
            else:
                print("sali")
        return

    @api.model
    def create(self, vals):
        res = super(SeteosGenerales, self).create(vals)
        if self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
            _bodega=self.nuevoCodigo[0]
            _categoria = self.nuevoCodigo[1]
            _num_correlativo = self.nuevoCodigo[2]
            self.insertNewCodigo(_bodega, _categoria, _num_correlativo)
        if self.env.company.name == "HOTEL SARITA, S.A.":
            self.insertarCodigoProductoHotel()
        self.nuevoCodigo.clear()
        return res





    def getCodigoProductoHotel(self):
        empresa = self.env['res.company']._company_default_get('sdi.channel').name
        query = "SELECT * FROM codigo_producto_hotel where empresa = '"+empresa+"' ORDER BY idproducto DESC LIMIT 1 "
        self.env.cr.execute(str(query))
        valor = self.env.cr.dictfetchone()
        y = json.loads(str(valor).replace("'", "\""))
        correlativo = int(y["idproducto"]) + 1
        return correlativo


    def insertarCodigoProductoHotel(self):
        try:
            numero = self.getCodigoProductoHotel()
            empresa = "'"+self.env['res.company']._company_default_get('sdi.channel').name+"'"
            cadena = str(numero)+","+str(empresa)
            query = """
                      INSERT INTO codigo_producto_hotel(idproducto,empresa)
                      VALUES ("""+cadena+""")
                  """
            self.env.cr.execute(query)
        except:
            numero = 1
            empresa = "'" + self.env['res.company']._company_default_get('sdi.channel').name + "'"
            cadena = str(numero) + "," + str(empresa)
            query = """
                      INSERT INTO codigo_producto_hotel(idproducto,empresa)
                      VALUES (""" + cadena + """)
                  """
            self.env.cr.execute(query)
        return ""

























