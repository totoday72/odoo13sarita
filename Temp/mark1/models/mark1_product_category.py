from odoo import fields, models, api
from odoo.exceptions import UserError
import json


class CInventario(models.Model):
    _inherit = 'product.category'

    name = fields.Char(
        string='Name',
        default=lambda self: self._get_default_name(),
    )

    x_empresagt = fields.Many2one(
        'res.company', string='Company', 
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'))

    @api.model
    def _get_default_name(self):
        return ""

    @api.model
    def _get_default_property_account_income_categ_id(self):
        return ""  # 435

    @api.model
    def _get_default_property_account_expense_categ_id(self):
        return ""  # 377

    # route_ids = fields.Many2many(readonly=True)
    # total_route_ids = fields.Many2many(readonly=True)
    # property_cost_method = fields.Selection(readonly=True)
    # property_valuation = fields.Selection(readonly=True)
    # property_account_creditor_price_difference_categ = fields.Many2one(default="", readonly=True)
    # property_stock_valuation_account_id = fields.Many2one(default="", readonly=True)
    # property_stock_account_input_categ_id = fields.Many2one(default="", readonly=True)
    # property_stock_account_output_categ_id = fields.Many2one(default="", readonly=True)
    # property_stock_journal = fields.Many2one(default="", readonly=True)


    @api.onchange('name')
    def set_account(self):
        # Coloca el nombre en Mayuscula
        self.name = str(self.name).upper()
        # Setea las cuentas respectivas por defecto.
        # self.property_account_income_categ_id = 435
        # self.property_account_expense_categ_id = 377
        return
