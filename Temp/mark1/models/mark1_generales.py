
from odoo import fields, models, api
from odoo.exceptions import UserError
import json


class SeteosGenerales(models.Model):
    _inherit = 'product.template'

    name = fields.Char(
        string='Name',
        default=lambda self: self._get_default_name(),
    )

    empresa = fields.Many2one('res.company', string='Company', required=True, default=lambda self:
                                self.env['res.company']._company_default_get('sdi.channel'), readonly=True)

    # property_account_income_id = fields.Many2one(default=lambda self: self._get_default_property_account_income_id(), store=True)
    # property_account_expense_id = fields.Many2one(default=lambda self: self._get_default_property_account_expense_id(), store=True)

    # ****************************************************************************
    # default_code = fields.Char(required=True)

    muestra = []

    @api.model
    def _get_default_categ(self):
        return ""

    @api.model
    def _get_default_code(self):
        return "5"




    def generaCodigoUnico(self, value):
        bodega = ""
        cateSeleccionada = ""
        datos = value.split("/")
        for nombre in datos:
            if nombre.strip() == "All":
                return "error"

        for x in range(0, len(datos) - 1):
            # Busco la Bodega
            if datos[0].strip() == "SECO":
                bodega = "1"
            if datos[0].strip() == "FRIO":
                bodega = "2"
            if datos[0].strip() == "CONGELADO":
                bodega = "3"
            if datos[0].strip() == "NO ALMACENABLE":
                bodega = "4"
            # Busco la categoria
            if datos[1].strip() == "PRODUCTO TERMINADO":
                cateSeleccionada = "1"
            if datos[1].strip() == "MATERIA PRIMA":
                cateSeleccionada = "2"
            if datos[1].strip() == "MATERIAL DE EMPAQUE":
                cateSeleccionada = "3"
            if datos[1].strip() == "MATERIAL DE LIMPIEZA":
                cateSeleccionada = "4"
            if datos[1].strip() == "MATERIAL DE PRODUCCION":
                cateSeleccionada = "5"
            if datos[1].strip() == "GASTO":
                cateSeleccionada = "6"
            if datos[1].strip() == "SERVICIO":
                cateSeleccionada = "7"
            if datos[1].strip() == "REPUESTO":
                cateSeleccionada = "8"
            if datos[1].strip() == "PAPELERIA Y UTILIES":
                cateSeleccionada = "9"
            if datos[1].strip() == "OTROS":
                cateSeleccionada = "0"

        if bodega == "" or cateSeleccionada == "":
            raise UserError(('Debe seleccionar un bodega con su respectiva categoria'))
        else:
            num_correlatvo = self.getCorrelativo(bodega, cateSeleccionada)

        self.muestra.append(bodega)
        self.muestra.append(cateSeleccionada)
        self.muestra.append(str(num_correlatvo))
        salida = bodega + cateSeleccionada + str(num_correlatvo)
        return salida

    def getCorrelativo(self, bodega, categoria):
        query = "select max(correlativo) from codigos_odoo where bodega =" + str(bodega) + " and categoria =" + str(
            categoria) + ";"
        self.env.cr.execute(str(query.replace("'", " ")))
        valor = self.env.cr.dictfetchone()
        y = json.loads(str(valor).replace("'", "\""))
        correlativo = int(y["max"]) + 1
        print(y["max"])
        return correlativo

    def insertNewCodigo(self, bodega, categoria, correlativo):
        query = """
              INSERT INTO codigos_odoo(bodega, categoria, correlativo)
              VALUES (%s, %s, %s)
          """
        params = (bodega, categoria, correlativo)
        self.env.cr.execute(query, params)

        return ""

    @api.onchange('categ_id')
    def _set_codigo(self):
        if self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
            id_categoria = self.categ_id.id
            categorias = self.env['product.category'].search([('id', '=', id_categoria)])
            categoriaCompleta = ""
            for conpleteCateg in categorias:
                categoriaCompleta += conpleteCateg.complete_name
                self.default_code = self.generaCodigoUnico(categoriaCompleta)
            #     if default_code == "error":
            #         self.default_code = "Código Invalido"
            #         return
            #
            # self.default_code = self.generaCodigoUnico(categoriaCompleta)
            else:
                print("sali")
        return

    @api.model
    def create(self, vals):
        res = super(SeteosGenerales, self).create(vals)
        if self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
            _bodega=self.muestra[0]
            _categoria = self.muestra[1]
            _num_correlativo = self.muestra[2]
            self.insertNewCodigo(_bodega, _categoria, _num_correlativo)
        self.muestra.clear()
        return res





    # ****************************************************************************

    @api.model
    def _get_default_name(self):
        return ""

    @api.model
    def _get_default_property_account_income_id(self):
        return ""  # 435

    @api.model
    def _get_default_property_account_expense_id(self):
        return ""  # 377

    @api.onchange('name')
    def set_account(self):
        self.name = str(self.name).upper()
        return


# Se realizan Cambios en modulo Compras/ Pedidos/Proveedor
class CCompraProveedor(models.Model):
    _inherit = 'res.partner'

    # Vista "Crear Proveedor".
    # Se Agrega el campo: "Nombre en Cheque".
    nombre_en_cheque = fields.Char(string="Nombre en Cheque:")
    company_id = fields.Many2one(readonly=True)

    # Se sobre escribe el valor de la variable name.
    name = fields.Char(default="")

    @api.onchange('name')
    def set_account(self):
        # Coloca el nombre en Mayuscula
        self.name = str(self.name).upper()
        return


class CompraPedido(models.Model):
    _inherit = 'purchase.order'
    company_id = fields.Many2one(readonly=True)

    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'), readonly=True)

    # En la vista account_move_view.xml se agrego string = "Serie y No. de Factura" al campo
    # <field name="ref"/>


# Se realizan Cambios en modulo Venta/Pedido
class CVenta(models.Model):
    _inherit = 'sale.order'

    # Campo en vista Pedido.
    nota_despacho = fields.Char(string="Nota de Despacho: ")


class Cbase(models.Model):
    _inherit = 'res.partner'

    @api.onchange('name')
    def set_upper(self):
        self.name = str(self.name).upper()


class CInventario(models.Model):
    _inherit = 'product.category'

    name = fields.Char(
        string='Name',
        default=lambda self: self._get_default_name(),
    )

    ver_cuentas = fields.Char(string='cuentas', readonly=True)
    empresa = fields.Many2one('res.company', string="Company", required=True,
                                default=lambda self: self.env.user.company_id.id)

    @api.model
    def _get_default_name(self):
        return ""

    @api.model
    def _get_default_property_account_income_categ_id(self):
        return ""  # 435

    @api.model
    def _get_default_property_account_expense_categ_id(self):
        return ""  # 377

    route_ids = fields.Many2many(readonly=True)
    total_route_ids = fields.Many2many(readonly=True)
    property_cost_method = fields.Selection(readonly=True)
    property_valuation = fields.Selection(readonly=True)
    property_account_creditor_price_difference_categ = fields.Many2one(default="", readonly=True)
    property_stock_valuation_account_id = fields.Many2one(default="", readonly=True)
    property_stock_account_input_categ_id = fields.Many2one(default="", readonly=True)
    property_stock_account_output_categ_id = fields.Many2one(default="", readonly=True)
    property_stock_journal = fields.Many2one(default="", readonly=True)
    property_account_income_categ_id = fields.Many2one(readonly=True)
    property_account_expense_categ_id = fields.Many2one(readonly=True)

    @api.onchange('name')
    def set_account(self):
        # Coloca el nombre en Mayuscula
        self.name = str(self.name).upper()
        # Setea las cuentas respectivas por defecto.
        # self.property_account_income_categ_id = 435
        # self.property_account_expense_categ_id = 377
        return

        return
