from odoo import fields, models, api
from odoo.exceptions import UserError
import json


# Se realizan Cambios en modulo Venta/Pedido
class CVenta(models.Model):
    _inherit = 'sale.order'

    # Campo en vista Pedido.
    nota_despacho = fields.Char(string="Nota de Despacho: ")
