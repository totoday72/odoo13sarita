from odoo import fields, models, api
from odoo.exceptions import UserError
import json

class CompraPedido(models.Model):
    _inherit = 'purchase.order'

    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self:
        self.env['res.company']._company_default_get('sdi.channel'), readonly=True)



