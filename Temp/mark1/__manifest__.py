# -*- coding: utf-8 -*-
{
    'name': "Mark I",

    'summary': """
        
        Módulo desarrollado por Aníbal Gómez Morales""",

    'description': """
        Establece por defecto la empresa, al momento de la creación de un producto.
        Establece por defecto las cuentas de ingreso y de gastos al momento de la creación de un producto.
        
    """,

    'author': "RESASA GT | Aníbal Gómez Morales",
    'website': "http://www.saritarestaurante.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase','stock','product','account','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/cambios_en_vistas.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
