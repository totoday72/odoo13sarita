# -*- coding: utf-8 -*-
{
    'name': "Sarita_Add",
    'version': '0.1',
    'summary': """SaritaAdd""",
    'description': """Mi modulo""",
    'author': "Mynor Agustin",
    'maintainer': "Mynor Agustin",
    'Company': "RESASA",
    'website': "https://saritarestaurante.com/",

    # any module necessary for this one to work correctly
    'depends': ['base','account'],
    'license': 'LGPL-3',
    'category': 'Sales',

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        # 'views/templates.xml',
        'views/viw_account_payment.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
