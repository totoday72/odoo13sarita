from odoo import fields, models, api

class modif_pago(models.Model):
    _inherit = 'account.payment'

    # x_debe_payment = fields.Monetary(
    #     # x_debe_payment = fields.Char(
    #     string='Debe', related='move_line_ids.debit', readonly=True
    #     , index=True
    #     #default=lambda self: self._get_default_name(),
    # )
    # x_credit_payment = fields.Monetary(
    #     string='Credito',related='move_line_ids.credit', readonly=True
    #     # default=lambda self: self._get_default_name(),
    # )

    x_ap = fields.Many2one(
        'account.move', string='Asiento',
        related='move_line_ids.move_id',
        readonly=True
        # default=lambda self: self._get_default_name(),
    )

# uso para el manejo de cheques
class AccountPayment(models.Model):
    _inherit = 'account.payment'

    # @api.multi
    def _compute_amount_in_word(self):
        for rec in self:
            rec.num_word = str(rec.currency_id.amount_to_text(rec.amount)).replace("Quetzal",
                                                                                   "Quetzales exactos.").replace(
                ".es y ", " con ").replace("Centavo", "Centavos").replace(
                ". y ", " con ").replace("Centavoss", "Centavos").replace(
                "Quetzaleses", "Quetzales exactos.").replace("Quetzales exactos.es", "Quetzales exactos.")

    num_word = fields.Char(string="Total en letras:", compute='_compute_amount_in_word')


