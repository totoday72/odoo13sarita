# -*- coding: utf-8 -*-
# from numpy.core import size

from odoo import api, fields, models, _
import datetime
import base64
import json
import uuid
import qrcode
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from io import BytesIO
import requests
# import numpy

class Inf_Factura_Electronica(models.Model):
    _inherit = 'account.move'

    inf_fecha_certifica = fields.Char(string='Fecha certifica')  # Devuelta por SAT
    inf_serie = fields.Char(sgring='Serie')
    inf_numero_autorizacion = fields.Char(string='Número autorización')
    inf_numero_factura = fields.Char(string='Número FEL')
    inf_ruta_doc = fields.Char(string="Link factura")
    inf_anula_numero = fields.Char(string='Número Anula')
    inf_anula_autoriza = fields.Char(string='Serie Anula')
    inf_anula_serie = fields.Char(string='Serie Anula')
    inf_anula_fecha = fields.Char(string="Fecha Anula")
    inf_anula_codigoUUID = fields.Char(string="Codigo unico UUID: ")
    inf_certifica_codigoUUID = fields.Char(string="Codigo unico UUID: ")
    inf_certificador_nombre = fields.Char(string="Nombre certificador")
    inf_certificador_nit = fields.Char(string="NIT certificador")
    inf_image_qr = fields.Binary(string="QR FEL")
    inf_fecha_certificaorg = fields.Char(string='Fecha Larga certifica')  # Devuelta por SAT
    generar_fel_factura = fields.Boolean(string="Generar Fel al Publicar Factura", default='True')
    numero_corte = fields.Integer(string='No. corte')
    publicar_1ero_mes = fields.Boolean(string="Publicar el 1ero del siguiente mes", default=False)
    intentos_publicar = fields.Integer(string="Intentos de facturacion", default=0)