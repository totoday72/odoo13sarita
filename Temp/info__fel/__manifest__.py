# -*- coding: utf-8 -*-
{
    'name': "Info_FEL",

    'summary': """
        Modulo que guardara un backup de la informacion fel generada por BSM infile v13""",

    'description': """
        Este modulo tiene la funcion de guardar un backup de la informacion fel de las facturas y poder restaurar dicha informacion en caso sea necesario desinstalar el POs o tambien el modulo  de  FEL.
    """,

    'author': "Erick Hernandez",
    'website': "http://www.facebook.com/totoday72",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/account_move.xml',
        'views/res_partner.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
