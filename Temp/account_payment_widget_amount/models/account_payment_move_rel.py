from odoo import api, fields, models, _

class Account_payment_move_relation(models.Model):
    _name= 'account_payment_widget_amount.payment_move_rel'
    _description = "Account move and account payment relation."


    factura_id = fields.Many2one("account.move", "Factura")
    pago_abono_id = fields.Many2one("account.payment", "Pago")
    no_corte_pago_abono = fields.Integer(string='Numero de corte')
    monto_pago_abono = fields.Float(string="Monto cobrado", digits=(6,2), default=0.00)

