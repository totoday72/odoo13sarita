# Copyright 2018-2021 ForgeFlow S.L.

from odoo import api, fields, models, _
from odoo.tools import float_compare


class AccountMove(models.Model):

    _inherit = "account.move"
    # numero_pago_abono = fields.Integer(string='Numero de Pago')
    # pago_abono = fields.Many2one("account.payment", "Pago", ondelete="cascade")

    def js_assign_outstanding_line(self, line_id):
        fact = self.env["account.move"].search([("id", "=", self.id)])
        moveline = self.env["account.move.line"].search([("id", "=", line_id)])
        pago = False
        for move in moveline:
            pago = self.env["account.payment"].search(['&', ("move_name", "=", move.move_name), ("partner_id", "=", fact.partner_id.id)])
        for fac in fact:
            monto_pago = 0.00
            move_line = self.env["account.move.line"].search(["&", ("payment_id", "=", pago.id), ("name", "=", 'Pago de cliente')])
            if abs(fac.amount_residual) >= abs(move_line.amount_residual):
                monto_pago = move_line.amount_residual
            elif abs(fac.amount_residual) < abs(move_line.amount_residual):
                monto_pago = fac.amount_residual
            rel = self.env['account_payment_widget_amount.payment_move_rel'].create({
                'factura_id': fac.id,
                'pago_abono_id': pago.id,
                'no_corte_pago_abono': 0,
                'monto_pago_abono': abs(monto_pago),
            })
        self.ensure_one()
        if "paid_amount" in self.env.context:
            return super(
                AccountMove,
                self.with_context(
                    move_id=self.id,
                    line_id=line_id,
                ),
            ).js_assign_outstanding_line(line_id)
        return super(AccountMove, self).js_assign_outstanding_line(line_id)


class AccountMoveLine(models.Model):

    _inherit = "account.move.line"

    def _prepare_reconciliation_partials(self):
        am_model = self.env["account.move"]
        aml_model = self.env["account.move.line"]
        partials = super(AccountMoveLine, self)._prepare_reconciliation_partials()
        if self.env.context.get("paid_amount", 0.0):
            total_paid = self.env.context.get("paid_amount", 0.0)
            current_am = am_model.browse(self.env.context.get("move_id"))
            current_aml = aml_model.browse(self.env.context.get("line_id"))
            decimal_places = current_am.company_id.currency_id.decimal_places
            if current_am.currency_id.id != current_am.company_currency_id.id:
                total_paid = current_am.currency_id._convert(
                    total_paid,
                    current_aml.currency_id,
                    current_am.company_id,
                    current_aml.date,
                )
            for partial in partials:
                debit_line = self.browse(partial.get("debit_move_id"))
                credit_line = self.browse(partial.get("credit_move_id"))
                different_currency = (
                    debit_line.currency_id.id != credit_line.currency_id.id
                )
                to_apply = min(total_paid, partial.get("amount", 0.0))
                partial.update(
                    {
                        "amount": to_apply,
                    }
                )
                if different_currency:
                    partial.update(
                        {
                            "debit_amount_currency": credit_line.company_currency_id._convert(
                                to_apply,
                                debit_line.currency_id,
                                credit_line.company_id,
                                credit_line.date,
                            ),
                            "credit_amount_currency": debit_line.company_currency_id._convert(
                                to_apply,
                                credit_line.currency_id,
                                debit_line.company_id,
                                debit_line.date,
                            ),
                        }
                    )
                else:
                    partial.update(
                        {
                            "debit_amount_currency": to_apply,
                            "credit_amount_currency": to_apply,
                        }
                    )
                total_paid -= to_apply
                if float_compare(total_paid, 0.0, precision_digits=decimal_places) <= 0:
                    break
        return partials
