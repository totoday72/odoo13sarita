# -*- encoding: utf-8 -*-

from odoo import api, models
from datetime import datetime


class ReporteVisa(models.AbstractModel):
    _name = 'report.l10n_gt_extra.reporte_credo'

    def lineas(self, datos):
        lineas = []
        total_consumo=0
        total_anticipos = 0
        total_abono = 0
        total_sub_total = 0
        total_sub_total2 = 0
        total_comision = 0
        total_iva_comision = 0
        total_comision_sin_iva=0
        total_retencion_iva = 0
        total_liquido_recibir = 0
        total_liquido_recibido = 0
        total_diferencia = 0
        total_inguat = 0


        for linea in self.env['corte_z.hotel'].search([('fecha', '>=', datos['fecha_desde']),
                     ('fecha', '<=', datos['fecha_hasta'])], order='fecha'):



            detalle = {
                'Fecha': linea.fecha.strftime("%d-%m-%Y"),
                'No._Corte': linea.num_corte_mostrado,
                'Consumo': self.formato_num(linea.cant_credo),
                'Anticipos': self.formato_num(linea.monto_anticipo),
                'Abono_Creditos': self.formato_num(linea.cxc_total),
                'Total': self.formato_num(linea.cant_total_credo),
                'Inguat': self.formato_num(linea.cs_inguat_c),
                'Sub_Total': self.formato_num(linea.cs_consumo_c),
                'Comision_sin_IVA': self.formato_num(linea.cs_comision_sin_iva_c),
                'Comision': self.formato_num(linea.cs_comision_c),
                'IVA_Comsion': self.formato_num(linea.cs_iva_c),
                'Retencion_de_Iva': self.formato_num(linea.cs_retencion_c),
                'Liquido_a_Recibir': self.formato_num(linea.cs_liquido_c),
                'Liquido_Recibido': self.formato_num(linea.cs_recibido_c),
                'Diferencia':self.formato_num(linea.cs_diferencia_c),


            }

            lineas.append(detalle)
            total_consumo += linea.cant_credo
            total_anticipos += linea.monto_anticipo
            total_abono += linea.cxc_total
            total_sub_total += linea.cant_total_credo
            total_inguat += linea.cs_inguat_c
            total_sub_total2 += linea.cs_consumo_c
            total_comision_sin_iva += linea.cs_comision_sin_iva_c
            total_comision += linea.cs_comision_c
            total_iva_comision += linea.cs_comision_c
            total_retencion_iva += linea.cs_retencion_c
            total_liquido_recibir += linea.cs_recibido_c
            total_liquido_recibido += linea.cs_recibido_c
            total_diferencia += linea.cs_diferencia_c



        sumatorias = {
                'Fecha':'',
                'No._Corte': '',
                'Consumo': self.formato_num(total_consumo),
                'Anticipos': self.formato_num(total_anticipos),
                'Abono_Creditos': self.formato_num(total_abono),
                'Total': self.formato_num(total_sub_total),
                'Inguat': self.formato_num(total_inguat),
                'Sub_Total': self.formato_num(total_sub_total2),
                'Comision_sin_IVA': self.formato_num(total_comision_sin_iva),
                'Comision': self.formato_num(total_comision),
                'IVA_Comsion': self.formato_num(total_iva_comision),
                'Retencion_de_Iva': self.formato_num(total_retencion_iva),
                'Liquido_a_Recibir': self.formato_num(total_liquido_recibir),
                'Liquido_Recibido': self.formato_num(total_liquido_recibido),
                'Diferencia': self.formato_num(total_diferencia),
         }
        lineas.append(sumatorias)
        return lineas

    def formato_num(self, num):
        n = '{:<}{:=12,.2f}'.format('', num)
        return n

    @api.model
    def _get_report_values(self, docids, data=None):
        return self.get_report_values(docids, data)

    @api.model
    def get_report_values(self, docids, data=None):
        model = self.env.context.get('active_model')
        docs = self.env[model].browse(self.env.context.get('active_ids', []))

        return {
            'doc_ids': self.ids,
            'doc_model': model,
            'data': data['form'],
            'docs': docs,
            'lineas': self.lineas,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
