# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import time

class AsistenteReporteVisa(models.TransientModel):
    _name = 'l10n_gt_extra.asistente_reporte_visa'


    fecha_desde = fields.Date(string="Fecha Inicial", required=True, default=lambda self: time.strftime('%Y-%m-01'))
    fecha_hasta = fields.Date(string="Fecha Final", required=True, default=lambda self: time.strftime('%Y-%m-%d'))

    def print_report(self):
        data = {
             'ids': [],
             'model': 'l10n_gt_extra.asistente_reporte_visa',
             'form': self.read()[0]
        }
        #return self.env.ref('l10n_gt_extra.action_reporte_visa').report_action(self, data=data)
        return self.env.ref('l10n_gt_extra.action_reporte_visa').with_context(landscape=True).report_action(self, data=data)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
