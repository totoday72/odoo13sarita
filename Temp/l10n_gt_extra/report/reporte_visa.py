# -*- encoding: utf-8 -*-

from odoo import api, models
from datetime import datetime


class ReporteVisa(models.AbstractModel):
    _name = 'report.l10n_gt_extra.reporte_visa'

    def lineas(self, datos):
        lineas = []
        total_consumo=0
        total_anticipos = 0
        total_abono = 0
        total_sub_total = 0
        total_sub_total2 = 0
        total_comision = 0
        total_iva_comision = 0
        total_comision_sin_iva=0
        total_retencion_iva = 0
        total_liquido_recibir = 0
        total_liquido_recibido = 0
        total_diferencia = 0
        total_inguat = 0

        for linea in self.env['corte_z.hotel'].search([('fecha', '>=', datos['fecha_desde']),
                     ('fecha', '<=', datos['fecha_hasta'])], order='fecha'):

            detalle = {
                'Fecha': linea.fecha.strftime("%d-%m-%Y"),
                'No._Corte': linea.num_corte_mostrado,
                'Consumo': self.formato_num(linea.cant_visa),
                'Anticipos': self.formato_num(linea.monto_anticipo),
                'Abono_Creditos': self.formato_num(linea.cxc_total),
                'Total': self.formato_num(linea.cant_total_visa),
                'Inguat': self.formato_num(linea.cs_inguat),
                'Sub_Total': self.formato_num(linea.cs_consumo_v),
                'Comision_sin_IVA': self.formato_num(linea.cs_comision_sin_iva),
                'Comision': self.formato_num(linea.cs_comision_v),
                'IVA_Comsion': self.formato_num(linea.cs_iva),
                'Retencion_de_Iva': self.formato_num(linea.cs_retencion),
                'Liquido_a_Recibir': self.formato_num(linea.cs_liquido),
                'Liquido_Recibido': self.formato_num(linea.cs_recibido),
                'Diferencia': self.formato_num(linea.cs_diferencia),
            }

            lineas.append(detalle)
            total_consumo += linea.cant_visa
            total_anticipos += linea.monto_anticipo
            total_abono += linea.cxc_total
            total_sub_total += linea.cant_total_visa
            total_inguat += linea.cs_inguat
            total_sub_total2 += linea.cs_consumo_v
            total_comision_sin_iva += linea.cs_comision_sin_iva
            total_comision += linea.cs_comision_v
            total_iva_comision += linea.cs_iva
            total_retencion_iva += linea.cs_retencion
            total_liquido_recibir += linea.cs_liquido
            total_liquido_recibido += linea.cs_recibido
            total_diferencia += linea.cs_diferencia

        sumatorias = {
                'Fecha':'',
                'No._Corte': '',
                'Consumo': self.formato_num(total_consumo),
                'Anticipos': self.formato_num(total_anticipos),
                'Abono_Creditos': self.formato_num(total_abono),
                'Total': self.formato_num(total_sub_total),
                'Inguat': self.formato_num(total_inguat),
                'Sub_Total': self.formato_num(total_sub_total2),
                'Comision_sin_IVA': self.formato_num(total_comision_sin_iva),
                'Comision': self.formato_num(total_comision),
                'IVA_Comsion': self.formato_num(total_iva_comision),
                'Retencion_de_Iva': self.formato_num(total_retencion_iva),
                'Liquido_a_Recibir': self.formato_num(total_liquido_recibir),
                'Liquido_Recibido': self.formato_num(total_liquido_recibido),
                'Diferencia': self.formato_num(total_diferencia),
         }
        lineas.append(sumatorias)
        return lineas

    def formato_num(self, num):
        n = '{:<}{:=12,.2f}'.format('', num)
        return n


    @api.model
    def _get_report_values(self, docids, data=None):
        return self.get_report_values(docids, data)

    @api.model
    def get_report_values(self, docids, data=None):
        model = self.env.context.get('active_model')
        docs = self.env[model].browse(self.env.context.get('active_ids', []))

        return {
            'doc_ids': self.ids,
            'doc_model': model,
            'data': data['form'],
            'docs': docs,
            'lineas': self.lineas,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
