# -*- coding: utf-8 -*-
{
    'name': "SaritaFEL",

    'summary': """
        PROSAR> Modulo para modificar Odoo, conforme a las necesidades de Sarita y Proseresa.""",

    'description': """
        Modulo SAT FEL Sarita ODOO v13:
        *Agregado:
        1. NIT de cliente.
    """,

    'author': "Erick Hernandez, Anibal Gomez, Mynor Agustin",
    'website': "http://www.saritarestaurante.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Edit Modules',
    'version': '1.1',

    # any module necessary for this one to work correctly
    'depends': ['account',],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_move.xml',
        'views/res_partner_form.xml',
        'views/templates.xml',
    ],

}
