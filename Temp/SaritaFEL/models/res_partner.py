from odoo import fields,models,api, _
from odoo.exceptions import UserError
import re
class sarita_fel_contact_info(models.Model):
    _inherit = 'res.partner'
    # account_nit = fields.Char(
    #     string = "N.I.T.:",
    # )

    @api.onchange('name')
    def set_upper(self):
        if self.name:
            self.name = str(self.name).upper()
            companys = self.env['res.partner'].search([])
            variabletexto = ""
            for contacto in companys:
                # print("Empresa>", contacto.name, " ID>", contacto.id)
                variabletexto += "Empresa>," + str(
                    contacto.name) + ", ID>" + str(
                    contacto.id) + "\n"
            # self.description = variabletexto
            if self.env.company.name == "Restaurante Sarita":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Productos y Servicios de Restaurantes S.A.":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Hotel Sarita, S.A.":
                self.parent_id = self.env.company.id
            elif self.env.company.name == "Personal y Servicios de Restaurantes S.A.":
                self.parent_id = self.env.company.id

        return

    @api.onchange('name')
    def action_colocarNit(self):
        if self.vat == False:
            self.vat = "CF"

    @api.onchange('vat')
    def action_revisarnit(self):
        if self.vat != False:
            txt = self.vat
            # 105304638
            # 2246659-2
            # 19652224
            # 3044990-K
            # 3044990-k
            # 3044990k
            # 3044990K
            # Return a match at every NON word character (characters NOT between a and Z. Like "!", "?" white-space etc.):
            x = re.findall("(^[C][F]$|^[c][f]$)|(^[0-9]{8}$|^[0-9]{9}$)|((^[0-9]{7})(([K]$|[k]$|[0-9]$)|([-]([k]$|[K]$|[0-9]$))))", txt)
            print(x)
            if x:
                print("Yes, there is at least one match!")
            else:
                print("No match")
                self.vat="CF"
                raise UserError(_('Verifique el NIT ingresado, pueden ser 8 o 9 numeros o 7 numeros guion seguido de un numero o k \nEjemplo: '
                                  '\n1)105304638 (9 numeros) '
                                  '\n2)10530463 (8 numeros) '
                                  '\n3)1053046-8 (7 numeros - numero) '
                                  '\n4)1053046-k (7 numeros - k) '
                                  '\n5)1053046k (7 numeros k)'
                                  '\n #Nota> la k puede ser minuscula o mayuscula'))


