
class resultado_Fel:
    pass
    serie = ""
    preimpreso = ""
    nombre = ""
    direccion = ""
    telefono = ""
    autorizacion = ""
    referencia = ""
    def __init__(self):
        self.serie = ""
        self.preimpreso = ""
        self.nombre = ""
        self.direccion = ""
        self.telefono = ""
        self.autorizacion = ""
        self.referencia = ""

    def colocarserie(self,serie):
        self.serie=serie

    def colocarpreimpreso(self, preimpreso):
        self.preimpreso = preimpreso

    def colocarnombre(self, nombre):
        self.nombre= nombre

    def colocardireccion(self, direccion):
        self.direccion = direccion

    def colocartelefono(self, telefono):
        self.telefono = telefono

    def colocarautorizacion(self,  numerodeautorizacion):
        self.autorizacion = numerodeautorizacion

    def colocarreferencia(self, referencia):
        self.referencia = referencia

    def darserie(self):
        return self.serie

    def darpreimpreso(self):
        return self.preimpreso

    def darnombre(self):
        return self.nombre

    def dardireccion(self):
        return self.direccion

    def dartelefono(self):
        return self.telefono

    def darautorizacion(self):
        return self.autorizacion

    def darreferencia(self):
        return self.referencia