# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from zeep import Client
import xml.etree.ElementTree as et
from .resultado_Fel import resultado_Fel
import re

class sarita_fel(models.Model):
    _inherit = 'account.move'
    # _inherits = {'account.move','res.partner'}
    account_nit = fields.Char(related='partner_id.vat',string="NIT:", store=True,)
    Referencia_interno = fields.Char(string="Referencia interno:", store=True, )
    Serie_interno = fields.Char(string="Serie Interno:", store=True, )
    Correlativo_interno = fields.Char(string="Correlativo Interno:",store=True, )
    Serie = fields.Char(string="Serie:", store=True, )
    Preimpreso = fields.Char(string="Preimpreso:", store=True, )
    Nombre = fields.Char(string="Nombre:", store=True, )
    Direccion = fields.Char(string="Direccion:", store=True, )
    Telefono = fields.Char(string="Telefono:", store=True, )
    Autorizacion = fields.Char(string="No Autorizacion:", store=True, )

    def action_readProducts(self):
        print(self.env.user.company_id.id)
        print(self.env.company.id)
        for line in self.invoice_line_ids:
            print("Producto>"+str(line.product_id.name)+"Cantidad>"+str(line.quantity)+"Precio>"+str(line.price_unit)+"Subtotal>"+str(line.price_subtotal))


    def SOAP_Service(self):
        # Operations:
        # anulaDocumento(pUsuario: xsd:string, pPassword: xsd:string, pNitEmisor: xsd:string, pSerie: xsd:string, pPreimpreso: xsd:string, pNitComprador: xsd:string, pFechaAnulacion: xsd:string,pMotivoAnulacion: xsd:string) -> result: xsd:string
        # generaDocumento(pUsuario: xsd:string, pPassword: xsd:string, pNitEmisor: xsd:string, pEstablecimiento: xsd:decimal, pTipoDoc: xsd:decimal, pIdMaquina: xsd:string, pTipoRespuesta: xsd:string, pXml: xsd:string) -> result: xsd:string
        fecha = str(self.invoice_date.day)+"/"+str(self.invoice_date.month)+"/"+str(self.invoice_date.year)
        print(fecha)
        if str(self.Referencia_interno) != "0" and str(self.Referencia_interno)!="":
            self.Serie = ""
            self.Preimpreso = ""
            self.Nombre = ""
            self.Direccion = ""
            self.Telefono = ""
            self.Autorizacion = ""
            if self.account_nit == "":
                self.account_nit = "CF"
            a = "<DocElectronico> " \
                "<Encabezado> " \
                "<Receptor>" \
                    "<NITReceptor>"+str(self.account_nit)+"</NITReceptor>" \
                    "<Nombre>NOMBRE COMPRADOR</Nombre>" \
                    "<Direccion>DIRECCION COMPRADOR</Direccion>" \
                "</Receptor>" \
                "<InfoDoc>" \
                    "<TipoVenta>B</TipoVenta>" \
                    "<DestinoVenta>1</DestinoVenta>" \
                    "<Fecha>"+str(fecha)+"</Fecha>" \
                    "<Moneda>1</Moneda>" \
                    "<Tasa>1</Tasa>" \
                    "<Referencia>" + str(self.Referencia_interno) + "</Referencia>" \
                    "<SerieAdmin>ESC</SerieAdmin>" \
                    "<NumeroAdmin>30081</NumeroAdmin>" \
                    "<Reversion>N</Reversion>" \
                "</InfoDoc>" \
                "<Totales>" \
                    "<Bruto>8236</Bruto>" \
                    "<Descuento>0</Descuento>" \
                    "<Exento>0</Exento>" \
                    "<Otros>0</Otros>" \
                    "<Neto>7353.58</Neto>" \
                    "<Isr>0</Isr>" \
                    "<Iva>882.42</Iva>" \
                    "<Total>8236</Total>" \
                "</Totales>" \
                "</Encabezado>" \
                        "<Detalles>" \
                                "<Productos>" \
                                    "<Producto>100</Producto>" \
                                        "<Descripcion>Sopa de Camron</Descripcion>" \
                                        "<Medida>1</Medida>" \
                                        "<Cantidad>100</Cantidad>" \
                                        "<Precio>25.36</Precio>" \
                                        "<PorcDesc>0</PorcDesc>" \
                                        "<ImpBruto>2536</ImpBruto>" \
                                        "<ImpDescuento>0</ImpDescuento>" \
                                        "<ImpExento>0</ImpExento>" \
                                        "<ImpOtros>0</ImpOtros>" \
                                        "<ImpNeto>2264.29</ImpNeto>" \
                                        "<ImpIsr>0</ImpIsr>" \
                                        "<ImpIva>271.71</ImpIva>" \
                                        "<ImpTotal>2536</ImpTotal>" \
                                        "<TipoVentaDet>B</TipoVentaDet>" \
                                "</Productos>" \
                                "<Productos>" \
                                    "<Producto>100</Producto>" \
                                        "<Descripcion>Sopa de Pollo</Descripcion>" \
                                        "<Medida>1</Medida>" \
                                        "<Cantidad>200</Cantidad>" \
                                        "<Precio>28.50</Precio>" \
                                        "<PorcDesc>0</PorcDesc>" \
                                        "<ImpBruto>5700</ImpBruto>" \
                                        "<ImpDescuento>0</ImpDescuento>" \
                                        "<ImpExento>0</ImpExento>" \
                                        "<ImpOtros>0</ImpOtros>" \
                                        "<ImpNeto>5089.29</ImpNeto>" \
                                        "<ImpIsr>0</ImpIsr>" \
                                        "<ImpIva>610.71</ImpIva>" \
                                        "<ImpTotal>5700</ImpTotal>" \
                                        "<TipoVentaDet>B</TipoVentaDet>" \
                                "</Productos>" \
                        "</Detalles>" \
                "</DocElectronico>"
            resultadoxml=""
            Fel="False"
            try:
                client = Client(wsdl='https://pdte.guatefacturas.com/webservices63/feltest/Guatefac?WSDL')
                resultadoxml = client.service.generaDocumento("SARITA_WSFEL","PRUEBASWS","30449901", 1, 1,1,"r", a)
                print(resultadoxml)
            except:
                resultadoxml=""
                print(resultadoxml)
                Fel = "False"
                return
            tree = et.fromstring("<?xml version='1.0' encoding='utf8'?>" + resultadoxml)
            tienedata = False
            objeto_resultado = resultado_Fel()
            for child in tree:
                if child.tag == "Serie":
                    tienedata = True
                    Fel = "True"
                    objeto_resultado.colocarserie(child.text)
                elif child.tag == "Preimpreso":
                    objeto_resultado.colocarpreimpreso(child.text)
                elif child.tag == "Nombre":
                    objeto_resultado.colocarnombre(child.text)
                elif child.tag == "Direccion":
                    objeto_resultado.colocardireccion(child.text)
                elif child.tag == "Telefono":
                    objeto_resultado.colocartelefono(child.text)
                elif child.tag == "NumeroAutorizacion":
                    objeto_resultado.colocarautorizacion(child.text)
                elif child.tag == "Referencia":
                    objeto_resultado.colocarreferencia(child.text)
                print(child.tag, child.attrib,child.text)  # los que se usan son tag y text para contenido

            if not tienedata:
                raise UserError(_('Existe un error con GuateFacturas: ' + str(resultadoxml)))
            else:
                self.Serie = objeto_resultado.serie
                self.Preimpreso = objeto_resultado.preimpreso
                self.Nombre = objeto_resultado.nombre
                self.Direccion = objeto_resultado.direccion
                self.Telefono = objeto_resultado.telefono
                self.Autorizacion = objeto_resultado.autorizacion
            print(repr(objeto_resultado.serie))
            print("STR:" + str(objeto_resultado.serie))
            print("INFO: \nSerie:" + str(
                objeto_resultado.darserie()) + " \nPreImpreso:" + str(
                objeto_resultado.darpreimpreso()) + " \nNombre:" + str(
                objeto_resultado.darnombre()) + " \nDireccion:" + str(
                objeto_resultado.dardireccion()) + " \nTelefono:" + str(
                objeto_resultado.dartelefono()) + " \nAutorizacion:" + str(
                objeto_resultado.darautorizacion()) + " \nReferencia:" + str(
                objeto_resultado.darreferencia()))
            id_compania = self.company_id.id
            values = "("+str(id_compania)+",'"+str(self.Serie_interno)+"',"+str(self.Correlativo_interno)+",'"+self.Referencia_interno+"',"+Fel+",'"+objeto_resultado.serie+"','"+objeto_resultado.preimpreso+"','"+objeto_resultado.autorizacion+"')"
            self._cr.execute('''
                        INSERT INTO public.sarita_referencias ("Company","serie_interno","Correlativo_interno","Referencia_interno","Fel_procesado","Fel_serie","Fel_Correlativo","Fel_Autorizacion")
                        VALUES %s
                        ''' % values
                        )#company,serie,correlativo,referencia,fel,felserie,felcorrelativo,felautorizacion
        else:
            print("not data")
            print("ingrese un valor que no sea 0 o vacio")
        print(self.account_nit)

        # a = "<DocElectronico> <Encabezado> <Receptor><NITReceptor>CF</NITReceptor><Nombre>NOMBRE COMPRADOR</Nombre><Direccion>DIRECCION COMPRADOR</Direccion></Receptor><InfoDoc><TipoVenta>B</TipoVenta><DestinoVenta>1</DestinoVenta><Fecha>25/12/2020</Fecha><Moneda>1</Moneda><Tasa>1</Tasa><Referencia>81</Referencia><SerieAdmin>ESC</SerieAdmin><NumeroAdmin>30081</NumeroAdmin><Reversion>N</Reversion></InfoDoc><Totales><Bruto>8236</Bruto><Descuento>0</Descuento><Exento>0</Exento><Otros>0</Otros><Neto>7353.58</Neto><Isr>0</Isr><Iva>882.42</Iva><Total>8236</Total></Totales></Encabezado><Detalles><Productos><Producto>100</Producto><Descripcion>Sopa de Camron</Descripcion><Medida>1</Medida><Cantidad>100</Cantidad><Precio>25.36</Precio><PorcDesc>0</PorcDesc><ImpBruto>2536</ImpBruto><ImpDescuento>0</ImpDescuento><ImpExento>0</ImpExento><ImpOtros>0</ImpOtros><ImpNeto>2264.29</ImpNeto><ImpIsr>0</ImpIsr><ImpIva>271.71</ImpIva><ImpTotal>2536</ImpTotal><TipoVentaDet>B</TipoVentaDet></Productos><Productos><Producto>100</Producto><Descripcion>Sopa de Pollo</Descripcion><Medida>1</Medida><Cantidad>200</Cantidad><Precio>28.50</Precio><PorcDesc>0</PorcDesc><ImpBruto>5700</ImpBruto><ImpDescuento>0</ImpDescuento><ImpExento>0</ImpExento><ImpOtros>0</ImpOtros><ImpNeto>5089.29</ImpNeto><ImpIsr>0</ImpIsr><ImpIva>610.71</ImpIva><ImpTotal>5700</ImpTotal><TipoVentaDet>B</TipoVentaDet></Productos></Detalles></DocElectronico>"
        # client = Client(wsdl='https://pdte.guatefacturas.com/webservices63/feltest/Guatefac?WSDL')
        # print(client.service.generaDocumento("SARITA_WSFEL","PRUEBASWS","30449901",1,1,1,"r",a))
        # client = Client(wsdl='http://www.dneonline.com/calculator.asmx?wsdl')
        # print(client.service.Divide(10,2))




    @api.onchange('partner_id')
    def do_genre(self):
        # self._cr.execute(
        #     '''
        #         SELECT *
        #         FROM res_partner request
        #         WHERE request.vat <> '4211455'
        #     '''
        # )
        idcompania = self.company_id.id
        if str(idcompania)=="False":
            raise UserError(_("Seleccione su compania antes de continuar."))
        print("Compania es:"+str(idcompania))
        self._cr.execute('''
            SELECT
              r."serie_interno",r."Correlativo_interno",r."Referencia_interno"
            FROM 
              public.sarita_referencias r
            WHERE
              r."Company" = %s
            ORDER BY r."Referencia_interno" DESC Limit 1
              '''% idcompania
                         )
        # results1 = self._cr.fetchone()
        results1 = self._cr.fetchall()
        for row in results1:
            print("Numero de conteo:"+str(row[0]))
            self.Serie_interno = str(row[0])
            self.Correlativo_interno = str(int(row[1])+1)
            self.Referencia_interno = str(int(row[2]) + 1)
        # for row in results1:
        #     print("Id: "+ str(row[0]))
        #     print("Name: " + str(row[1]))
        #     print("Company_id: " + str(row[2]))
        #     print("Create_ate: " + str(row[3]))
        #     if str(row[18])=='Gerente':
        #         # raise UserError(_('Hay un gerente!'))
        #         print("Es gerente")
        #     else:
        #         print("no es gerente")
        #         # raise UserError(_('No es un gerente'))
        # self.SOAP_Service() ##############################################################################3
        # raise UserError(_('Fecha>'+str(self.invoice_date)))
        # results = self.env['res.partner'].search([('vat', '!=', '4211455')])
        # for result in results:
        #     # print("Name: " + str(result.name))
        # raise UserError(_('Property:' + str(results) + ":CR"+str(results1)))

    # _inherit = 'account.move'
    # account_genero = fields.Text(
    #     string="Genero:",
    # )

    # account_nit = fields.Selection(
    #     string = "N.I.T.:",
    #     selection = [('nit','42121'),('data','123155')]
    # )

    # _name = 'sarita_fel.sarita_fel'
    # _description = 'sarita_fel.sarita_fel'
    #
    # name = fields.Char()
    # value = fields.Integer()
    # value2 = fields.Float(compute="_value_pc", store=True)
    # description = fields.Text()
    #
    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100

# Cómo definir un campo computado
#
# Los campos calculados son definidos con los campos Dependencias y Calculos.
#
# El campo Dependencies listar los campos que dependan de los campos actuales. Es una lista separada por comas de nombres de campo, como name, size. Puedes también referirte a campos accesibles a través de otros campos relacionales, por instancia partner_id.company_id.name.
#
# El campo Compute es el código Python para calcular el valor del campo en un conjunto de registros. El valor del campo tiene que ser asignado a cada registro con un diccionario.
#
#
#     for record in self:
#         record['size'] = len(record.name)
#
# Las únicas variables predefinidas
#
#     self (el conjunto de registros para calcular)
#     datetime (Módulo Python)
#     dateutil (Módulo Python)
#     time (Módulo Python)
#
# Otras características son accesibles a través de self, como self.env, etc.
