# -*- coding: utf-8 -*-
from odoo import http
import json
from odoo.http import Response



class CorteZController(http.Controller):

    @http.route('/api/cortes', auth='public', method=['GET'], csrf=False)
    def get_cortes(self, **kw):
        try:
            # cortes = http.request.env['corte_z.hotel'].sudo().search_read([], ['id','usuario','company_gt','realizado'])
            cortes = http.request.env['account.move'].sudo().search_read([], ['name', 'partner_id','invoice_partner_display_name','amount_total_signed','state'])
            res = json.dumps(cortes, ensure_ascii=False).encode('utf-8')
            return Response(res, content_type='application/json;charset=utf-8', status=200)
        except Exception as e:
            return Response(json.dumps({'error': str(e)}), content_type='application/json;charset=utf-8', status=505)

